#include "Soundtrack.h"
#include <iostream>

using namespace std;

Sounds::Sounds()
{
}

Sounds::~Sounds()
{
}

void Sounds::Music(std::string _Music)
{
	const char* music = _Music.c_str();
	SimpleAudioEngine::getInstance()->playBackgroundMusic(music, true);
}

void Sounds::Sound(std::string _Sounds)
{
	const char* sound = _Sounds.c_str();
	SimpleAudioEngine::getInstance()->playEffect(sound);
	SimpleAudioEngine::getInstance()->stopEffect(true);
}

void Sounds::deletemusic()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}
void Sounds::SoundE()
{
	SimpleAudioEngine::getInstance()->stopEffect(true);
}
//void Sounds::lasers()
//{
//	SimpleAudioEngine::getInstance()->playEffect("Soundtrack/laser.mp3");
//	SimpleAudioEngine::getInstance()->stopEffect(true);
//}
//
//void Sounds::SoundvolumeMax()
//{
//	SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
//}
//
//void Sounds::SoundvolumeMin()
//{
//	SimpleAudioEngine::getInstance()->setEffectsVolume(0.1f);
//}
