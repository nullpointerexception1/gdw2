#include "mBox.h"
#include "HelloWorldScene.h"
#include "Character.h"
#include "CreatorScene.h"
#include "PlayMapScene.h"
#include "Titlescreen.h"
#include "SimpleAudioEngine.h"
#include "DisplayHandler.h"
#include "InputHandler.h"
#include "Soundtrack.h"

Size S;
Vec2 O;
Sounds starting;
int n = 2;

cocos2d::Scene * Title::createScene()
{
	return Title::create();
}


void Title::onExit()
{
	starting.deletemusic();
	Scene::onExit();
}

void Title::onEnter()
{
	starting.Music("Soundtrack/Second to Death title.mp3");
	Scene::onEnter();
}

bool Title::init()
{
	if (!Scene::init())
	{
		return false;
	}
	//DISPLAY->createDebugConsole(true);
	//std::cout << "THE CONSOLE IS NOW OPEN" << std::endl;
	//To edit titlescreen, edit PlaceholderTitlescreen.png, use Texture Packer on the TemporarySpriteSheet.tps file within resources, and publish sprite sheet.

	auto visibleSize = Director::getInstance()->getVisibleSize();
	S = visibleSize;
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	O = origin;

	blackScreen = DrawNode::create();
	blackScreen->drawSolidRect(Vec2(0, 0), visibleSize, Color4F::BLACK);
	backdrop = Sprite::createWithSpriteFrameName("PlaceholderTitlescreen.png");
	backdrop->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(backdrop, 0);
	for (int i = 0; i < n; i++)
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("MenuIcon.png");
		temp->setPosition(visibleSize.width - temp->getContentSize().width / 2, 0.75f * visibleSize.height - temp->getContentSize().height / 2 - (float) i * 0.5f * (visibleSize.height - temp->getContentSize().height) / (n - 1));
		icons.push_back(temp);
		mbox sub;
		sub.setPos(temp->getPosition() - temp->getContentSize() / 2);
		sub.setDim(temp->getContentSize());
		clicks.push_back(sub);
		Label* label;
		//Title screen button text
		if (i == 0)
			label = Label::createWithSystemFont("Play", "Times New Roman", 30);
		//else if (i == 1)
		//	label = Label::createWithSystemFont("Level Creator", "Times New Roman", 30);
		else
			label = Label::createWithSystemFont("Exit", "Times New Roman", 30);

		label->setColor(Color3B(1, 1, 1));
		label->setPosition(temp->getPosition());
		ops.push_back(label);
		this->addChild(ops[i], 2);
		
		this->addChild(icons[i], 1);
	}
	this->addChild(blackScreen, 10);
	this->scheduleUpdate();
	return true;
}

void Title::update(float dt)
{
	updateInputs(dt);
	INPUTS->clearForNextFrame();
}

void Title::updateInputs(float dt)
{
	if ((!transferScene) && inTime > 0)
	{
		//std::cout << "DECREASING" << std::endl;
		blackout(dt, &inTime, true);
	}
	else if (transferScene)
	{
		trigger(dt, choice);
	}
	else
	{
		updateMouseInputs(dt);
		updateKeyboardInputs(dt);
	}
}

void Title::updateMouseInputs(float dt)
{
	for (int i = 0; i < n; i++)
		if (clicks[i].contains(INPUTS->getMousePosition()))
		{
			icons[i]->setScale(1.25f);
			icons[i]->setPosition(S.width - icons[i]->getContentSize().width * 1.25f / 2.f, icons[i]->getPosition().y);
			ops[i]->setScale(1.25f);
			ops[i]->setPosition(icons[i]->getPosition());
			if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
			{
				transferScene = true;
				choice = ops[i]->getString();
			}
		}
		else
		{
			icons[i]->setScale(1.f);
			icons[i]->setPosition(S.width - icons[i]->getContentSize().width / 2.f, icons[i]->getPosition().y);
			ops[i]->setScale(1.f);
			ops[i]->setPosition(icons[i]->getPosition());
		}
}

void Title::trigger(float dt, std::string i)
{
	if (i == "Play" && inTime < 1)
	{
		blackout(dt * 2.0, &inTime, false);
	}
	else if (i == "Play")
	{
		starting.Sound("Soundtrack/selection button.mp3");
		Scene* s = PlayMap::createScene();
		Director::getInstance()->replaceScene(s);
	}
	else if (i == "Level Creator")
	{
		starting.Sound("Soundtrack/selection button.mp3");
		Scene* s = Creator::createScene();
		Director::getInstance()->replaceScene(s);
	}
	else
	{
		starting.Sound("Soundtrack/selection button.mp3");
		Director::getInstance()->end();
	}
}

void Title::blackout(float dt, float *t, bool direction)
{
	if (direction)
	{
		*t -= dt;
		if (*t < 0)
			*t = 0;
	}
	else
	{
		*t += dt;
		if (*t > 1)
			*t = 1;
	}
	blackScreen->setOpacity(*t * 255);
}

void Title::updateKeyboardInputs(float dt)
{
}

void Title::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}