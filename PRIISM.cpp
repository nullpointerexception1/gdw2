#include "PRIISM.h"
#include "Soundtrack.h"
#include <cstdlib>
#include <ctime>

Sounds Attack;

PRIISM::PRIISM(Scene * s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection, vector<Sprite*> lazor) : boss(s, pos, walkCycle, startDirection, 2)
{
	srand(time(0));
	setStandardAcc(accLimit);
	setSpawnpoint(pos);
	setHP(4);
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	setAlive(true);
	setUseStandardAcc(true);
	setMaxSpeed(0.f);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
	attackTime = 0.f;
	for (int i = 0; i < lazor.size(); i++)
	{
		lazerbeam.push_back(lazor[i]);
		lazerbeam[i]->setOpacity(0);
		if (i % 2 == 0)
		{
			lazerbeam[i]->setScaleX(2);
			lazerbeam[i]->setScaleY(100);
		}
		else
		{
			lazerbeam[i]->setScale(64.f / lazerbeam[i]->getContentSize().width);
			lazerbeam[i]->setPosition(pos);
			lazerbeam[i]->setGlobalZOrder(30);
		}
		lazerVel.push_back(Vec2(0, 0));
		s->addChild(lazerbeam[i]);
	}
	reverseDirection = (rand() % 2) * 2 - 1;
}

void PRIISM::spawn()
{
	setPosition(getSpawnPoint());
	getBossSprite()->setPosition(getSpawnPoint());
	getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	setVelocity(0, 0);
	setAcceleration(0, 0);
	setHP(4);
	setAlive(true);
	setUseStandardAcc(true);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
	attackTime = 0.f;
	laserSpeedUp = 0;
	setMaxSpeed(0.f);
	for (int i = 0; i < lazerbeam.size(); i++)
	{
		lazerbeam[i]->setOpacity(0);
		lazerVel[i] = Vec2(0, 0);
		lazerbeam[i]->setPosition(getBossSprite()->getPosition());
	}
	reverseDirection = (rand() % 2) * 2 - 1;
}

void PRIISM::die()
{
	Attack.Sound("Soundtrack/Priism Death.mp3");
	setAlive(false);
	lazerbeam[1]->setOpacity(0);
	lazerbeam[0]->setOpacity(0);
}

void PRIISM::move()
{
	Vec2 tempAcc = getVectorTo(getPlayerPosition());
	if (abs(tempAcc.x) > abs(tempAcc.y))
	{
		if (tempAcc.x > 0)
		{
			setDirection(3);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(1);
			getBossSprite()->setScaleX(-1 * abs(getBossSprite()->getScaleX()));
		}
	}
	else
	{
		if (tempAcc.y > 0)
		{
			setDirection(2);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(0);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
	}
	tempAcc.normalize();
	tempAcc *= getStandardAcc();
	setAcceleration(reverseDirection * Vec2(tempAcc.y, -tempAcc.x));
}

void PRIISM::attack(int attackType, float dt)
{
	if (attackType == 1)
	{
		attackTime += dt;
		if (attackTime * (laserSpeedUp + 1) > attackWait)
		{
			Attack.Sound("Soundtrack/Priism laser.mp3");
			attackTime = 0;
			lazerbeam[0]->setOpacity(255);
			lazerbeam[0]->setPosition(lazerbeam[1]->getPosition() + Vec2(0, lazerbeam[0]->getContentSize().height * 0.5f * lazerbeam[0]->getScaleY()));
			lazerbeam[0]->setGlobalZOrder(31);
			setSignal(Vec3(lazerbeam[1]->getPosition().x, lazerbeam[1]->getPosition().y, 1));
			lazerbeam[1]->setPosition(getPlayerPosition());
			lazerbeam[1]->setOpacity(0);
		}
		else
		{
			if (lazerbeam[0]->getOpacity() > 0)
			{
				if (lazerbeam[0]->getOpacity() - dt * 500.f > 0)
				{
					lazerbeam[0]->setOpacity(lazerbeam[0]->getOpacity() - dt * 500.f);
				}
				else
				{
					lazerbeam[0]->setOpacity(0);
				}
			}
			lazerbeam[1]->setOpacity(255.f * attackTime * (laserSpeedUp + 1) / attackWait);
			lazerbeam[1]->setRotation((attackTime * (laserSpeedUp + 1) - attackWait) * (attackTime * (laserSpeedUp + 1) - attackWait) * 180.f);
			Vec2 tempVec = getPlayerPosition() - lazerbeam[1]->getPosition();
			if (tempVec.length() > 0)
			{
				tempVec.normalize();
				tempVec *= 1000.f * (laserSpeedUp + 1) * dt;
			}
			lazerVel[1] += tempVec;
			if ((getPlayerPosition() - lazerbeam[1]->getPosition()).dot(lazerVel[1]) / ((getPlayerPosition() - lazerbeam[1]->getPosition()).length() * lazerVel[1].length()) > 0.5f)
			{
				if (lazerVel[1].length() > 800.f)
				{
					lazerVel[1].normalize();
					lazerVel[1] *= (800.f);
				}
			}
			else
			{
				if (lazerVel[1].length() > 100.f * getHP())
				{
					lazerVel[1].normalize();
					lazerVel[1] *= 100.f * getHP();
				}
			}
			
			lazerbeam[1]->setPosition(lazerbeam[1]->getPosition() + lazerVel[1] * dt);
		}
	}
}

Vec2 PRIISM::getVectorTo(Vec2 place)
{
	if ((place - getPosition()).length() > 0)
	{
		Vec2 temp = place - getPosition();
		temp.normalize();
		return temp;
	}
	else
		return Vec2(0, 0);
}

void PRIISM::setTarget(Vec2 tar)
{
	target = tar;
}

Vec2 PRIISM::getTarget()
{
	return target;
}

void PRIISM::onDamage(float dt)
{
	laserSpeedUp++;
	attackTime = 0.f;
	setMaxSpeed(getMaxSpeed() + 200.f);
	reverseDirection *= -1.f;
}
