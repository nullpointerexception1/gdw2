#pragma once
#include "BossVirt.h"
#include <vector>

using namespace::std;

class PRIISM : public boss
{
public:
	PRIISM(Scene* s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection, vector<Sprite*> lazor);
	void spawn();
	void die();
	void move();
	void attack(int attackType, float dt);
	Vec2 getVectorTo(Vec2 place);
	void setTarget(Vec2 tar);
	Vec2 getTarget();
	void onDamage(float dt);
private:
	Vec2 target;
	float attackTime = 0.f, attackWait = 6.f, reverseDirection = 1.f;
	vector<Sprite*> lazerbeam;
	vector<Vec2> lazerVel;
	float lazerAcc = 0.f;
	int laserSpeedUp = 0;
};