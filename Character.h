#pragma once

#include <vector>
#include "cocos2d.h"

using namespace::cocos2d;

class character
{
public:
	character();
	character(Scene* scene, std::vector<std::vector<SpriteFrame*>> daSpritz);
	~character();

	void setBB (float x, float y);
	void Scale (float s);
	float getBX();
	float getBY();

	Vec2 speed;

	float scale, bx, by, acc;

	Vec2 checkpoint;

	int walkNum, direction, frameTot, explosionFrame, maxPlode, movingBox;
	float walkTime, stepLength, boomLength, boomTime;
	bool invert, blowingUp, hasMoved;

	Vec2 mockVel = Vec2(0, 0);

	Vec2 prev = Vec2(0, 0);

	Vec2 toBox;

	Sprite* name;

	std::vector<std::vector<SpriteFrame*>> walk;
	std::vector<SpriteFrame*> splode;
	std::vector<std::vector<SpriteFrame*>> trip;
	std::vector<SpriteFrame*> burn;
	Sprite* getSprite();

	bool canExplode = true, tripped = false, burned = false;
	void explode();
	void setAtCheckpoint();
	void incrementWalk(float dt);
	void resetWalk();
	void explosionSequence(float dt);
};