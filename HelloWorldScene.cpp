#include <vector>
#include "LevelTile.h"
#include "Character.h"
#include "PlayMapScene.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "DisplayHandler.h"
#include "InputHandler.h"
#include <ctime>
#include <cstdlib>
#include <fstream>
#include "Soundtrack.h"

USING_NS_CC;

const int fheight = 60;
const int fwidth = 60;

using namespace::cocos2d;
using namespace::std;

Sounds Effect;
Camera* cam;
Vec2 visibleSizeGlobal;

Scene* HelloWorld::createScene()
{

	//DISPLAY->createDebugConsole(true);
	//std::cout << "THE CONSOLE IS NOW OPEN" << std::endl;
	Scene* scene = HelloWorld::createWithPhysics();
	HelloWorld* layery = HelloWorld::create();
	//layer->setTag(2);
	cam = Camera::create();
	cam->setCameraFlag(CameraFlag::USER1);
	layery->addChild(cam, 2);
	layery->setTag(100);
	layery->setCameraMask((unsigned short)CameraFlag::USER1, true);
	//layeryH = layery;
	//
	//
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	//
	scene->addChild(layery);
	//cout << "HAPPINESS" << endl;
	//sceneHandle = scene;

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();
	physicsWorld = scene->getPhysicsWorld();
	
    return scene;
}

void HelloWorld::onExit()
{
	for (int i = 0; i < bosses.size(); i++)
	{
		delete bosses[i];
	}
	Effect.deletemusic();
	Scene::onExit();
}

void HelloWorld::onEnter()
{
	// code below checks which map has been selected and which background music will be played
	string word;
	string line;
	int offset;
	fstream NameMap;
	NameMap.open("CollectionOfS2DSaves.txt");
	if (NameMap.is_open())
	{
		while (!NameMap.eof())
		{
			getline(NameMap, line);
			//stupid code above, needs it or everything breaks and freeze
			//the whole game and gives a black scene
			//method one:
			/*if ((offset = mapName.find("DEMO LEVEL", 0)) != string::npos)
			{
			Effect.Music("Soundtrack/Basement Music 2.mp3");
			}*/
			//method two:
			if (mapName == "LEVEL 1" || mapName == "LEVEL 2" || mapName == "LEVEL 3" || mapName == "LEVEL 4")
			{
				Effect.Music("Soundtrack/Sector 1.mp3");
			}
			else if (mapName == "LEVEL 5")
			{
				Effect.Music("Soundtrack/Sector 1 boss.mp3");
			}
			else if (mapName == "LEVEL 6" || mapName == "LEVEL 7" || mapName == "LEVEL 8" || mapName == "LEVEL 9")
			{
				Effect.Music("Soundtrack/Sector 2.mp3");
			}
			else if (mapName == "LEVEL 10")
			{
				Effect.Music("Soundtrack/Sector 2 boss.mp3");
			}
			if (mapName == "LEVEL 11" || mapName == "LEVEL 12" || mapName == "LEVEL 13" || mapName == "LEVEL 14")
			{
				Effect.Music("Soundtrack/Sector 3.mp3");
			}
			if (mapName == "LEVEL 15")
			{
				Effect.Music("Soundtrack/Sector 3 boss.mp3");
			}
		}
		NameMap.close();
	}
	Scene::onEnter();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//DISPLAY->createDebugConsole(true);
	//std::cout << "THE CONSOLE IS NOW OPEN" << std::endl;
	srand(time(0));
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
	//cout << "HI" << endl;
	//cout << "1" << endl;
    auto visibleSize = Director::getInstance()->getVisibleSize();
	visibleSizeGlobal = Vec2(visibleSize.width, visibleSize.height);
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	walkNum = 1;
	//cout << "2" << endl;

	//SpriteFrameCache::getInstance()->addSpriteFramesWithFile("TempSprSheet.plist");
	//SpriteFrameCache::getInstance()->addSpriteFramesWithFile("door.plist");

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
   

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

	getDefaultCamera()->setVisible(false);
	ifstream fIn, mapToPlay;
	mapToPlay.open("S2DPlayingMap.txt");
	getline(mapToPlay, mapName);
	mapToPlay.close();
	fIn.open("AllSprite.txt");
	string mini;
	//cout << "3" << endl;
	while (getline(fIn, mini))
	{
		Sprite* temp = Sprite::createWithSpriteFrameName(mini);
		temp->getTexture()->setTexParameters(texParams);
		temp->setScale(64.f / temp->getContentSize().width);
		temp->setVisible(false);
		getline(fIn, mini);
		temp->setGlobalZOrder(stoi(mini) + 10);
		getline(fIn, mini);
		temp->setTag(stoi(mini));

		all.push_back(temp);
		string te = "";
		int sm = all.size() - 1;
		te += to_string(sm);
		all[sm]->setName(te);
		this->addChild(all[sm]);
	}
	fIn.close();
    // add "HelloWorld" splash screen"
	for (int i = 0; i < maximumH; i++)
	{
		std::vector <std::vector<bool>> HIYA;
		std::vector <std::vector <Sprite*>> hello;
		std::vector<int> operate;
		std::vector<bool> onOrOff;
		std::vector<float> tempMint;
		std::vector<bool> ops;
		for (int j = 0; j < maximumW; j++)
		{
			Sprite* de = Sprite::createWithSpriteFrame(all[1]->getSpriteFrame());
			std::vector<Sprite*> hillo;
			int opa = 0;
			hillo.push_back(de);
			hello.push_back(hillo);
			std::vector<bool> OP;
			for (int k = 0; k < 6; k++)
				OP.push_back(false);
			HIYA.push_back(OP);
			operate.push_back(opa);
			onOrOff.push_back(false);
			tempMint.push_back(0);
			ops.push_back(false);
		}
		bossDoorsOpened.push_back(ops);
		permanentlyOn.push_back(ops);
		updateGrid.push_back(HIYA);
		tiles.push_back(hello);
		opacityOfSquare.push_back(operate);
		lampsOnOrOff.push_back(onOrOff);
		mintyTiles.push_back(tempMint);
	}

	//cout << "6" << endl;
	LoadingMap();
	displayStuff = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("level_complete.png"));
	displayStuff->setVisible(false);
	displayStuff->setGlobalZOrder(-100);

	clickToContinue = Label::createWithSystemFont("Click To Continue", "Times New Roman", 50);
	clickToContinue->setVisible(false);
	clickToContinue->setColor(Color3B(200, 0, 0));

	GG = Sprite::create("rip.png");
	GG->setVisible(false);
	GG->setOpacity(0);
	GG->setGlobalZOrder(400.5f);
	GG->setPosition(visibleSize);
	
	keepOnGoing = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("MenuIcon.png"));
	keepOnGoing->setPosition(visibleSize.width * 0.3f, visibleSize.height * 0.25f);
	keepOnGoing->setVisible(false);
	keepOnGoing->setOpacity(0);
	keepOnGoing->setGlobalZOrder(401);

	keepOnGoingL = Label::createWithSystemFont("Continue", "Times New Roman", 30);
	keepOnGoingL->setPosition(keepOnGoing->getPosition());
	keepOnGoingL->setVisible(false);
	keepOnGoingL->setOpacity(0);
	keepOnGoingL->setGlobalZOrder(402);
	
	keepOnGoingB.setPos(keepOnGoing->getPosition() - keepOnGoing->getContentSize() * 0.5f);
	keepOnGoingB.setDim(keepOnGoing->getContentSize());
	
	giveUp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("MenuIcon.png"));
	giveUp->setPosition(visibleSize.width * 0.7f, visibleSize.height * 0.25f);
	giveUp->setVisible(false);
	giveUp->setOpacity(0);
	giveUp->setGlobalZOrder(401);

	giveUpL = Label::createWithSystemFont("Give Up", "Times New Roman", 30);
	giveUpL->setPosition(giveUp->getPosition());
	giveUpL->setVisible(false);
	giveUpL->setOpacity(0);
	giveUpL->setGlobalZOrder(402);

	giveUpB.setPos(giveUp->getPosition() - giveUp->getContentSize() * 0.5f);
	giveUpB.setDim(giveUp->getContentSize());

	//cout << "7" << endl;

	//VertexBuffer *v = VertexBuffer::create();

	vector<vector<SpriteFrame*>> characterFrames;
	vector<SpriteFrame*> frontFrames, sideFrames, backFrames, otherSideFrames;
	for (int i = 0; i < 12; i++)
	{
		if (i < 9)
		{
			frontFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("front0" + to_string(i + 1) + ".png"));
			sideFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("side0" + to_string(i + 1) + ".png"));
			backFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("back0" + to_string(i + 1) + ".png"));
			otherSideFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("side0" + to_string(i + 1) + ".png"));
			//otherSideFrames[i]->setOriginalSize(Size(0 - otherSideFrames[i]->getOriginalSize().width, otherSideFrames[i]->getOriginalSize().height));
		}
		else
		{
			frontFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("front" + to_string(i + 1) + ".png"));
			sideFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("side" + to_string(i + 1) + ".png"));
			backFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("back" + to_string(i + 1) + ".png"));
			otherSideFrames.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("side" + to_string(i + 1) + ".png"));
			//otherSideFrames[i]->setOriginalSize(Size(0 - otherSideFrames[i]->getOriginalSize().width, otherSideFrames[i]->getOriginalSize().height));
		}
	}
	characterFrames.push_back(frontFrames);
	characterFrames.push_back(sideFrames);
	characterFrames.push_back(backFrames);
	characterFrames.push_back(otherSideFrames);
	for (int i = 0; i < characterFrames.size(); i++)
	{
		for (int o = 0; o < characterFrames[i].size(); o++)
		{
			characterFrames[i][o]->getTexture()->setTexParameters(texParams);
		}
	}

	frontBlink = SpriteFrameCache::getInstance()->getSpriteFrameByName("frontBlink.png");
	frontBlink->getTexture()->setTexParameters(texParams);
	sideBlink = SpriteFrameCache::getInstance()->getSpriteFrameByName("sideBlink.png");
	sideBlink->getTexture()->setTexParameters(texParams);
	backBlink = SpriteFrameCache::getInstance()->getSpriteFrameByName("backBlink.png");
	backBlink->getTexture()->setTexParameters(texParams);

	character YOU (this, characterFrames);
	for (int i = 0; i < 10; i++)
		YOU.splode.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("exp_" + to_string(i + 1) + ".png"));
	for (int i = 0; i < YOU.splode.size(); i++)
		YOU.splode[i]->getTexture()->setTexParameters(texParams);

	for (int i = 0; i < 7; i++)
		doorz.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("door_" + to_string(i) + ".png"));
	for (int i = 0; i < 7; i++)
		bossDoorz.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("boss_door_" + to_string(i) + ".png"));
	for (int ii = 0; ii < 4; ii++)
	{
		vector<SpriteFrame*> trippy;
		for (int i = 0; i < 6; i++)
		{
			if (ii == 0)
				trippy.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("player_front_goop_" + to_string(i) + ".png"));
			else if (ii == 1)
				trippy.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("player_side_goop_" + to_string(i) + ".png"));
			else if (ii == 2)
				trippy.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("player_back_goop_" + to_string(i) + ".png"));
			else if (ii == 3)
				trippy.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("player_side_goop_" + to_string(i) + ".png"));
		}
		YOU.trip.push_back(trippy);
	}
	for (int i = 0; i < 5; i++)
	{
		YOU.burn.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("player_ash_" + to_string(i) + ".png"));
	}
	
	//YOU.name = Sprite::create("guy_bomber2.png");
	ch.push_back(YOU);
	//cout << "5" << endl;
	Texture2D::TexParams texParams = { GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };
	ch[0].name->getTexture()->setTexParameters(texParams);
	//ch[0].name->setScale(2.f);
	//PhysicsBody *pb = PhysicsBody::createCircle(ch[0].name->getContentSize().width / 2);
	//pb->setDynamic(true);
	//pb->setRotationEnable(false);
	//ch[0].name->setPhysicsBody(pb);
	ch[0].name->getPhysicsBody()->setCollisionBitmask(1);
	ch[0].name->getPhysicsBody()->setCategoryBitmask(2);
	ch[0].checkpoint = daStart * 64.f + Vec2( - maximumW * 32.f, 0.f);
	ch[0].name->setPosition(ch[0].checkpoint);
	this->addChild(ch[0].name);
	this->addChild(displayStuff);
	this->addChild(clickToContinue);

	blackScreen = DrawNode::create();
	blackScreen->drawSolidRect(Vec2(0, 0), visibleSize, Color4F::BLACK);
	blackScreen->setGlobalZOrder(400);
	this->addChild(blackScreen, 10);

	loding = Label::createWithSystemFont("Loading...", "Helvetica", 80);
	loding->setGlobalZOrder(400);
	loding->setColor(Color3B::WHITE);
	loding->setOpacity(0);

	currentSignSprite = Sprite::createWithSpriteFrame(all[1]->getSpriteFrame());
	currentSignSprite->setVisible(false);
	currentSignSprite->setScale(2);
	currentSignSprite->setGlobalZOrder(400);

	this->addChild(loding, 11);
	this->addChild(currentSignSprite);
	this->addChild(GG);
	this->addChild(keepOnGoing);
	this->addChild(keepOnGoingL);
	this->addChild(giveUp);
	this->addChild(giveUpL);
	HelloWorld::scheduleUpdate();
	//cout << "4" << endl;
    return true;
}

//bool HelloWorld::initWithPhysics()
//{
//	if (!Scene::initWithPhysics())
//	{
//		return false;
//	}
//	cout << "HERE I AM" << endl;
//	return true;
//}

void HelloWorld::update(float dt)
{
	if (ch[0].blowingUp && !resetMap)
	{
		resetMap = true;
	}
	else if (!ch[0].blowingUp && resetMap)
	{

		death = true;
		for (int i = 0; i < maximumW * maximumH; i++)
		{
			for (int k = 0; k < tiles[i % maximumW][i / maximumW].size(); k++)
			{
				if (tiles[i % maximumW][i / maximumW][k]->getTag() == 15)
				{
					tiles[i % maximumW][i / maximumW][k]->setOpacity(255);
				}
			}
		}
		stopBlackOut = false;
		tileUpdate.clear();
		tileBOOL.clear();
		nextTileUpdate.clear();
		nextTileBOOL.clear();
		buttonUpdate.clear();
		openDoors.clear();
		openBOOL.clear();
		lightLevel.clear();
		nextLightLevel.clear();
		opacityUpdate.clear();
		nextOpacityUpdate.clear();
		SUPEROpacityUpdate.clear();
		for (int i = 0; i < bossDoorLocations.size(); i++)
		{
			bossDoorBOOL[i] = false;
			bossDoorTimes[i] = 0.f;
		}
		for (int i = 0; i < timerz.size(); i++)
		{
			timerz[i].w = 0;
			timerMode[i] = false;
		}
		for (int i = 0; i < lazerLocs.size(); i++)
		{
			beamLocs[i] = lazerLocs[i];
			lazerz[i][0]->setPosition(beamLocs[i] * 64 - Vec2(maximumW * 32, 0));
			lazerz[i][1]->setPosition(beamLocs[i] * 64 - Vec2(maximumW * 32, 0));
			lazerz[i][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
			lazerz[i][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
			isLazer[i] = false;
			lazerz[i][0]->setVisible(false);
			lazerz[i][1]->setVisible(false);
			lazerTime[i] = 0;
		}
		for (int i = 0; i < maximumH; i++)
		{
			for (int j = 0; j < maximumW; j++)
			{
				lampsOnOrOff[i][j] = false;
				opacityOfSquare[i][j] = 0;
				permanentlyOn[i][j] = false;
				mintyTiles[i][j] = 0;
				bossDoorsOpened[i][j] = false;
				for (int k = 0; k < tiles[i][j].size(); k++)
				{
					if (blackedOut)
					{
						tiles[i][j][k]->setColor(Color3B(0, 0, 0));
						if (tiles[i][j][k]->getTag() == 27)
						{
							nextOpacityUpdate.push_back(Vec2(i, j));
						}
					}
					if (tiles[i][j][k]->getTag() == 2)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button.png"));
					}
					else if (tiles[i][j][k]->getTag() == 6)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("door_0.png"));
					}
					else if (tiles[i][j][k]->getTag() == 10)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_pressure_plate.png"));
					}
					else if (tiles[i][j][k]->getTag() == 14)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lever_off.png"));
					}
					else if (tiles[i][j][k]->getTag() == 26)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lamp.png"));
					}
					else if (tiles[i][j][k]->getTag() == 30)
					{
						tiles[i][j][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boss_door_0.png"));
					}
					else if (tiles[i][j][k]->getTag() == 60)
					{
						tiles[i][j][k]->setVisible(false);
					}
				}
				for (int k = 0; k < 6; k++)
				{
					updateGrid[i][j][k] = false;
				}
			}
		}
		mintyTrail.clear();
		for (int i = 0; i < bosses.size(); i++)
			bosses[i]->getBossSprite()->getPhysicsBody()->setEnabled(false);
		for (int i = 0; i < boxes.size(); i++)
		{
			boxes[i]->setPosition(originalBoxPositions[i]);
			boxDestroyed[i] = false;
			boxes[i]->setGlobalZOrder(3.00006f - 0.0001f * boxes[i]->getPosition().y);
		}
		
		GG->setVisible(true);
		keepOnGoing->setVisible(true);
		keepOnGoingL->setVisible(true);
		giveUp->setVisible(true);
		giveUpL->setVisible(true);
		resetMap = false;
	}
	if (!transferScene && inTime > 0)
	{
		blackout(dt * 2.f, &inTime, true);
		loding->setPosition(cam->getPosition());
		blackScreen->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f);
	}
	else
	{
	}
	if (won)
	{
		winSeq(dt);
	}
	else
	{
		//cout << dt << endl;
		physicsWorld->setGravity(Vec2(0, 0));

		Vec2 scru = prep - visibleSizeGlobal / 2;
		Vec2 scrd = prep + visibleSizeGlobal / 2;
		for (int i = scru.x / 64 - 1; i < scrd.x / 64 + 1; i++)
		{
			for (int j = scru.y / 64 - 1; j < scrd.y / 64 + 1; j++)
			{
				if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
				{
					for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
					{
						int tempTag = tiles[maximumW / 2 + i][j][k]->getTag();
						if (tempTag != 3 && tempTag != 4 && tempTag != 7 && tempTag != 8 && tempTag != 9 && tempTag != 11 && tempTag != 12 && tempTag != 13 && tempTag != 19 && tempTag != 28 && tempTag != 60 && tempTag != 32)
							tiles[maximumW / 2 + i][j][k]->setVisible(false);
						if (permanentlyOn[maximumW / 2 + i][j])
							tiles[maximumW / 2 + i][j][k]->setColor(Color3B(255, 255, 255));
						else if (blackedOut)
							tiles[maximumW / 2 + i][j][k]->setColor(Color3B(opacityOfSquare[maximumW / 2 + i][j], opacityOfSquare[maximumW / 2 + i][j], opacityOfSquare[maximumW / 2 + i][j]));
						if (tempTag == 60 && mintyTiles[maximumW / 2 + i][j] > 0)
							tiles[maximumW / 2 + i][j][k]->setVisible(false);
						//if (tiles[maximumW / 2 + i][j][k]->getTag() > 0 && tiles[maximumW / 2 + i][j][k]->getPosition().y < prep.y && tiles[maximumW / 2 + i][j][k]->getGlobalZOrder() > 100)
						//	tiles[maximumW / 2 + i][j][k]->setGlobalZOrder(tiles[maximumW / 2 + i][j][k]->getGlobalZOrder() - 200.f);
					}
				}
			}
		}
		for (int i = 0; i < isLazer.size(); i++)
		{
			if (lazerz[i][0]->getPosition().x > scru.x - 64.f && lazerz[i][0]->getPosition().x < scrd.x + 64.f && lazerz[i][0]->getPosition().y > scru.y - 64.f && lazerz[i][0]->getPosition().y < scrd.y + 64.f)
			{
				lazerz[i][0]->setVisible(false);
				lazerz[i][1]->setVisible(false);
			}
		}
		for (int i = 0; i < boxes.size(); i++)
		{
			if (boxes[i]->getPosition().x > scru.x - 64.f && boxes[i]->getPosition().x < scrd.x + 64.f && boxes[i]->getPosition().y > scru.y - 64.f && boxes[i]->getPosition().y < scrd.y + 64.f)
				boxes[i]->setVisible(false);
			if (permanentlyOn[(int)(boxes[i]->getPosition().x + 32.f) / 64 + maximumW / 2][(int)(boxes[i]->getPosition().y + 32.f) / 64])
				boxes[i]->setColor(Color3B(255, 255, 255));
			else if (blackedOut)
				boxes[i]->setColor(Color3B(tiles[(int)(boxes[i]->getPosition().x + 32.f) / 64 + maximumW / 2][(int)(boxes[i]->getPosition().y + 32.f) / 64][0]->getColor().r, tiles[(int)(boxes[i]->getPosition().x + 32.f) / 64 + maximumW / 2][(int)(boxes[i]->getPosition().y + 32.f) / 64][0]->getColor().g, tiles[(int)(boxes[i]->getPosition().x + 32.f) / 64 + maximumW / 2][(int)(boxes[i]->getPosition().y + 32.f) / 64][0]->getColor().b));
			//if (boxes[i]->getPosition().y < prep.y)
			//	boxes[i]->setGlobalZOrder(3.f - boxes[i]->getPosition().y * 0.0001f);
			//else
			//	boxes[i]->setGlobalZOrder(203.f - boxes[i]->getPosition().y * 0.0001f);
		}
		scru = prep;// -Vec2(64.f, 64.f);
		scrd = prep;// +Vec2(64.f, 64.f);
		for (int i = scru.x / 64 - 2; i < scrd.x / 64 + 2; i++)
		{
			for (int j = scru.y / 64 - 2; j < scrd.y / 64 + 2; j++)
			{
				if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
				{
					for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
					{
						if (tiles[maximumW / 2 + i][j][k]->getTag() == 1 || tiles[maximumW / 2 + i][j][k]->getTag() == 6 || (tiles[maximumW / 2 + i][j][k]->getTag() >= 20 && tiles[maximumW / 2 + i][j][k]->getTag() <= 23) || tiles[maximumW / 2 + i][j][k]->getTag() == 25 || tiles[maximumW / 2 + i][j][k]->getTag() == 30)
						{
							tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
							//if (tiles[maximumW / 2 + i][j][k]->getPosition().y < prep.y)
							//	for (int a = 0; a < tiles[maximumW / 2 + i][j].size(); a++)
							//	{
							//		int tempTag = tiles[maximumW / 2 + i][j][a]->getTag();
							//		if (tempTag == 1 || tempTag == 2 || tempTag == 6)
							//			tiles[maximumW / 2 + i][j][a]->setGlobalZOrder((int)tiles[maximumW / 2 + i][j][a]->getGlobalZOrder() % 200);
							//	}
						}
					}
				}
			}
		}
		for (int i = 0; i < boxes.size(); i++)
		{
			if (boxes[i]->getPosition().x > scru.x - 128.f && boxes[i]->getPosition().x < scrd.x + 128.f && boxes[i]->getPosition().y > scru.y - 128.f && boxes[i]->getPosition().y < scrd.y + 128.f)
			{
				boxes[i]->getPhysicsBody()->setEnabled(false);
			}
		}
		for (int m = 0; m < bosses.size(); m++)
		{
			bosses[m]->getBossSprite()->setGlobalZOrder(3.00008f - bosses[m]->getBossSprite()->getPosition().y * 0.0001f);
			scru = previousBossPositions[m];// -Vec2(64.f, 64.f);
			scrd = previousBossPositions[m];// +Vec2(64.f, 64.f);
			for (int i = scru.x / 64 - 2; i < scrd.x / 64 + 2; i++)
			{
				for (int j = scru.y / 64 - 2; j < scrd.y / 64 + 2; j++)
				{
					if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
					{
						for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
						{
							if (tiles[maximumW / 2 + i][j][k]->getTag() == 1 || tiles[maximumW / 2 + i][j][k]->getTag() == 6 || (tiles[maximumW / 2 + i][j][k]->getTag() >= 20 && tiles[maximumW / 2 + i][j][k]->getTag() <= 23) || tiles[maximumW / 2 + i][j][k]->getTag() == 25)
							{
								tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
								//if (tiles[maximumW / 2 + i][j][k]->getPosition().y < prep.y)
								//	for (int a = 0; a < tiles[maximumW / 2 + i][j].size(); a++)
								//	{
								//		int tempTag = tiles[maximumW / 2 + i][j][a]->getTag();
								//		if (tempTag == 1 || tempTag == 2 || tempTag == 6)
								//			tiles[maximumW / 2 + i][j][a]->setGlobalZOrder((int)tiles[maximumW / 2 + i][j][a]->getGlobalZOrder() % 200);
								//	}
							}
						}
					}
				}
			}
		}
		scru = cam->getPosition() - visibleSizeGlobal / 2;
		scrd = cam->getPosition() + visibleSizeGlobal / 2;
		for (int i = scru.x / 64 - 1; i < scrd.x / 64 + 1; i++)
		{
			for (int j = scru.y / 64 - 1; j < scrd.y / 64 + 1; j++)
			{
				if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
				{
					for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
					{
						int tempTag = tiles[maximumW / 2 + i][j][k]->getTag();
						if (tempTag != 3 && tempTag != 4 && tempTag != 7 && tempTag != 8 && tempTag != 9 && tempTag != 11 && tempTag != 12 && tempTag != 13 && tempTag != 19 && tempTag != 28 && tempTag != 60 && tempTag != 32)
							tiles[maximumW / 2 + i][j][k]->setVisible(true);
						if (tempTag == 60 && mintyTiles[maximumW / 2 + i][j] > 0)
							tiles[maximumW / 2 + i][j][k]->setVisible(true);
						//if (tiles[maximumW / 2 + i][j][k]->getTag() > 0 && tiles[maximumW / 2 + i][j][k]->getPosition().y < cam->getPosition().y && tiles[maximumW / 2 + i][j][k]->getGlobalZOrder() < 100)
						//	tiles[maximumW / 2 + i][j][k]->setGlobalZOrder(tiles[maximumW / 2 + i][j][k]->getGlobalZOrder() + 200.f);
					}
				}
			}
		}
		for (int i = 0; i < isLazer.size(); i++)
		{
			if (lazerz[i][0]->getPosition().x > scru.x - 64.f && lazerz[i][0]->getPosition().x < scrd.x + 64.f && lazerz[i][0]->getPosition().y > scru.y - 64.f && lazerz[i][0]->getPosition().y < scrd.y + 64.f && isLazer[i])
			{
				lazerz[i][0]->setVisible(true);
				lazerz[i][1]->setVisible(true);
			}
		}
		for (int i = 0; i < boxes.size(); i++)
		{
			if (boxes[i]->getPosition().x > scru.x - 64.f && boxes[i]->getPosition().x < scrd.x + 64.f && boxes[i]->getPosition().y > scru.y - 64.f && boxes[i]->getPosition().y < scrd.y + 64.f)
				boxes[i]->setVisible(true);
			//if (boxes[i]->getPosition().y < cam->getPosition().y)
			//	boxes[i]->setGlobalZOrder(203.f - boxes[i]->getPosition().y * 0.0001f);
			//else
			//	boxes[i]->setGlobalZOrder(3.f - boxes[i]->getPosition().y * 0.0001f);
		}
		scru = cam->getPosition();// -Vec2(64.f, 64.f);
		scrd = cam->getPosition();// +Vec2(64.f, 64.f);
		for (int i = scru.x / 64 - 2; i < scrd.x / 64 + 2; i++)
		{
			for (int j = scru.y / 64 - 2; j < scrd.y / 64 + 2; j++)
			{
				if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
				{
					//if (MapT[maxW / 2 + i][j][k]->getName() !="1")
					for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
					{
						if (tiles[maximumW / 2 + i][j][k]->getTag() == 1 || tiles[maximumW / 2 + i][j][k]->getTag() == 6 || (tiles[maximumW / 2 + i][j][k]->getTag() >= 20 && tiles[maximumW / 2 + i][j][k]->getTag() <= 23) || tiles[maximumW / 2 + i][j][k]->getTag() == 25 || tiles[maximumW / 2 + i][j][k]->getTag() == 30)
						{
							tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(true);
							if (tiles[maximumW / 2 + i][j][k]->getTag() == 6)
							{
								int tempForBools = 0;
								for (int a = 0; a < 5; a++)
									if (updateGrid[maximumW / 2 + i][j][a])
										tempForBools++;
								if (tempForBools > 0)
								{
									tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
								}
							}
							else if (tiles[maximumW / 2 + i][j][k]->getTag() == 30)
							{
								if (bossDoorsOpened[maximumW / 2 + i][j])
								{
									tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
								}
							}
							//if (tiles[maximumW / 2 + i][j][k]->getPosition().y < cam->getPosition().y)
							//	for (int a = 0; a < tiles[maximumW / 2 + i][j].size(); a++)
							//	{
							//		int tempTag = tiles[maximumW / 2 + i][j][a]->getTag();
							//		if (tempTag == 1 || tempTag == 2 || tempTag == 6)
							//			tiles[maximumW / 2 + i][j][a]->setGlobalZOrder((int)tiles[maximumW / 2 + i][j][a]->getGlobalZOrder() % 200 + 200);
							//	}
						}
						if (j > 0 && tiles[maximumW / 2 + i][j][k]->getName() == "1")
						{
							for (int i2 = 0; i2 < tiles[maximumW / 2 + i][j - 1].size(); i2++)
								if (tiles[maximumW / 2 + i][j - 1][i2]->getTag() == 16)
									tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
						}
					}
				}
			}
		}
		for (int i = 0; i < boxes.size(); i++)
		{
			if (boxes[i]->getPosition().x > scru.x - 128.f && boxes[i]->getPosition().x < scrd.x + 128.f && boxes[i]->getPosition().y > scru.y - 128.f && boxes[i]->getPosition().y < scrd.y + 128.f)
			{
				boxes[i]->getPhysicsBody()->setEnabled(true);
			}
			if (boxDestroyed[i])
			{
				boxes[i]->setPosition(-1000, 0);
			}
		}
		for (int m = 0; m < bosses.size(); m++)
		{
			scru = bosses[m]->getBossSprite()->getPosition();// -Vec2(64.f, 64.f);
			scrd = bosses[m]->getBossSprite()->getPosition();// +Vec2(64.f, 64.f);
			for (int i = scru.x / 64 - 2; i < scrd.x / 64 + 2; i++)
			{
				for (int j = scru.y / 64 - 2; j < scrd.y / 64 + 2; j++)
				{
					if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
					{
						//if (MapT[maxW / 2 + i][j][k]->getName() !="1")
						for (int k = 0; k < tiles[maximumW / 2 + i][j].size(); k++)
						{
							if (tiles[maximumW / 2 + i][j][k]->getTag() == 1 || tiles[maximumW / 2 + i][j][k]->getTag() == 6 || (tiles[maximumW / 2 + i][j][k]->getTag() >= 20 && tiles[maximumW / 2 + i][j][k]->getTag() <= 23) || tiles[maximumW / 2 + i][j][k]->getTag() == 25)
							{
								tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(true);
								if (tiles[maximumW / 2 + i][j][k]->getTag() == 6)
								{
									int tempForBools = 0;
									for (int a = 0; a < 5; a++)
										if (updateGrid[maximumW / 2 + i][j][a])
											tempForBools++;
									if (tempForBools > 0)
									{
										tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
									}
								}
								//if (tiles[maximumW / 2 + i][j][k]->getPosition().y < cam->getPosition().y)
								//	for (int a = 0; a < tiles[maximumW / 2 + i][j].size(); a++)
								//	{
								//		int tempTag = tiles[maximumW / 2 + i][j][a]->getTag();
								//		if (tempTag == 1 || tempTag == 2 || tempTag == 6)
								//			tiles[maximumW / 2 + i][j][a]->setGlobalZOrder((int)tiles[maximumW / 2 + i][j][a]->getGlobalZOrder() % 200 + 200);
								//	}
							}
							if (j > 0 && tiles[maximumW / 2 + i][j][k]->getName() == "1")
							{
								for (int i2 = 0; i2 < tiles[maximumW / 2 + i][j - 1].size(); i2++)
									if (tiles[maximumW / 2 + i][j - 1][i2]->getTag() == 16)
										tiles[maximumW / 2 + i][j][k]->getPhysicsBody()->setEnabled(false);
							}
						}
					}
				}
			}
		}

		noFrameChange = false;
		if (!ch[0].blowingUp && ch[0].hasMoved)
		{
			if (((ch[0].prev - ch[0].name->getPosition()).length() < 0.01f) || (boxGoopMinor && !beMovin))
			{
				tickTock += dt;
			}
			else
			{
				tickTock = 0;
			}
			if (tickTock < maxTime * 0.4f && tickTock >= maxTime * 0.2f)
			{
				//ch[0].name->setColor(Color3B(255.f, 0, 0));
				if (ch[0].tripped)
					ch[0].name->setSpriteFrame(ch[0].trip[ch[0].direction][5]);
				else if (ch[0].burned)
				{
				}
				else
				{
					Effect.Sound("Soundtrack/Beep.mp3");
					switch (ch[0].direction)
					{
					case 1:
						ch[0].name->setSpriteFrame(sideBlink);
						break;
					case 2:
						ch[0].name->setSpriteFrame(backBlink);
						break;
					case 3:
						ch[0].name->setSpriteFrame(sideBlink);
						break;
					default:
						ch[0].name->setSpriteFrame(frontBlink);
						break;
					}
				}
				noFrameChange = true;
			}
			else if (tickTock < maxTime * 0.8f && tickTock >= maxTime * 0.6f)
			{
				//ch[0].name->setColor(Color3B(255.f, 0, 0));
			}
			else if (tickTock > maxTime * 0.6f)
			{
				//ch[0].name->setColor(Color3B(255.f, 255.f, 255.f));
				if (ch[0].tripped)
					ch[0].name->setSpriteFrame(ch[0].trip[ch[0].direction][5]);
				else if (ch[0].burned)
				{
				}
				else
				{
					Effect.Sound("Soundtrack/Beep.mp3");
					switch (ch[0].direction)
					{
					case 1:
						ch[0].name->setSpriteFrame(sideBlink);
						break;
					case 2:
						ch[0].name->setSpriteFrame(backBlink);
						break;
					case 3:
						ch[0].name->setSpriteFrame(sideBlink);
						break;
					default:
						ch[0].name->setSpriteFrame(frontBlink);
						break;
					}
				}
				noFrameChange = true;
			}
			if (tickTock > maxTime)
			{
				tickTock = 0;
				amountOfShake += 20.f;
				ch[0].explode();
				splatterTime = 0.f;
				burnTime = 0.f;
				ch[0].name->setGlobalZOrder(300);
				ch[0].hasMoved = false;
				ch[0].name->setScale(192.f / ch[0].name->getContentSize().width);
				if (ch[0].movingBox >= 0)
				{
					boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
					boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
					boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
					ch[0].movingBox = -1;
					ch[0].toBox = Vec2(0, 0);
				}
			}
		}

		prep = cam->getPosition();

		updateInputs(dt);
	}
	ch[0].prev = ch[0].name->getPosition();
	INPUTS->clearForNextFrame();
}

void HelloWorld::updateInputs(float dt)
{
	//cout << getDefaultCamera()->getPosition().x << ", " << getDefaultCamera()->getPosition().y << endl;
	if (death)
		updateMouseInputs(dt);
	else
	{
		updateKeyboardInputs(dt);
		updateUpdatables(dt);
	}
}

void HelloWorld::tseq()
{
}

void HelloWorld::updateMouseInputs(float dt)
{
	if (iClicked && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
	{
		iClicked = false;
	}
	else if (!iClicked && INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
	{
		iClicked = true;
	}
	if (GGOpacity < 255)
	{
		GGOpacity += dt * 255.f;
		if (GGOpacity > 255)
			GGOpacity = 255.f;
		keepOnGoing->setOpacity(GGOpacity);
		keepOnGoingL->setOpacity(GGOpacity);
		giveUp->setOpacity(GGOpacity);
		giveUpL->setOpacity(GGOpacity);
		GG->setOpacity(GGOpacity);
	}
	if (keepOnGoingB.contains(INPUTS->getMousePosition()))
	{
		keepOnGoing->setScale(1.25f);
		keepOnGoingL->setScale(1.25f);
		if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
		{
			// code below checks which map has been selected and which background music will be played
			string word;
			string line;
			int offset;
			fstream NameMap;
			NameMap.open("CollectionOfS2DSaves.txt");
			Effect.deletemusic();
			if (NameMap.is_open())
			{
				while (!NameMap.eof())
				{
					getline(NameMap, line);
					if (mapName == "LEVEL 1" || mapName == "LEVEL 2" || mapName == "LEVEL 3" || mapName == "LEVEL 4")
					{
						Effect.Music("Soundtrack/Sector 1.mp3");
					}
					else if (mapName == "LEVEL 5")
					{
						Effect.Music("Soundtrack/Sector 1 boss.mp3");
					}
					else if (mapName == "LEVEL 6" || mapName == "LEVEL 7" || mapName == "LEVEL 8" || mapName == "LEVEL 9")
					{
						Effect.Music("Soundtrack/Sector 2.mp3");
					}
					else if (mapName == "LEVEL 10")
					{
						Effect.Music("Soundtrack/Sector 2 boss.mp3");
					}
					if (mapName == "LEVEL 11" || mapName == "LEVEL 12" || mapName == "LEVEL 13" || mapName == "LEVEL 14")
					{
						Effect.Music("Soundtrack/Sector 3.mp3");
					}
					if (mapName == "LEVEL 15")
					{
						Effect.Music("Soundtrack/Sector 3 boss.mp3");
					}
				}
				NameMap.close();
			}
			for (int i = 0; i < bosses.size(); i++)
			{
				bosses[i]->getBossSprite()->getPhysicsBody()->setEnabled(true);
				bosses[i]->spawn();
			}
			death = false;
			GGOpacity = 0;
			keepOnGoing->setOpacity(GGOpacity);
			keepOnGoingL->setOpacity(GGOpacity);
			giveUp->setOpacity(GGOpacity);
			giveUpL->setOpacity(GGOpacity);
			GG->setOpacity(GGOpacity);
			GG->setVisible(false);
			keepOnGoing->setVisible(false);
			keepOnGoingL->setVisible(false);
			giveUp->setVisible(false);
			giveUpL->setVisible(false);
			//if (explosionFrame >= maxPlode)
			//{
				ch[0].explosionFrame = 0;
				ch[0].boomTime = 0;
				ch[0].setAtCheckpoint();
				ch[0].tripped = false;
				ch[0].burned = false;
				ch[0].name->setScale(64.f / ch[0].name->getContentSize().width);
				ch[0].name->getPhysicsBody()->setEnabled(true);
				ch[0].name->getPhysicsBody()->setVelocity(Vec2(0, 0));
				ch[0].name->getPhysicsBody()->resetForces();
				ch[0].name->setGlobalZOrder(100);
				ch[0].direction = 0;
			//}
		}
	}
	else
	{
		keepOnGoing->setScale(1.0f);
		keepOnGoingL->setScale(1.0f);
	}
	if (giveUpB.contains(INPUTS->getMousePosition()))
	{
		giveUp->setScale(1.25f);
		giveUpL->setScale(1.25f);
		if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
		{
			Scene* s = PlayMap::createScene();
			Director::getInstance()->replaceScene(s);
		}
	}
	else
	{
		giveUp->setScale(1.0f);
		giveUpL->setScale(1.0f);
	}
}

void HelloWorld::updateKeyboardInputs(float dt)
{
	if (INPUTS->getKey(KeyCode::KEY_SPACE) && !ch[0].blowingUp)
	{
		if (!press)
		{
			press = true;
			if (ch[0].movingBox >= 0)
			{
				boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
				boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
				boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
				boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
				ch[0].movingBox = -1;
				ch[0].toBox = Vec2(0, 0);
			}
			else
			{
				for (int i = 0; i < boxes.size(); i++)
				{
					if (ch[0].name->getPosition().y - boxes[i]->getPosition().y > 0 && ch[0].name->getPosition().y - boxes[i]->getPosition().y < 40.f && abs(ch[0].name->getPosition().x - boxes[i]->getPosition().x) < 16.f && ch[0].direction == 0)
					{
						ch[0].movingBox = i;
						ch[0].toBox = boxes[i]->getPosition() - ch[0].name->getPosition();
						boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(1);
						boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(2);
						int locX = floor((boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2);
						int locY = floor((boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64);
						bPrep = Vec2(locX, locY);
						boxes[ch[0].movingBox]->setColor(Color3B(0, 255, 0));
					}
					else if (ch[0].name->getPosition().y - boxes[i]->getPosition().y < 0 && ch[0].name->getPosition().y - boxes[i]->getPosition().y > -40.f && abs(ch[0].name->getPosition().x - boxes[i]->getPosition().x) < 16.f && ch[0].direction == 2)
					{
						ch[0].movingBox = i;
						ch[0].toBox = boxes[i]->getPosition() - ch[0].name->getPosition();
						boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(1);
						boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(2);
						int locX = floor((boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2);
						int locY = floor((boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64);
						bPrep = Vec2(locX, locY);
						boxes[ch[0].movingBox]->setColor(Color3B(0, 255, 0));
					}
					else if (ch[0].name->getPosition().x - boxes[i]->getPosition().x > 0 && ch[0].name->getPosition().x - boxes[i]->getPosition().x < 56.f && abs(ch[0].name->getPosition().y - boxes[i]->getPosition().y) < 10.f && ch[0].direction == 1)
					{
						ch[0].movingBox = i;
						ch[0].toBox = boxes[i]->getPosition() - ch[0].name->getPosition();
						boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(1);
						boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(2);
						int locX = floor((boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2);
						int locY = floor((boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64);
						bPrep = Vec2(locX, locY);
						boxes[ch[0].movingBox]->setColor(Color3B(0, 255, 0));
					}
					else if (ch[0].name->getPosition().x - boxes[i]->getPosition().x < 0 && ch[0].name->getPosition().x - boxes[i]->getPosition().x > -56.f && abs(ch[0].name->getPosition().y - boxes[i]->getPosition().y) < 10.f && ch[0].direction == 3)
					{
						ch[0].movingBox = i;
						ch[0].toBox = boxes[i]->getPosition() - ch[0].name->getPosition();
						boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(1);
						boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(2);
						int locX = floor((boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2);
						int locY = floor((boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64);
						bPrep = Vec2(locX, locY);
						boxes[ch[0].movingBox]->setColor(Color3B(0, 255, 0));
					}
				}

				int locX = floor((ch[0].name->getPosition().x + 32.f) / 64 + maximumW / 2);
				int locY = floor((ch[0].name->getPosition().y + 16.f) / 64);
				if (locX >= 0 && locX < maximumW && locY >= 0 && locY <= maximumH)
				{
					for (int i = 0; i < tiles[locX][locY].size(); i++)
					{
						if (tiles[locX][locY][i]->getTag() == 14)
						{
							nextTileUpdate.push_back(Vec2(locX, locY));
							if (updateGrid[locX][locY][0])
							{
								Effect.Sound("Soundtrack/switch 2.mp3");
								nextTileBOOL.push_back(false);
								tiles[locX][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lever_off.png"));
								updateGrid[locX][locY][0] = false;
							}
							else
							{
								Effect.Sound("Soundtrack/switch 2.mp3");
								nextTileBOOL.push_back(true);
								tiles[locX][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lever_on.png"));
								updateGrid[locX][locY][0] = true;
							}
						}
					}
				}

				if (locX >= 0 && locX < maximumW && locY >= 0 && locY <= maximumH)
				{
					for (int i = 0; i < tiles[locX][locY].size(); i++)
					{
						if (tiles[locX][locY][i]->getTag() == 2)
						{
							if ((!updateGrid[locX][locY][0]) && (!updateGrid[locX][locY][1]) && (!updateGrid[locX][locY][2]) && (!updateGrid[locX][locY][3]) && (!updateGrid[locX][locY][4]))
							{
								Effect.Sound("Soundtrack/game button.mp3");
								nextTileUpdate.push_back(Vec2(locX, locY));
								nextTileBOOL.push_back(true);
								buttonUpdate.push_back(Vec3(locX, locY, 3.f));
								tiles[locX][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button_pressed.png"));
							}
							updateGrid[locX][locY][0] = true;
						}
					}
				}
				if (ch[0].direction == 2)
				{
					if (locX >= 0 && locX < maximumW && locY >= 0 && locY <= maximumH - 1)
					{
						for (int i = 0; i < tiles[locX][locY + 1].size(); i++)
						{
							if (tiles[locX][locY + 1][i]->getTag() == 2)
							{
								if ((!updateGrid[locX][locY + 1][0]) && (!updateGrid[locX][locY + 1][1]) && (!updateGrid[locX][locY + 1][2]) && (!updateGrid[locX][locY + 1][3]) && (!updateGrid[locX][locY + 1][4]))
								{
									Effect.Sound("Soundtrack/game button.mp3");
									nextTileUpdate.push_back(Vec2(locX, locY + 1));
									nextTileBOOL.push_back(true);
									buttonUpdate.push_back(Vec3(locX, locY + 1, 3.f));
									tiles[locX][locY + 1][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button_pressed.png"));
								}
								updateGrid[locX][locY + 1][0] = true;
							}
						}
					}
				}
				else if (ch[0].direction == 0)
				{
					if (locX >= 0 && locX < maximumW && locY >= 1 && locY <= maximumH)
					{
						for (int i = 0; i < tiles[locX][locY - 1].size(); i++)
						{
							if (tiles[locX][locY - 1][i]->getTag() == 2)
							{
								if ((!updateGrid[locX][locY - 1][0]) && (!updateGrid[locX][locY - 1][1]) && (!updateGrid[locX][locY - 1][2]) && (!updateGrid[locX][locY - 1][3]) && (!updateGrid[locX][locY - 1][4]))
								{
									Effect.Sound("Soundtrack/game button.mp3");
									nextTileUpdate.push_back(Vec2(locX, locY - 1));
									nextTileBOOL.push_back(true);
									buttonUpdate.push_back(Vec3(locX, locY - 1, 3.f));
									tiles[locX][locY - 1][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button_pressed.png"));
								}
								updateGrid[locX][locY - 1][0] = true;
							}
						}
					}
				}
				else if (ch[0].direction == 1)
				{
					if (locX >= 1 && locX < maximumW && locY >= 0 && locY <= maximumH)
					{
						for (int i = 0; i < tiles[locX - 1][locY].size(); i++)
						{
							if (tiles[locX - 1][locY][i]->getTag() == 2)
							{
								if ((!updateGrid[locX - 1][locY][0]) && (!updateGrid[locX - 1][locY][1]) && (!updateGrid[locX - 1][locY][2]) && (!updateGrid[locX - 1][locY][3]) && (!updateGrid[locX - 1][locY][4]))
								{
									Effect.Sound("Soundtrack/game button.mp3");
									nextTileUpdate.push_back(Vec2(locX - 1, locY));
									nextTileBOOL.push_back(true);
									buttonUpdate.push_back(Vec3(locX - 1, locY, 3.f));
									tiles[locX - 1][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button_pressed.png"));
								}
								updateGrid[locX - 1][locY][0] = true;
							}
						}
					}
				}
				else if (ch[0].direction == 3)
				{
					if (locX >= 0 && locX < maximumW - 1 && locY >= 0 && locY <= maximumH)
					{
						for (int i = 0; i < tiles[locX + 1][locY].size(); i++)
						{
							if (tiles[locX + 1][locY][i]->getTag() == 2)
							{
								if ((!updateGrid[locX + 1][locY][0]) && (!updateGrid[locX + 1][locY][1]) && (!updateGrid[locX + 1][locY][2]) && (!updateGrid[locX + 1][locY][3]) && (!updateGrid[locX + 1][locY][4]))
								{
									Effect.Sound("Soundtrack/game button.mp3");
									nextTileUpdate.push_back(Vec2(locX + 1, locY));
									nextTileBOOL.push_back(true);
									buttonUpdate.push_back(Vec3(locX + 1, locY, 3.f));
									tiles[locX + 1][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button_pressed.png"));
								}
								updateGrid[locX + 1][locY][0] = true;
							}
						}
					}
				}
			}
		}
	}
	else
		press = false;

	if (!ch[0].blowingUp)
	{
		int locX3 = floor((ch[0].name->getPosition().x + 32.f) / 64 + maximumW / 2);
		int locY3 = floor((ch[0].name->getPosition().y + 16.f) / 64);
		ch[0].name->setColor(Color3B(tiles[locX3][locY3][0]->getColor().r, tiles[locX3][locY3][0]->getColor().g, tiles[locX3][locY3][0]->getColor().b));
		for (int i = 0; i < tiles[locX3][locY3].size(); i++)
		{
			if (tiles[locX3][locY3][i]->getTag() == 12)
			{
				Effect.Sound("Soundtrack/Victory.mp3");
				Effect.deletemusic();
				won = true;
				displayStuff->setPosition(cam->getPosition());
				displayStuff->setVisible(true);
				clickToContinue->setPosition(cam->getPosition() - Vec2(0, 100));
				clickToContinue->setVisible(true);
				clickToContinue->setOpacity(0);
				for (int j = 0; j < ch.size(); j++)
					ch[0].name->setVisible(false);
				Vec2 scru = prep - visibleSizeGlobal / 2;
				Vec2 scrd = prep + visibleSizeGlobal / 2;
				lowerAnimX = scru.x / 64 - 1 + maximumW / 2;
				lowerAnimY = scru.y / 64 - 1;
				upperAnimX = scrd.x / 64 + 2 + maximumW / 2;
				upperAnimY = scrd.y / 64 + 2;
				moveAnimX = lowerAnimX;
				moveAnimY = lowerAnimY;
				for (int i = 0; i < boxes.size(); i++)
					boxes[i]->setVisible(false);
				for (int i = 0; i < bosses.size(); i++)
					bosses[i]->getBossSprite()->setVisible(false);
				for (int i = 0; i < isLazer.size(); i++)
				{
					lazerz[i][0]->setVisible(false);
					lazerz[i][1]->setVisible(false);
				}
			}
			if ((tiles[locX3][locY3][i]->getTag() == 15 && tiles[locX3][locY3][i]->getOpacity() > 0 && !ch[0].tripped && !ch[0].burned) || (tiles[locX3][locY3][i]->getTag() == 60 && tiles[locX3][locY3][i]->isVisible() && !ch[0].tripped && !ch[0].burned))
			{
				ch[0].tripped = true;
				ch[0].mockVel = ch[0].name->getPhysicsBody()->getVelocity();
				if (ch[0].mockVel.length() > 0.1f)
					ch[0].mockVel.normalize();
				else
					ch[0].mockVel = Vec2(0, 0);
			}
			currentSignSprite->setVisible(false);
			//cout << "AM NOW HERE" << endl;
			if (tiles[locX3][locY3][i]->getTag() == 18)
			{
				for (int j = 0; j < signLocations.size(); j++)
				{
					if ((int)signLocations[j].x == locX3 && (int)signLocations[j].y == locY3)
					{
						currentSignSprite->setVisible(true);
						currentSignSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(signSprites[j]));
						//cout << "AM HERE" << endl;
					}
				}
			}
		}
		if (locY3 < maximumH - 1)
			for (int i = 0; i < tiles[locX3][locY3 + 1].size(); i++)
			{
				for (int j = 0; j < signLocations.size(); j++)
				{
					if ((int)signLocations[j].x == locX3 && (int)signLocations[j].y == locY3 + 1)
					{
						currentSignSprite->setVisible(true);
						currentSignSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(signSprites[j]));
						//cout << "AM HERE" << endl;
					}
					else if (tiles[locX3][locY3][0]->getName() == "1")
					{
						if ((tiles[locX3][locY3 + 1][i]->getTag() == 15 && tiles[locX3][locY3 + 1][i]->getOpacity() > 0 && !ch[0].tripped && !ch[0].burned) || (tiles[locX3][locY3 + 1][i]->getTag() == 60 && tiles[locX3][locY3 + 1][i]->isVisible() && !ch[0].tripped && !ch[0].burned))
						{
							ch[0].tripped = true;
							ch[0].mockVel = ch[0].name->getPhysicsBody()->getVelocity();
							if (ch[0].mockVel.length() > 0.1f)
								ch[0].mockVel.normalize();
							else
								ch[0].mockVel = Vec2(0, 0);
						}
					}
				}
			}
		/*for (int i = 0; i < tiles[locX3][locY3].size(); i++)
		{
			if (!won && tiles[locX3][locY3][i]->getTag() == 16 && locY3 < maximumH - 3)
			{
				if (ch[0].name->getPosition().y > tiles[locX3][locY3][0]->getPosition().y + 10.f)
					ch[0].name->setPosition(tiles[locX3][locY3 + 2][0]->getPosition() - Vec2(0, 10.f));
			}
		}
		if (locY3 > 2)
			for (int i = 0; i < tiles[locX3][locY3 - 1].size(); i++)
			{
				if (!won && tiles[locX3][locY3 - 1][i]->getTag() == 16)
				{
					ch[0].name->setPosition(tiles[locX3][locY3 - 1][0]->getPosition() - Vec2(0, 16.f));
				}
			}*/
		ch[0].name->setGlobalZOrder(3.00008f - 0.0001f * ch[0].name->getPosition().y);
		if (ch[0].movingBox >= 0)
			ch[0].name->setPosition(boxes[ch[0].movingBox]->getPosition() - ch[0].toBox);
		Vec2 tot = Vec2(0.f, 0.f);
		Vec2 antitot = Vec2(0.f, 0.f);
		if (ch[0].burned)
		{
			singe(dt);
		}
		else if (ch[0].tripped)
		{
			splatter(dt);
		}
		else if (INPUTS->getKey(KeyCode::KEY_W) || INPUTS->getKey(KeyCode::KEY_S) || INPUTS->getKey(KeyCode::KEY_A) || INPUTS->getKey(KeyCode::KEY_D) || INPUTS->getKey(KeyCode::KEY_UP_ARROW) || INPUTS->getKey(KeyCode::KEY_DOWN_ARROW) || INPUTS->getKey(KeyCode::KEY_LEFT_ARROW) || INPUTS->getKey(KeyCode::KEY_RIGHT_ARROW))
		{
			beMovin = true;
			ch[0].hasMoved = true;
			ch[0].incrementWalk(dt);
			if (INPUTS->getKey(KeyCode::KEY_W) || INPUTS->getKey(KeyCode::KEY_UP_ARROW))
			{
				tot += Vec2(0.f, 1.f);
				if (ch[0].movingBox < 0)
				{
					ch[0].direction = 2;
					ch[0].invert = false;
				}
			}
			if (INPUTS->getKey(KeyCode::KEY_S) || INPUTS->getKey(KeyCode::KEY_DOWN_ARROW))
			{
				tot += Vec2(0.f, -1.f);
				if (ch[0].movingBox < 0)
				{
					ch[0].direction = 0;
					ch[0].invert = false;
				}
			}
			if (INPUTS->getKey(KeyCode::KEY_A) || INPUTS->getKey(KeyCode::KEY_LEFT_ARROW))
			{
				tot += Vec2(-1.f, 0.f);
				if (ch[0].movingBox < 0)
				{
					ch[0].direction = 1;
					ch[0].invert = true;
				}
			}
			if (INPUTS->getKey(KeyCode::KEY_D) || INPUTS->getKey(KeyCode::KEY_RIGHT_ARROW))
			{
				tot += Vec2(1.f, 0.f);
				if (ch[0].movingBox < 0)
				{
					ch[0].direction = 3;
					ch[0].invert = false;
				}
			}
			tot.normalize();

			tot = tot * 2.f * ch[0].acc;
			if (ch[0].invert)
				ch[0].name->setScaleX(abs(ch[0].name->getScaleX()) * -1.f);
			else
				ch[0].name->setScaleX(abs(ch[0].name->getScaleX()) * 1.f);
		}
		else
		{
			ch[0].resetWalk();
			beMovin = false;
		}
		if (!noFrameChange && !ch[0].tripped && !ch[0].burned)
			ch[0].name->setSpriteFrame(ch[0].walk[ch[0].direction][ch[0].walkNum]);
		else if (ch[0].tripped && !ch[0].burned && splatterTime >= 4)
			ch[0].name->setSpriteFrame(ch[0].trip[ch[0].direction][5]);
		if (ch[0].name->getPhysicsBody()->getVelocity().length() > 0.01f)
		{
			antitot = -ch[0].name->getPhysicsBody()->getVelocity();
			antitot.normalize();
			if (ch[0].acc > ch[0].name->getPhysicsBody()->getVelocity().length() * 100.f / dt)
			{
				antitot *= ch[0].name->getPhysicsBody()->getVelocity().length() * 100.f / dt;
			}
			else
			{
				antitot = antitot * ch[0].acc;
			}
		}
		tot += antitot;

		tileCollisionCheck(dt, &tot);

		float mult = 240.0f;
		if ((INPUTS->getKey(KeyCode::KEY_RIGHT_SHIFT) || INPUTS->getKey(KeyCode::KEY_LEFT_SHIFT)) && ch[0].movingBox < 0)
		{
			ch[0].incrementWalk(dt);
			mult = 480.f;
		}
		if (ch[0].movingBox >= 0)
		{
			bool itHasBeenSet = false;
			for (int k = 0; k < tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64].size(); k++)
			{
				if (tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64][k]->getTag() == 15 && tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64][k]->getOpacity() > 0)
				{
					mult = 20.f;
					boxGoopMinor = true;
					itHasBeenSet = true;
				}
				else if (tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64][k]->getTag() == 60 && tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64][k]->isVisible())
				{
					mult = 25.f;
					boxGoopMinor = true;
					itHasBeenSet = true;
				}
				else
				{
					boxGoopMinor = false;
				}
			}
			if (!itHasBeenSet && (boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 < maximumH - 1)
			{
				for (int k = 0; k < tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 + 1].size(); k++)
				{
					if (tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64][0]->getName() == "1")
					{
						if (tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 + 1][k]->getTag() == 15 && tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 + 1][k]->getOpacity() > 0)
						{
							mult = 20.f;
							boxGoopMinor = true;
							itHasBeenSet = true;
						}
						else if (tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 + 1][k]->getTag() == 60 && tiles[(boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2][(boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64 + 1][k]->isVisible())
						{
							mult = 25.f;
							boxGoopMinor = true;
							itHasBeenSet = true;
						}
						else
						{
							boxGoopMinor = false;
						}
					}
				}
			}
		}
		ch[0].name->getPhysicsBody()->applyForce(tot);
		if (ch[0].name->getPhysicsBody()->getVelocity().length() > mult)
		{
			Vec2 sub = ch[0].name->getPhysicsBody()->getVelocity();
			if (sub.length() > 0.01f)
				sub.normalize();
			else
				sub = Vec2(0, 0);
			sub *= mult;
			ch[0].name->getPhysicsBody()->setVelocity(sub);
		}
		if (ch[0].tripped && ch[0].movingBox >= 0)
		{
			boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
			ch[0].name->getPhysicsBody()->setVelocity(Vec2(0, 0));
			boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
			boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
			boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
			ch[0].movingBox = -1;
			ch[0].toBox = Vec2(0, 0);
		}
		if (ch[0].burned && ch[0].movingBox >= 0)
		{
			boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
			ch[0].name->getPhysicsBody()->setVelocity(Vec2(0, 0));
			boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
			boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
			boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
			ch[0].movingBox = -1;
			ch[0].toBox = Vec2(0, 0);
		}
		if (ch[0].movingBox >= 0)
		{
			boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
			boxes[ch[0].movingBox]->setPosition(ch[0].name->getPosition() + ch[0].toBox + ch[0].name->getPhysicsBody()->getVelocity() * dt);
			boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(1);
			boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(2);
			for (int i = boxes[ch[0].movingBox]->getPosition().x / 64 - 2; i < boxes[ch[0].movingBox]->getPosition().x / 64 + 2; i++)
			{
				for (int j = boxes[ch[0].movingBox]->getPosition().y / 64 - 2; j < boxes[ch[0].movingBox]->getPosition().y / 64 + 2; j++)
				{
					if (i >= -maximumW / 2 && i < maximumW / 2 && j >= 0 && j < maximumH)
					{
						for (int k = 0; k < tiles[i + maximumW / 2][j].size(); k++)
							if (tiles[i + maximumW / 2][j][k]->getTag() == 1 || tiles[i + maximumW / 2][j][k]->getTag() == 6 || (tiles[i + maximumW / 2][j][k]->getTag() >= 20 && tiles[i + maximumW / 2][j][k]->getTag() <= 23) || tiles[i + maximumW / 2][j][k]->getTag() == 25)
								if (tiles[i + maximumW / 2][j][k]->getPhysicsBody()->isEnabled())
								{
									if (abs(tiles[i + maximumW / 2][j][k]->getPosition().x - boxes[ch[0].movingBox]->getPosition().x) < 58.f && abs(tiles[i + maximumW / 2][j][k]->getPosition().y - boxes[ch[0].movingBox]->getPosition().y) < 32.f)
									{
										boxes[ch[0].movingBox]->setPosition(boxes[ch[0].movingBox]->getPosition() - ch[0].name->getPhysicsBody()->getVelocity() * dt);
										boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box_angry.png"));
										//boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
										//boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
									}
								}
					}
				}
			}
			for (int i = 0; i < boxes.size(); i++)
			{
				if (abs(boxes[i]->getPosition().x - boxes[ch[0].movingBox]->getPosition().x) < 52.f && abs(boxes[i]->getPosition().y - boxes[ch[0].movingBox]->getPosition().y) < 32.f && i != ch[0].movingBox)
				{
					boxes[ch[0].movingBox]->setPosition(boxes[ch[0].movingBox]->getPosition() - ch[0].name->getPhysicsBody()->getVelocity() * dt);
					boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box_angry.png"));
					//boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
					//boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
				}
			}
			boxes[ch[0].movingBox]->setGlobalZOrder(3.00006f - 0.0001f * boxes[ch[0].movingBox]->getPosition().y);
			Vec2 bp = boxes[ch[0].movingBox]->getPosition();
			int locX = floor((boxes[ch[0].movingBox]->getPosition().x + 32.f) / 64 + maximumW / 2);
			int locY = floor((boxes[ch[0].movingBox]->getPosition().y + 16.f) / 64);
			if (locX != (int)bPrep.x || locY != (int)bPrep.y)
			{
				for (int i = 0; i < tiles[(int)bPrep.x][(int)bPrep.y].size(); i++)
				{
					if (tiles[(int)bPrep.x][(int)bPrep.y][i]->getTag() == 10)
					{
						bool gateKeeper = true;
						for (int j = 0; j < boxes.size(); j++)
						{
							int locX2 = floor((boxes[j]->getPosition().x + 32.f) / 64 + maximumW / 2);
							int locY2 = floor((boxes[j]->getPosition().y + 16.f) / 64);
							if (locX2 == (int)bPrep.x && locY2 == (int)bPrep.y && j != ch[0].movingBox)
							{
								gateKeeper = false;
							}
						}
						if (gateKeeper)
						{
							nextTileUpdate.push_back(Vec2((int)bPrep.x, (int)bPrep.y));
							nextTileBOOL.push_back(false);
							tiles[(int)bPrep.x][(int)bPrep.y][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_pressure_plate.png"));
						}
					}
				}
				for (int i = 0; i < tiles[locX][locY].size(); i++)
				{
					if (tiles[locX][locY][i]->getTag() == 10)
					{
						Effect.Sound("Soundtrack/switch 1.mp3");
						nextTileUpdate.push_back(Vec2(locX, locY));
						nextTileBOOL.push_back(true);
						tiles[locX][locY][i]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_pressure_plate_pressed.png"));
					}
				}
			}
			bPrep = Vec2(locX, locY);

			if (abs(boxes[ch[0].movingBox]->getPosition().x - ch[0].name->getPosition().x) > 64.f || abs(boxes[ch[0].movingBox]->getPosition().y - ch[0].name->getPosition().y) > 48.f)
			{
				boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
				boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
				boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
				boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
				ch[0].movingBox = -1;
				ch[0].toBox = Vec2(0, 0);
			}
		}
	}
	else
	{
		ch[0].explosionSequence(dt);
		ch[0].name->setSpriteFrame(ch[0].splode[ch[0].explosionFrame]);
	}
	
	//Camera* camera = Camera::create();
	//camera->getDefaultCamera();
	//camera->setCameraFlag(CameraFlag::USER1);
	//this->addChild(camera);
	//cam = HelloWorld::getDefaultCamera();
	cam->setPosition(ch[0].name->getPosition());
	GG->setPosition(cam->getPosition());
	keepOnGoing->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f + Vec2(visibleSizeGlobal.x * 0.3f, visibleSizeGlobal.y * 0.25f));
	keepOnGoingL->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f + Vec2(visibleSizeGlobal.x * 0.3f, visibleSizeGlobal.y * 0.25f));
	giveUp->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f + Vec2(visibleSizeGlobal.x * 0.7f, visibleSizeGlobal.y * 0.25f));
	giveUpL->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f + Vec2(visibleSizeGlobal.x * 0.7f, visibleSizeGlobal.y * 0.25f));
	
	currentSignSprite->setPosition(cam->getPosition() + Vec2(currentSignSprite->getContentSize().width * currentSignSprite->getScale() * 0.5f - visibleSizeGlobal.x * 0.5, visibleSizeGlobal.y * 0.5 - currentSignSprite->getContentSize().height * currentSignSprite->getScale() * 0.5f));
	if (inTime > 0)
	{
		loding->setPosition(cam->getPosition());
		blackScreen->setPosition(cam->getPosition() - visibleSizeGlobal * 0.5f);
	}
	if (boxGoopMinor && !boxGoopMajor)
	{
		Effect.Sound("Soundtrack/Goop.mp3");
		boxGoopMajor = true;
	}
	else if (!boxGoopMinor && boxGoopMajor)
	{
		boxGoopMajor = false;
	}
	shakey();
	//getDefaultCamera()->setPosition(ch[0].name->getPosition());
	
}

void HelloWorld::updateUpdatables(float dt)
{
	tileUpdateList();
	triggerLight(dt);
	opacityUp(dt);
	nextTileUpdateList();
	nextTriggerLight(dt);
	nextOpacityUp(dt);
	buttonUpdateList(dt);
	doorOpener(dt);
	triggerTimers(dt);
	mintyGoop(dt);
	if (ch[0].hasMoved && !ch[0].blowingUp)
	{
		moveBosses(dt);
	}
	else if (ch[0].blowingUp)
	{
		for (int i = 0; i < bosses.size(); i++)
		{
			bosses[i]->getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
		}
	}
	if (stopBlackOut)
		turnOnAllLights();
	else
		turnOffAllLights();
}

void HelloWorld::tileCollisionCheck(float dt, Vec2 *tot)
{
	
}

void HelloWorld::regTile(int tilex, int tiley, int tilez, std::string tileSprite)
{

}

void HelloWorld::physTile(int tilex, int tiley, int tilez, std::string tileSprite)
{
	PhysicsBody* pb = PhysicsBody::createBox(tiles[tilex][tiley][tilez]->getContentSize(), PhysicsMaterial(0.01f, 0.01f, 0.01f));
	pb->setDynamic(false);
	tiles[tilex][tiley][tilez]->setPhysicsBody(pb);
	//tiles[tilex][tiley][tilez]->getPhysicsBody()->setCollisionBitmask(2);
	//tiles[tilex][tiley][tilez]->getPhysicsBody()->setCategoryBitmask(1);
}

void HelloWorld::NPC(int x, int y, std::string NPCSprite)
{
	character npc;
	npc.name = Sprite::create(NPCSprite);
	npc.name->setScale(64.f / npc.name->getContentSize().height);
	npc.name->setAnchorPoint(Vec2(0.5f, 0.5f));
	npc.name->setPosition(x * 64 - fwidth * 32, y * 64 - fheight * 32);
	PhysicsBody* pb = PhysicsBody::createCircle(npc.name->getContentSize().width / 2);
	pb->setDynamic(false);
	npc.name->setPhysicsBody(pb);
	//npc.name->getPhysicsBody()->setCollisionBitmask(1);
	//npc.name->getPhysicsBody()->setCategoryBitmask(2);
	ch.push_back(npc);
	this->addChild(ch[ch.size() - 1].name, 2);
}

void HelloWorld::LoadingMap()
{
	int starter = 0;
	ifstream saveFile;
	string *toOpen;
	saveFile.open(mapName + "S2DF.txt");
	string s;
	vector <string> ss;
	while (getline(saveFile, s))
	{
		stringstream into(s);
		while (getline(into, s, '/'))
		{
			ss.push_back(s);
		}
		for (int i = tiles[starter % maximumW][starter / maximumW].size() - 1; i >= 0; i--)
		{
			tiles[starter % maximumW][starter / maximumW][i]->setVisible(false);
			this->removeChild(tiles[starter % maximumW][starter / maximumW][i]);
		}
		tiles[starter % maximumW][starter / maximumW].clear();
		for (int i = 0; i < ss.size(); i++)
		{
			string a = ss[i];
			stringstream into2(a);
			vector <string> sss;
			while (getline(into2, a, '='))
			{
				sss.push_back(a);
			}
			Sprite* temp = Sprite::createWithSpriteFrame(all[stoi(sss[0])]->getSpriteFrame());
			temp->getTexture()->setTexParameters(texParams);
			temp->setName(sss[0]);
			temp->setScale(64.f / temp->getContentSize().width);
			temp->setPosition((starter % maximumW - maximumW / 2) * 64.f, (starter / maximumW) * 64.f);
			temp->setGlobalZOrder(stoi(sss[1]));
			temp->setRotation(stoi(sss[2]) * 90);
			temp->setTag(stoi(sss[3]));
			//if (sss[3] == "1")
			//{
			//	PhysicsBody* pb = PhysicsBody::createBox(temp->getContentSize(), PhysicsMaterial(0.01f, 0.01f, 0.01f));
			//	pb->setDynamic(false);
			//	pb->setEnabled(false);
			//	temp->setPhysicsBody(pb);
			//	temp->getPhysicsBody()->setCollisionBitmask(2);
			//	temp->getPhysicsBody()->setCategoryBitmask(1);
			//}
			//else if (sss[3] == "2")
			//{
			//	//temp->runAction(openDoor);
			//}
			if (temp->getTag() == 5)
			{
				boxes.push_back(temp);
				originalBoxPositions.push_back(temp->getPosition());
			}
			else if (temp->getTag() == 29)
			{
				vector<vector<SpriteFrame*>> wc;
				vector<vector<SpriteFrame*>> dc;
				for (int k = 0; k < 4; k++)
				{
					vector<SpriteFrame*> wcc;
					vector<SpriteFrame*> dcc;
					for (int j = 0; j < 6; j++)
					{
						if (k == 0)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_walk_down_" + to_string(j) + ".png"));
							dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_die_down_" + to_string(j) + ".png"));
						}
						else if (k == 1)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_walk_side_" + to_string(j) + ".png"));
							dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_die_side_" + to_string(j) + ".png"));
						}
						else if (k == 2)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_walk_up_" + to_string(j) + ".png"));
							dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_die_up_" + to_string(j) + ".png"));
						}
						else if (k == 3)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_walk_side_" + to_string(j) + ".png"));
							dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("minty_die_side_" + to_string(j) + ".png"));
						}
					}
					wc.push_back(wcc);
					dc.push_back(dcc);
				}
				boss* disGuy = new Minty(this, temp->getPosition(), wc, 800.f, 0);
				disGuy->loadDeathCycle(dc);
				bosses.push_back(disGuy);
				mintyTime = true;
			}
			else if (temp->getTag() == 31)
			{
				vector<vector<SpriteFrame*>> wc;
				vector<vector<SpriteFrame*>> dc;
				for (int k = 0; k < 4; k++)
				{
					vector<SpriteFrame*> wcc;
					vector<SpriteFrame*> dcc;
					if (k == 0)
					{
						wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_down.png"));
					}
					else if (k == 1)
					{
						wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_side.png"));
					}
					else if (k == 2)
					{
						wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_up.png"));
					}
					else if (k == 3)
					{
						wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_side.png"));
					}
					for (int j = 0; j < 3; j++)
						dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_die_" + to_string(j) + ".png"));
					wc.push_back(wcc);
					dc.push_back(dcc);
				}
				vector<Sprite*> tempTemp;
				for (int k = 0; k < 5; k++)
				{
					Sprite* tempTempTemp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
					Sprite* TempTempTemp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("priism_target.png"));
					tempTemp.push_back(tempTempTemp);
					tempTemp.push_back(TempTempTemp);
				}
				boss* disGuy = new PRIISM(this, temp->getPosition(), wc, 1400.f, 0, tempTemp);
				disGuy->loadDeathCycle(dc);
				bosses.push_back(disGuy);
			}
			else if (temp->getTag() == 33)
			{
				vector<vector<SpriteFrame*>> wc;
				vector<vector<SpriteFrame*>> dc;
				for (int k = 0; k < 4; k++)
				{
					vector<SpriteFrame*> wcc;
					vector<SpriteFrame*> dcc;
					for (int j = 1; j < 13; j++)
					{
						string ssss = "";
						if (j < 10)
							ssss = "0";
						if (k == 0)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("ceo_down_" + ssss + to_string(j) + ".png"));
						}
						else if (k == 1)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("ceo_side_" + ssss + to_string(j) + ".png"));
						}
						else if (k == 2)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("ceo_up_" + ssss + to_string(j) + ".png"));
						}
						else if (k == 3)
						{
							wcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("ceo_side_" + ssss + to_string(j) + ".png"));
						}
					}
					for (int j = 0; j < 4; j++)
						dcc.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("ceo_die_side_" + to_string(j) + ".png"));
					wc.push_back(wcc);
					dc.push_back(dcc);
				}
				vector<SpriteFrame*> monos;
				monos.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("monocle.png"));
				for (int i = 0; i < 4; i++)
				{
					monos.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("monocle_hit_" + to_string(i) + ".png"));
				}
				monos.push_back(SpriteFrameCache::getInstance()->getSpriteFrameByName("monocle.png"));
				boss* disGuy = new CEO(this, temp->getPosition(), wc, 1400.f, 0, monos);
				disGuy->loadDeathCycle(dc);
				bosses.push_back(disGuy);
			}
			else
				tiles[starter % maximumW][starter / maximumW].push_back(temp);
			//this->addChild(tiles[starter % maximumW][starter / maximumW][i]);
		}
		ss.clear();
		starter++;
	}
	for (int i = 0; i < maximumW * maximumH; i++)
	{

		if (mintyTime)
		{
			Sprite* temp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_goop_puddle.png"));
			temp->getTexture()->setTexParameters(texParams);
			temp->setName("-1");
			temp->setScale(64.f / temp->getContentSize().width);
			temp->setPosition((i % maximumW - maximumW / 2) * 64.f, (i / maximumW) * 64.f);
			temp->setGlobalZOrder(-9);
			temp->setRotation(0);
			temp->setTag(60);
			tiles[i % maximumW][i / maximumW].push_back(temp);
		}
		for (int k = 0; k < tiles[i % maximumW][i / maximumW].size(); k++)
		{
			if (tiles[i % maximumW][i / maximumW][k]->getGlobalZOrder() <= 2)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(tiles[i % maximumW][i / maximumW][k]->getGlobalZOrder() - 10);
				
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() > 0)
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(3.f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
			if (tiles[i % maximumW][i / maximumW][k]->getName() == "1")
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(10);
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 2 || tiles[i % maximumW][i / maximumW][k]->getTag() == 6)
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(3.00005f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 10)
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(tiles[i % maximumW][i / maximumW][k]->getGlobalZOrder() - 10);
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 14)
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(2.99755f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 19)
			{
				timerz.push_back(Vec4(i % maximumW, i / maximumW, 0.5f * (1.f + tiles[i % maximumW][i / maximumW][k]->getRotation() / 90), 0.f));
				timerMode.push_back(false);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 17)
			{
				tiles[i % maximumW][i / maximumW][k]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("NothingTile.png"));
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(10);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 15)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(tiles[i % maximumW][i / maximumW][k]->getGlobalZOrder() - 9);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() >= 20 && tiles[i % maximumW][i / maximumW][k]->getTag() <= 23)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(3.00005f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
				std::vector<Sprite*> lazas;
				for (int j = 0; j < 2; j++)
				{
					Sprite* laza = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
					laza->setPosition(tiles[i % maximumW][i / maximumW][k]->getPosition());
					laza->setScale(64.f / laza->getContentSize().width);
					laza->setGlobalZOrder(0);
					laza->setVisible(false);
					lazas.push_back(laza);
				}
				lazerz.push_back(lazas);
				lazerLocs.push_back(Vec2(i % maximumW, i / maximumW));
				beamLocs.push_back(Vec2(i % maximumW, i / maximumW));
				lazerTime.push_back(0);
				isLazer.push_back(false);
				this->addChild(lazerz[lazerz.size() - 1][0]);
				this->addChild(lazerz[lazerz.size() - 1][1]);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 24)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(-5);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 25)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(3.f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 28)
			{
				blackedOut = true;
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 30)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(3.00005f - 0.0001f * tiles[i % maximumW][i / maximumW][k]->getPosition().y);
				bossDoorLocations.push_back(Vec2(i % maximumW, i / maximumW));
				bossDoorBOOL.push_back(false);
				bossDoorTimes.push_back(0.f);
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 32)
			{
				antiBlackOutSections.push_back(Vec2(i % maximumW, i / maximumW));
			}
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 60)
			{
				tiles[i % maximumW][i / maximumW][k]->setGlobalZOrder(-4);
			}

			tiles[i % maximumW][i / maximumW][k]->setVisible(false);
			if (tiles[i % maximumW][i / maximumW][k]->getTag() == 1 || tiles[i % maximumW][i / maximumW][k]->getTag() == 6 || (tiles[i % maximumW][i / maximumW][k]->getTag() >= 20 && tiles[i % maximumW][i / maximumW][k]->getTag() <= 23) || tiles[i % maximumW][i / maximumW][k]->getTag() == 25 || tiles[i % maximumW][i / maximumW][k]->getTag() == 30)
			{
				//cout << "HERE! " << i << endl;
				bool drop = false;
				if (i / maximumW < maximumH - 1)
				{
					drop = true;
					for (int a = 0; a < tiles[i % maximumW][i / maximumW + 1].size(); a++)
						if (tiles[i % maximumW][i / maximumW + 1][a]->getTag() == 1 || tiles[i % maximumW][i / maximumW + 1][a]->getTag() == 6 || (tiles[i % maximumW][i / maximumW + 1][a]->getTag() >= 20 && tiles[i % maximumW][i / maximumW + 1][a]->getTag() <= 23) || tiles[i % maximumW][i / maximumW + 1][a]->getTag() == 25 || tiles[i % maximumW][i / maximumW + 1][a]->getTag() == 30)
							drop = false;
				}
				//cout << "HERE2! " << i << endl;
				if (drop)
				{
					PhysicsBody* pb = PhysicsBody::createBox(Size(tiles[i % maximumW][i / maximumW][k]->getContentSize().width, tiles[i % maximumW][i / maximumW][k]->getContentSize().height / 2), PhysicsMaterial(0.01f, 0.01f, 0.01f));
					pb->setDynamic(false);
					pb->setEnabled(false);
					pb->setPositionOffset(Vec2(0, -tiles[i % maximumW][i / maximumW][k]->getContentSize().height * tiles[i % maximumW][i / maximumW][k]->getScale() * 0.25f));
					tiles[i % maximumW][i / maximumW][k]->setPhysicsBody(pb);
					tiles[i % maximumW][i / maximumW][k]->getPhysicsBody()->setCollisionBitmask(2);
					tiles[i % maximumW][i / maximumW][k]->getPhysicsBody()->setCategoryBitmask(1);
				}
				else
				{
					PhysicsBody* pb = PhysicsBody::createBox(tiles[i % maximumW][i / maximumW][k]->getContentSize(), PhysicsMaterial(0.01f, 0.01f, 0.01f));
					pb->setDynamic(false);
					pb->setEnabled(false);
					tiles[i % maximumW][i / maximumW][k]->setPhysicsBody(pb);
					tiles[i % maximumW][i / maximumW][k]->getPhysicsBody()->setCollisionBitmask(2);
					tiles[i % maximumW][i / maximumW][k]->getPhysicsBody()->setCategoryBitmask(1);
				}
			}
			this->addChild(tiles[i % maximumW][i / maximumW][k]);
		}
	}
	for (int i = 0; i < boxes.size(); i++)
	{
		PhysicsBody* pb = PhysicsBody::createBox(Size(boxes[i]->getContentSize().width * 0.9f, boxes[i]->getContentSize().height / 2), PhysicsMaterial(0.01f, 0.01f, 0.01f));
		pb->setDynamic(false);
		pb->setEnabled(false);
		pb->setPositionOffset(Vec2(0, -boxes[i]->getContentSize().height / 2));
		boxes[i]->setPhysicsBody(pb);
		boxes[i]->setVisible(false);
		boxes[i]->setGlobalZOrder(3.00006f - boxes[i]->getPosition().y * 0.0001f);
		boxes[i]->getPhysicsBody()->setCollisionBitmask(2);
		boxes[i]->getPhysicsBody()->setCategoryBitmask(1);
		boxDestroyed.push_back(false);
		this->addChild(boxes[i]);
	}
	for (int i = 0; i < bosses.size(); i++)
	{
		previousBossPositions.push_back(bosses[i]->getBossSprite()->getPosition());
		PhysicsBody* pb = PhysicsBody::createCircle(bosses[i]->getBossSprite()->getContentSize().height * 0.25f);
		pb->setRotationEnable(false);
		pb->setCollisionBitmask(1);
		pb->setCategoryBitmask(2);
		pb->setVelocity(Vec2(0, 0));
		pb->setPositionOffset(Vec2(0, -abs(bosses[i]->getBossSprite()->getContentSize().height * 0.5f)));
		bosses[i]->getBossSprite()->setGlobalZOrder(3.00008f - bosses[i]->getBossSprite()->getPosition().y * 0.0001f);
		bosses[i]->getBossSprite()->setPhysicsBody(pb);
		bosses[i]->getBossSprite()->setPosition(bosses[i]->getPosition());
		bosses[i]->addBossToScene(this);
	}
	saveFile.close();
	saveFile.open(mapName + "S2DW.txt");
	while (getline(saveFile, s))
	{
		stringstream into(s);
		while (getline(into, s, ','))
		{
			ss.push_back(s);
		}
		transmitters.push_back(Vec2(stoi(ss[0]), stoi(ss[1])));
		receivers.push_back(Vec2(stoi(ss[2]), stoi(ss[3])));
		ss.clear();
	}
	saveFile.close();
	saveFile.open(mapName + "S2DT.txt");
	getline(saveFile, s);
	stringstream into(s);
	while (getline(into, s, ','))
	{
		ss.push_back(s);
	}
	daStart = Vec2(stoi(ss[0]), stoi(ss[1]));
	daEnd = Vec2(stoi(ss[2]), stoi(ss[3]));
	ss.clear();
	saveFile.close();
	saveFile.open(mapName + "S2DS.txt");
	while (getline(saveFile, s))
	{
		stringstream into(s);
		while (getline(into, s, ','))
		{
			ss.push_back(s);
		}
		signLocations.push_back(Vec2(stoi(ss[0]), stoi(ss[1])));
		signSprites.push_back(ss[2]);
		ss.clear();
	}
	if (blackedOut)
	{
		for (int i = 0; i < maximumW * maximumH; i++)
		{
			for (int j = 0; j < tiles[i % maximumW][i / maximumW].size(); j++)
			{
				tiles[i % maximumW][i / maximumW][j]->setColor(Color3B(0, 0, 0));
				if (tiles[i % maximumW][i / maximumW][j]->getTag() == 27)
				{
					nextOpacityUpdate.push_back(Vec2(i % maximumW, i / maximumW));
				}
			}
		}
		for (int i = 0; i < boxes.size(); i++)
		{
			boxes[i]->setColor(Color3B(0, 0, 0));
		}
	}
	saveFile.close();
}

void HelloWorld::tileUpdateList()
{
	bool playLazer = false;
	int locX4 = floor((ch[0].name->getPosition().x + 32.f) / 64 + maximumW / 2);
	int locY4 = floor((ch[0].name->getPosition().y + 16.f) / 64);
	for (int i = 0; i < tileUpdate.size(); i++)
	{
		for (int j = 0; j < tiles[tileUpdate[i].x][tileUpdate[i].y].size(); j++)
		{
			if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 3)
			{
				for (int k = 0; k < transmitters.size(); k++)
				{
					if (transmitters[k] == tileUpdate[i] && tileBOOL[i] != updateGrid[(int)receivers[k].x][(int)receivers[k].y][5])
					{
						receiverOutput(tileBOOL[i], k);
					}
				}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 7)
			{
				for (int k = 0; k < transmitters.size(); k++)
				{
					if (transmitters[k] == tileUpdate[i])
					{
						int tempForBools = 0;
						for (int a = 0; a < 5; a++)
							if (updateGrid[tileUpdate[i].x][tileUpdate[i].y][a])
								tempForBools++;
						if (tileBOOL[i] && tempForBools >= 2 && (!updateGrid[(int)receivers[k].x][(int)receivers[k].y][5]))
						{
							receiverOutput(true, k);
						}
						else if ((!tileBOOL[i]) && tempForBools <= 1 && updateGrid[(int)receivers[k].x][(int)receivers[k].y][5])
						{
							receiverOutput(false, k);
						}
					}
				}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 8)
			{
				for (int k = 0; k < transmitters.size(); k++)
				{
					if (transmitters[k] == tileUpdate[i])
					{
						int tempForBools = 0;
						for (int a = 0; a < 5; a++)
							if (updateGrid[tileUpdate[i].x][tileUpdate[i].y][a])
								tempForBools++;
						if (tileBOOL[i] && tempForBools > 0 && (!updateGrid[(int)receivers[k].x][(int)receivers[k].y][5]))
						{
							receiverOutput(true, k);
						}
						else if ((!tileBOOL[i]) && tempForBools == 0 && updateGrid[(int)receivers[k].x][(int)receivers[k].y][5])
						{
							receiverOutput(false, k);
						}
					}
				}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 9)
			{
				for (int k = 0; k < transmitters.size(); k++)
				{
					if (transmitters[k] == tileUpdate[i])
					{
						int tempForBools = 0;
						for (int a = 0; a < 5; a++)
							if (updateGrid[tileUpdate[i].x][tileUpdate[i].y][a])
								tempForBools++;
						if (tempForBools % 2 == 1 && (!updateGrid[(int)receivers[k].x][(int)receivers[k].y][5]))
						{
							receiverOutput(true, k);
						}
						else if (tempForBools % 2 == 0 && updateGrid[(int)receivers[k].x][(int)receivers[k].y][5])
						{
							receiverOutput(false, k);
						}
					}
				}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 6)
			{
				int tempForBools = 0;
				for (int a = 0; a < 5; a++)
					if (updateGrid[tileUpdate[i].x][tileUpdate[i].y][a])
						tempForBools++;
				if (tileBOOL[i] && tempForBools <= 1)
				{
					openDoors.push_back(Vec3(tileUpdate[i].x, tileUpdate[i].y, 0.f));
					openBOOL.push_back(true);
				}
				else if ((!tileBOOL[i]) && tempForBools == 0)
				{
					openDoors.push_back(Vec3(tileUpdate[i].x, tileUpdate[i].y, 6.f * doorSpeed));
					openBOOL.push_back(false);
				}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 20 && tileBOOL[i])
			{
				if (abs(locX4 - tileUpdate[i].x) < 10 && abs(locY4 - tileUpdate[i].y) < 8)
					playLazer = true;
				for (int k = 0; k < lazerLocs.size(); k++)
					if (lazerLocs[k] == tileUpdate[i] && !isLazer[k])
					{
						nextLightLevel.push_back(Vec2(k, 0));
						beamLocs[k].y += 1;
						lazerz[k][0]->setPosition(lazerz[k][0]->getPosition() + Vec2(0, 64));
						lazerz[k][0]->setRotation(0);
						lazerz[k][1]->setPosition(lazerz[k][1]->getPosition() + Vec2(0, 64));
						lazerz[k][1]->setRotation(0);
					}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 21 && tileBOOL[i])
			{
				if (abs(locX4 - tileUpdate[i].x) < 10 && abs(locY4 - tileUpdate[i].y) < 8)
					playLazer = true;
				for (int k = 0; k < lazerLocs.size(); k++)
					if (lazerLocs[k] == tileUpdate[i] && !isLazer[k])
					{
						nextLightLevel.push_back(Vec2(k, 3));
						beamLocs[k].x -= 1;
						lazerz[k][0]->setPosition(lazerz[k][0]->getPosition() - Vec2(64, 0));
						lazerz[k][0]->setRotation(270);
						lazerz[k][1]->setPosition(lazerz[k][1]->getPosition() - Vec2(0, 0));
						lazerz[k][1]->setRotation(270);
					}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 22 && tileBOOL[i])
			{
				if (abs(locX4 - tileUpdate[i].x) < 10 && abs(locY4 - tileUpdate[i].y) < 8)
					playLazer = true;
				for (int k = 0; k < lazerLocs.size(); k++)
					if (lazerLocs[k] == tileUpdate[i] && !isLazer[k])
					{
						nextLightLevel.push_back(Vec2(k, 2));
						beamLocs[k].y -= 1;
						lazerz[k][0]->setPosition(lazerz[k][0]->getPosition() - Vec2(0, 64));
						lazerz[k][0]->setRotation(180);
						lazerz[k][1]->setPosition(lazerz[k][1]->getPosition() - Vec2(0, 64));
						lazerz[k][1]->setRotation(180);
					}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 23 && tileBOOL[i])
			{
				if (abs(locX4 - tileUpdate[i].x) < 10 && abs(locY4 - tileUpdate[i].y) < 8)
					playLazer = true;
				for (int k = 0; k < lazerLocs.size(); k++)
					if (lazerLocs[k] == tileUpdate[i] && !isLazer[k])
					{
						nextLightLevel.push_back(Vec2(k, 1));
						beamLocs[k].x += 1;
						lazerz[k][0]->setPosition(lazerz[k][0]->getPosition() + Vec2(64, 0));
						lazerz[k][0]->setRotation(90);
						lazerz[k][1]->setPosition(lazerz[k][1]->getPosition() + Vec2(64, 0));
						lazerz[k][1]->setRotation(90);
					}
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 26 && tileBOOL[i])
			{
				tiles[tileUpdate[i].x][tileUpdate[i].y][j]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lamp_on.png"));
				lampsOnOrOff[tileUpdate[i].x][tileUpdate[i].y] = true;
				nextOpacityUpdate.push_back(Vec2(tileUpdate[i].x, tileUpdate[i].y));
			}
			else if (tiles[tileUpdate[i].x][tileUpdate[i].y][j]->getTag() == 26 && !tileBOOL[i])
			{
				tiles[tileUpdate[i].x][tileUpdate[i].y][j]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_lamp.png"));
				lampsOnOrOff[tileUpdate[i].x][tileUpdate[i].y] = false;
				nextOpacityUpdate.push_back(Vec2(tileUpdate[i].x, tileUpdate[i].y));
			}
			if (!stopBlackOut && tileBOOL[i])
				for (int ii = 0; ii < antiBlackOutSections.size(); ii++)
				{
					if ((int)antiBlackOutSections[ii].x == (int)tileUpdate[i].x && (int)antiBlackOutSections[ii].y == (int)tileUpdate[i].y)
					{
						stopBlackOut = true;
						SUPEROpacityUpdate.push_back(Vec2(tileUpdate[i].x, tileUpdate[i].y));
					}
				}
		}
	}
	if (playLazer)
	{
		Effect.Sound("Soundtrack/lazer" + to_string(lazerToPlay) + ".mp3");
		lazerToPlay++;
		lazerToPlay %= 5;
	}
	tileUpdate.clear();
	tileBOOL.clear();
}

void HelloWorld::receiverOutput(bool mode, int k)
{
	for (int i = 0; i < tiles[(int)receivers[k].x][(int)receivers[k].y].size(); i++)
	{
		if (tiles[(int)receivers[k].x][(int)receivers[k].y][i]->getTag() == 4)
		{
			nextTileUpdate.push_back(receivers[k]);
			nextTileUpdate.push_back(receivers[k] + Vec2(0, 1));
			nextTileUpdate.push_back(receivers[k] + Vec2(0, -1));
			nextTileUpdate.push_back(receivers[k] + Vec2(1, 0));
			nextTileUpdate.push_back(receivers[k] + Vec2(-1, 0));
			nextTileBOOL.push_back(mode);
			nextTileBOOL.push_back(mode);
			nextTileBOOL.push_back(mode);
			nextTileBOOL.push_back(mode);
			nextTileBOOL.push_back(mode);
			updateGrid[(int)receivers[k].x][(int)receivers[k].y][0] = mode;
			updateGrid[(int)receivers[k].x][(int)receivers[k].y + 1][1] = mode;
			updateGrid[(int)receivers[k].x][(int)receivers[k].y - 1][2] = mode;
			updateGrid[(int)receivers[k].x + 1][(int)receivers[k].y][3] = mode;
			updateGrid[(int)receivers[k].x - 1][(int)receivers[k].y][4] = mode;
			updateGrid[(int)receivers[k].x][(int)receivers[k].y][5] = mode;
		}
		else if (tiles[(int)receivers[k].x][(int)receivers[k].y][i]->getTag() == 13)
		{
			nextTileUpdate.push_back(receivers[k]);
			nextTileBOOL.push_back(mode);
			updateGrid[(int)receivers[k].x][(int)receivers[k].y][0] = mode;
			updateGrid[(int)receivers[k].x][(int)receivers[k].y][5] = mode;
		}
	}
}

void HelloWorld::nextTileUpdateList()
{
	for (int i = 0; i < nextTileUpdate.size(); i++)
	{
		if (nextTileUpdate[i].x >= 0 && nextTileUpdate[i].y >= 0 && nextTileUpdate[i].x < maximumW && nextTileUpdate[i].y < maximumH)
		{
			tileUpdate.push_back(nextTileUpdate[i]);
			tileBOOL.push_back(nextTileBOOL[i]);
		}
	}
	nextTileUpdate.clear();
	nextTileBOOL.clear();
}

void HelloWorld::buttonUpdateList(float dt)
{
	for (int i = 0; i < buttonUpdate.size(); i++)
	{
		buttonUpdate[i].z -= dt;
		if (buttonUpdate[i].z <= 0)
		{
			for (int j = 0; j < tiles[(int)buttonUpdate[i].x][(int)buttonUpdate[i].y].size(); j++)
				if (tiles[(int)buttonUpdate[i].x][(int)buttonUpdate[i].y][j]->getTag() == 2)
					tiles[(int)buttonUpdate[i].x][(int)buttonUpdate[i].y][j]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_wall_button.png"));
			updateGrid[(int)buttonUpdate[i].x][(int)buttonUpdate[i].y][0] = false;
			nextTileUpdate.push_back(Vec2(buttonUpdate[i].x, buttonUpdate[i].y));
			nextTileBOOL.push_back(false);
			buttonUpdate.erase(buttonUpdate.begin() + i);
			i--;
		}
	}
}

void HelloWorld::doorOpener(float dt)
{
	for (int i = 0; i < openDoors.size(); i++)
	{
		int tempSize = tiles[openDoors[i].x][openDoors[i].y].size();
		for (int j = 0; j < tempSize; j++)
		{
			if (tiles[openDoors[i].x][openDoors[i].y][j]->getTag() == 6)
			{
				if (openBOOL[i])
					openDoors[i].z += dt;
				else
					openDoors[i].z -= dt;
				if (openDoors[i].z / doorSpeed < 6 && openDoors[i].z > 0)
				{
					tiles[openDoors[i].x][openDoors[i].y][j]->setSpriteFrame(doorz[(int)(openDoors[i].z / doorSpeed)]);
				}
				else if (openDoors[i].z <= 0)
				{
					Effect.Sound("Soundtrack/Door open.mp3");
					tiles[openDoors[i].x][openDoors[i].y][j]->setSpriteFrame(doorz[0]);
					j = tiles[openDoors[i].x][openDoors[i].y].size();
					openDoors.erase(openDoors.begin() + i);
					openBOOL.erase(openBOOL.begin() + i);
					i--;
				}
				else
				{
					Effect.Sound("Soundtrack/Door close.mp3");
					tiles[openDoors[i].x][openDoors[i].y][j]->setSpriteFrame(doorz[6]);
					j = tiles[openDoors[i].x][openDoors[i].y].size();
					openDoors.erase(openDoors.begin() + i);
					openBOOL.erase(openBOOL.begin() + i);
					i--;
				}
			}
		}
	}
	for (int i = 0; i < bossDoorLocations.size(); i++)
	{
		if (bossDoorBOOL[i])
		{
			for (int j = 0; j < tiles[bossDoorLocations[i].x][bossDoorLocations[i].y].size(); j++)
			{
				if (tiles[bossDoorLocations[i].x][bossDoorLocations[i].y][j]->getTag() == 30)
				{
					if (bossDoorTimes[i] / doorSpeed < 6)
					{
						tiles[bossDoorLocations[i].x][bossDoorLocations[i].y][j]->setSpriteFrame(bossDoorz[(int)(bossDoorTimes[i] / doorSpeed)]);
						bossDoorTimes[i] += dt;
					}
					else
					{
						tiles[bossDoorLocations[i].x][bossDoorLocations[i].y][j]->setSpriteFrame(bossDoorz[6]);
					}
				}
			}
		}
	}
}

void HelloWorld::triggerTimers(float dt)
{
	for (int i = 0; i < timerz.size(); i++)
	{
		timerz[i].w += dt;
		int b, a;

		if (timerMode[i])
			b = 1;
		else
			b = 0;
		a = b;
		while (timerz[i].w > timerz[i].z)
		{
			timerz[i].w -= timerz[i].z;
			b++;
		}
		b %= 2;
		if (b != a)
		{
			if (b == 1)
			{
				timerMode[i] = true;
			}
			else
			{
				timerMode[i] = false;
			}
			nextTileUpdate.push_back(Vec2((int)timerz[i].x, (int)timerz[i].y));
			nextTileUpdate.push_back(Vec2((int)timerz[i].x, (int)timerz[i].y) + Vec2(0, 1));
			nextTileUpdate.push_back(Vec2((int)timerz[i].x, (int)timerz[i].y) + Vec2(0, -1));
			nextTileUpdate.push_back(Vec2((int)timerz[i].x, (int)timerz[i].y) + Vec2(1, 0));
			nextTileUpdate.push_back(Vec2((int)timerz[i].x, (int)timerz[i].y) + Vec2(-1, 0));
			nextTileBOOL.push_back(timerMode[i]);
			nextTileBOOL.push_back(timerMode[i]);
			nextTileBOOL.push_back(timerMode[i]);
			nextTileBOOL.push_back(timerMode[i]);
			nextTileBOOL.push_back(timerMode[i]);
			updateGrid[(int)timerz[i].x][(int)timerz[i].y][0] = timerMode[i];
			updateGrid[(int)timerz[i].x][(int)timerz[i].y + 1][1] = timerMode[i];
			updateGrid[(int)timerz[i].x][(int)timerz[i].y - 1][2] = timerMode[i];
			updateGrid[(int)timerz[i].x + 1][(int)timerz[i].y][3] = timerMode[i];
			updateGrid[(int)timerz[i].x - 1][(int)timerz[i].y][4] = timerMode[i];
			updateGrid[(int)timerz[i].x][(int)timerz[i].y][5] = timerMode[i];
		}
	}
}

void HelloWorld::triggerLight(float dt)
{
	for (int i = 0; i < lightLevel.size(); i++)
	{
		lazerTime[lightLevel[i].x] += dt;
		if (lazerTime[lightLevel[i].x] > lazerDelay)
		{
			while (lazerTime[lightLevel[i].x] > lazerDelay)
			{
				lazerTime[lightLevel[i].x] -= lazerDelay;
				bool glass = false, stopYouRightThere = false, halfForDoor = false;
				
				for (int j = 0; j < tiles[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y].size(); j++)
				{
					int g = tiles[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][j]->getTag();
					if (g == 1 || (g >= 20 && g <= 23))
					{
						isLazer[lightLevel[i].x] = false;
						lazerz[lightLevel[i].x][0]->setVisible(false);
						lazerz[lightLevel[i].x][1]->setVisible(false);
					}
					else if (g == 6)
					{
						int tempForBools = 0;
						for (int a = 0; a < 5; a++)
							if (updateGrid[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][a])
								tempForBools++;
						if (tempForBools == 0)
						{
							isLazer[lightLevel[i].x] = false;
							lazerz[lightLevel[i].x][0]->setVisible(false);
							lazerz[lightLevel[i].x][1]->setVisible(false);
						}
					}
					else if (g == 24 && !updateGrid[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][5])
					{
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(0, 1));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(0, -1));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(1, 0));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(-1, 0));
						nextTileBOOL.push_back(true);
						nextTileBOOL.push_back(true);
						nextTileBOOL.push_back(true);
						nextTileBOOL.push_back(true);
						nextTileBOOL.push_back(true);
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y][0] = true;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y + 1][1] = true;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y - 1][2] = true;
						updateGrid[(int)beamLocs[lightLevel[i].x].x + 1][(int)beamLocs[lightLevel[i].x].y][3] = true;
						updateGrid[(int)beamLocs[lightLevel[i].x].x - 1][(int)beamLocs[lightLevel[i].x].y][4] = true;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y][5] = true;
					}
					else if (g == 24 && updateGrid[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][5])
					{
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(0, 1));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(0, -1));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(1, 0));
						nextTileUpdate.push_back(Vec2((int)beamLocs[lightLevel[i].x].x, (int)beamLocs[lightLevel[i].x].y) + Vec2(-1, 0));
						nextTileBOOL.push_back(false);
						nextTileBOOL.push_back(false);
						nextTileBOOL.push_back(false);
						nextTileBOOL.push_back(false);
						nextTileBOOL.push_back(false);
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y][0] = false;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y + 1][1] = false;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y - 1][2] = false;
						updateGrid[(int)beamLocs[lightLevel[i].x].x + 1][(int)beamLocs[lightLevel[i].x].y][3] = false;
						updateGrid[(int)beamLocs[lightLevel[i].x].x - 1][(int)beamLocs[lightLevel[i].x].y][4] = false;
						updateGrid[(int)beamLocs[lightLevel[i].x].x][(int)beamLocs[lightLevel[i].x].y][5] = false;
					}
					else if (g == 15)
					{
						tiles[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][j]->setOpacity(0);
					}
				}
				for (int j = 0; j < boxes.size(); j++)
				{
					if ((lightLevel[i].y == 0 || lightLevel[i].y == 2) && abs(boxes[j]->getPosition().x - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y - lazerz[lightLevel[i].x][0]->getPosition().y) < 32.f)
					{
						isLazer[lightLevel[i].x] = false;
						lazerz[lightLevel[i].x][0]->setVisible(false);
						lazerz[lightLevel[i].x][1]->setVisible(false);
					}
					else if ((lightLevel[i].y == 1 || lightLevel[i].y == 3) && abs(boxes[j]->getPosition().x - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y - lazerz[lightLevel[i].x][0]->getPosition().y) < 16.f)
					{
						isLazer[lightLevel[i].x] = false;
						lazerz[lightLevel[i].x][0]->setVisible(false);
						lazerz[lightLevel[i].x][1]->setVisible(false);
					}
					if (lightLevel[i].y == 0 && abs(boxes[j]->getPosition().x - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y - 64.f - lazerz[lightLevel[i].x][0]->getPosition().y) < 32.f)
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						stopYouRightThere = true;
					}
					else if (lightLevel[i].y == 1 && abs(boxes[j]->getPosition().x - 64.f - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y - lazerz[lightLevel[i].x][0]->getPosition().y) < 16.f)
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						stopYouRightThere = true;
					}
					else if (lightLevel[i].y == 2 && abs(boxes[j]->getPosition().x - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y + 64.f - lazerz[lightLevel[i].x][0]->getPosition().y) < 32.f)
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						stopYouRightThere = true;
					}
					else if (lightLevel[i].y == 3 && abs(boxes[j]->getPosition().x + 64.f - lazerz[lightLevel[i].x][0]->getPosition().x) < 32.f && abs(boxes[j]->getPosition().y - lazerz[lightLevel[i].x][0]->getPosition().y) < 16.f)
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						stopYouRightThere = true;
					}
				}
				if (isLazer[lightLevel[i].x])
				{
					for (int n = 0; n < bosses.size(); n++)
						if (abs(lazerz[lightLevel[i].x][0]->getPosition().x - bosses[n]->getBossSprite()->getPosition().x) < 40.f && abs(lazerz[lightLevel[i].x][0]->getPosition().y - bosses[n]->getBossSprite()->getPosition().y) < 30.f && bosses[n]->isAlive() && bosses[n]->getBossType() == 2 && bosses[n]->getBossCoolDown() <= 0)
						{
							Effect.Sound("Soundtrack/Priism hurt.mp3");
							bosses[n]->setBossCoolDown(0.5f);
							stopYouRightThere = true;
							bosses[n]->damage(200);
							bosses[n]->onDamage(dt);
							bosses[n]->setHP(bosses[n]->getHP() - 1);
							if (bosses[n]->getHP() <= 0)
							{
								Effect.deletemusic();
								bosses[n]->die();
								for (int k = 0; k < bossDoorBOOL.size(); k++)
								{
									bossDoorBOOL[k] = true;
									bossDoorsOpened[bossDoorLocations[k].x][bossDoorLocations[k].y] = true;
								}
							}
						}
					if (abs(ch[0].name->getPosition().y - lazerz[lightLevel[i].x][0]->getPosition().y) < 36.f && abs(ch[0].name->getPosition().x - lazerz[lightLevel[i].x][0]->getPosition().x) < 36.f)
					{
						if (ch[0].movingBox >= 0 && ch[0].direction == (int) lightLevel[i].y)
						{
						}
						else
						{
							ch[0].burned = true;
						}
					}
					lazerz[lightLevel[i].x][0]->setRotation(lightLevel[i].y * 90);
					lazerz[lightLevel[i].x][1]->setRotation(lightLevel[i].y * 90);
					if (lightLevel[i].y == 0)
					{
						if (lazerTime[lightLevel[i].x] <= lazerDelay)
						{
							nextLightLevel.push_back(lightLevel[i]);
						}
						lazerz[lightLevel[i].x][0]->setPosition(lazerz[lightLevel[i].x][0]->getPosition() + Vec2(0, 64));
						lazerz[lightLevel[i].x][1]->setPosition(lazerz[lightLevel[i].x][1]->getPosition() + Vec2(0, 64));
						beamLocs[lightLevel[i].x].y += 1;
					}
					else if (lightLevel[i].y == 1)
					{
						if (lazerTime[lightLevel[i].x] <= lazerDelay)
						{
							nextLightLevel.push_back(lightLevel[i]);
						}
						lazerz[lightLevel[i].x][0]->setPosition(lazerz[lightLevel[i].x][0]->getPosition() + Vec2(64, 0));
						lazerz[lightLevel[i].x][1]->setPosition(lazerz[lightLevel[i].x][1]->getPosition() + Vec2(64, 0));
						beamLocs[lightLevel[i].x].x += 1;
					}
					else if (lightLevel[i].y == 2)
					{
						if (lazerTime[lightLevel[i].x] <= lazerDelay)
						{
							nextLightLevel.push_back(lightLevel[i]);
						}
						lazerz[lightLevel[i].x][0]->setPosition(lazerz[lightLevel[i].x][0]->getPosition() - Vec2(0, 64));
						lazerz[lightLevel[i].x][1]->setPosition(lazerz[lightLevel[i].x][1]->getPosition() - Vec2(0, 64));
						beamLocs[lightLevel[i].x].y -= 1;
					}
					else if (lightLevel[i].y == 3)
					{
						if (lazerTime[lightLevel[i].x] <= lazerDelay)
						{
							nextLightLevel.push_back(lightLevel[i]);
						}
						lazerz[lightLevel[i].x][0]->setPosition(lazerz[lightLevel[i].x][0]->getPosition() - Vec2(64, 0));
						lazerz[lightLevel[i].x][1]->setPosition(lazerz[lightLevel[i].x][1]->getPosition() - Vec2(64, 0));
						beamLocs[lightLevel[i].x].x -= 1;
					}
				}
				else
				{
					beamLocs[lightLevel[i].x] = lazerLocs[lightLevel[i].x];
					lazerz[lightLevel[i].x][0]->setPosition(beamLocs[lightLevel[i].x] * 64 - Vec2(maximumW * 32, 0));
					lazerz[lightLevel[i].x][1]->setPosition(beamLocs[lightLevel[i].x] * 64 - Vec2(maximumW * 32, 0));
				}
				lazerz[lightLevel[i].x][0]->setGlobalZOrder(2.9984f - 0.0001 * lazerz[lightLevel[i].x][0]->getPosition().y);
				lazerz[lightLevel[i].x][1]->setGlobalZOrder(2.9984f - 0.0001 * lazerz[lightLevel[i].x][1]->getPosition().y);
				for (int j = 0; j < tiles[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y].size(); j++)
				{
					int g = tiles[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][j]->getTag();
					if (g == 25)
					{
						glass = true;
					}

					else if (g == 6)
					{
						int tempForBools = 0;
						for (int a = 0; a < 5; a++)
							if (updateGrid[beamLocs[lightLevel[i].x].x][beamLocs[lightLevel[i].x].y][a])
								tempForBools++;
						if (tempForBools == 0)
						{
							halfForDoor = true;
							lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
							lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						}
					}
					else if (g >= 20 && g <= 23)
					{
						halfForDoor = true;
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
					}
				}
				if (stopYouRightThere || halfForDoor)
				{
					if (lightLevel[i].y == 0)
					{
						lazerz[lightLevel[i].x][0]->setGlobalZOrder(3.00006f - 0.0001 * lazerz[lightLevel[i].x][0]->getPosition().y);
						lazerz[lightLevel[i].x][1]->setGlobalZOrder(3.00006f - 0.0001 * lazerz[lightLevel[i].x][1]->getPosition().y);
					}
				}
				else if (glass)
				{
					if (lightLevel[i].y == 0 || lightLevel[i].y == 2)
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_half.png"));
						lazerz[lightLevel[i].x][0]->setRotation(0);
						lazerz[lightLevel[i].x][1]->setRotation(180);
						lazerz[lightLevel[i].x][0]->setGlobalZOrder(3.00006f - 0.0001 * lazerz[lightLevel[i].x][0]->getPosition().y);
						lazerz[lightLevel[i].x][1]->setGlobalZOrder(2.9984f - 0.0001 * lazerz[lightLevel[i].x][1]->getPosition().y);
					}
					else
					{
						lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
						lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
						lazerz[lightLevel[i].x][0]->setGlobalZOrder(2.9984f - 0.0001 * lazerz[lightLevel[i].x][0]->getPosition().y);
						lazerz[lightLevel[i].x][1]->setGlobalZOrder(2.9984f - 0.0001 * lazerz[lightLevel[i].x][1]->getPosition().y);
					}
				}
				else
				{
					lazerz[lightLevel[i].x][0]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
					lazerz[lightLevel[i].x][1]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_laser_full.png"));
				}
			}
		}
		else
		{
			nextLightLevel.push_back(lightLevel[i]);
		}
	}
	lightLevel.clear();
}

void HelloWorld::nextTriggerLight(float dt)
{
	for (int i = 0; i < nextLightLevel.size(); i++)
	{
		lightLevel.push_back(nextLightLevel[i]);
		isLazer[lightLevel[i].x] = true;
	}
	nextLightLevel.clear();
}

void HelloWorld::opacityUp(float dt)
{
	for (int i = 0; i < opacityUpdate.size(); i++)
	{
		bool capture = false;
		int myOpacity = opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y];
		for (int j = 0; j < tiles[opacityUpdate[i].x][opacityUpdate[i].y].size(); j++)
		{
			if (tiles[opacityUpdate[i].x][opacityUpdate[i].y][j]->getTag() == 27)
				capture = true;
			else if (tiles[opacityUpdate[i].x][opacityUpdate[i].y][j]->getTag() == 26 && lampsOnOrOff[opacityUpdate[i].x][opacityUpdate[i].y])
				capture = true;
		}
		bool madDiffernce = false;
		if (capture)
		{
			myOpacity = 255;
		}
		else
		{
			if (opacityUpdate[i].x > 0)
			{
				myOpacity = opacityOfSquare[opacityUpdate[i].x - 1][opacityUpdate[i].y] - 51; 
			}
			if (opacityUpdate[i].x < maximumW - 1 && opacityOfSquare[opacityUpdate[i].x + 1][opacityUpdate[i].y] > myOpacity - 51)
			{
				myOpacity = opacityOfSquare[opacityUpdate[i].x + 1][opacityUpdate[i].y] - 51;
			}
			if (opacityUpdate[i].y > 0 && opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y - 1] > myOpacity - 51)
			{
				myOpacity = opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y - 1] - 51;
			}
			if (opacityUpdate[i].y < maximumH - 1 && opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y + 1] > myOpacity - 51)
			{
				myOpacity = opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y + 1] - 51;
			}
		}
		if (myOpacity < 0)
			myOpacity = 0;
		
		if (tiles[opacityUpdate[i].x][opacityUpdate[i].y][0]->getName() != "1" && myOpacity != opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y])
		{
			if (opacityUpdate[i].x > 0)
			{
				nextOpacityUpdate.push_back(Vec2(opacityUpdate[i].x - 1, opacityUpdate[i].y));
			}
			if (opacityUpdate[i].x < maximumW - 1)
			{
				nextOpacityUpdate.push_back(Vec2(opacityUpdate[i].x + 1, opacityUpdate[i].y));
			}
			if (opacityUpdate[i].y > 0)
			{
				nextOpacityUpdate.push_back(Vec2(opacityUpdate[i].x, opacityUpdate[i].y - 1));
			}
			if (opacityUpdate[i].y < maximumH - 1)
			{
				nextOpacityUpdate.push_back(Vec2(opacityUpdate[i].x, opacityUpdate[i].y + 1));
			}

			opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y] = myOpacity;
		}
		else if (tiles[opacityUpdate[i].x][opacityUpdate[i].y][0]->getName() == "1")
		{
			opacityOfSquare[opacityUpdate[i].x][opacityUpdate[i].y] = 0;
		}
		
	}
	opacityUpdate.clear();
}

void HelloWorld::nextOpacityUp(float dt)
{
	for (int i = 0; i < nextOpacityUpdate.size(); i++)
	{
		opacityUpdate.push_back(nextOpacityUpdate[i]);
	}
	nextOpacityUpdate.clear();
}

void HelloWorld::shakey()
{
	if (amountOfShake > 0)
	{
		cam->setPosition(cam->getPosition() + Vec2(amountOfShake, 0) * (float)shakeDirection);
		amountOfShake--;
		shakeDirection *= -1;
	}
}

void HelloWorld::splatter(float dt)
{
	splatterTime += dt;
	if (splatterTime * 20.f < 4)
	{
		Effect.Sound("Soundtrack/Goop.mp3");
		ch[0].name->setSpriteFrame(ch[0].trip[ch[0].direction][splatterTime * 20.f]);
		/*if (ch[0].direction == 2)
		{
			ch[0].name->setPosition(ch[0].name->getPosition() + Vec2(0, dt * 100.f));
		}
		else if (ch[0].direction == 0)
		{
			ch[0].name->setPosition(ch[0].name->getPosition() - Vec2(0, dt * 100.f));
		}
		else if (ch[0].direction == 1)
		{
			ch[0].name->setPosition(ch[0].name->getPosition() - Vec2(dt * 100.f, 0));
		}
		else if (ch[0].direction == 3)
		{
			ch[0].name->setPosition(ch[0].name->getPosition() + Vec2(dt * 100.f, 0));
		}*/
		ch[0].name->setPosition(ch[0].name->getPosition() + ch[0].mockVel * dt * 100.f);
	}
	else
	{
		ch[0].name->setSpriteFrame(ch[0].trip[ch[0].direction][4]);
	}
}

void HelloWorld::singe(float dt)
{
	burnTime += dt;
	ch[0].name->setGlobalZOrder(-1);
	if (burnTime * 15.f < 4)
	{
		ch[0].name->setSpriteFrame(ch[0].burn[burnTime * 15.f]);
		/*if (ch[0].direction == 2)
		{
		ch[0].name->setPosition(ch[0].name->getPosition() + Vec2(0, dt * 100.f));
		}
		else if (ch[0].direction == 0)
		{
		ch[0].name->setPosition(ch[0].name->getPosition() - Vec2(0, dt * 100.f));
		}
		else if (ch[0].direction == 1)
		{
		ch[0].name->setPosition(ch[0].name->getPosition() - Vec2(dt * 100.f, 0));
		}
		else if (ch[0].direction == 3)
		{
		ch[0].name->setPosition(ch[0].name->getPosition() + Vec2(dt * 100.f, 0));
		}*/
		ch[0].name->setPosition(ch[0].name->getPosition());
		ch[0].name->getPhysicsBody()->setVelocity(Vec2(0, 0));
	}
	else
	{
		ch[0].name->setSpriteFrame(ch[0].burn[4]);
		ch[0].name->getPhysicsBody()->setVelocity(Vec2(0, 0));
	}
}

void HelloWorld::winSeq(float dt)
{
	if (moveAnimX < upperAnimX + upperAnimY - lowerAnimY)
	{
		slowSeq++;
		slowSeq %= 3;
		if (slowSeq == 1)
		{
			bombSeqTime += dt;
			while (bombSeqTime > nextBomb)
			{
				bombSeqTime -= nextBomb;
				for (int i = lowerAnimY; i < upperAnimY; i++)
				{
					tilesToBoom.push_back(Vec2(moveAnimX - i + lowerAnimY, i));
					tileBoomTime.push_back(0.f);
				}
				moveAnimX++;
			}
		}
	}
	else if (clickToContinue->getOpacity() < 255)
	{
		if (clickToContinue->getOpacity() + dt * 255.f > 255)
			clickToContinue->setOpacity(255);
		else
			clickToContinue->setOpacity(clickToContinue->getOpacity() + dt * 255.f);
	}
	else if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
	{
		Scene *s = PlayMap::createScene();
		Director::getInstance()->replaceScene(s);
	}
	tileBoom(dt);
	/*if (moveAnimX < upperAnimX)
	{
		tilesToBoom.push_back(Vec2(moveAnimX, moveAnimY));
		tileBoomTime.push_back(0.f);
		moveAnimX++;
	}
	else if (moveAnimY < upperAnimY)
	{
		moveAnimX = lowerAnimX;
		moveAnimY++;
		tilesToBoom.push_back(Vec2(moveAnimX, moveAnimY));
		tileBoomTime.push_back(0.f);
		moveAnimX++;
	}*/
}

void HelloWorld::tileBoom(float dt)
{
	for (int i = 0; i < tilesToBoom.size(); i++)
	{
		if (tilesToBoom[i].x >= 0 && tilesToBoom[i].y >= 0 && tilesToBoom[i].x < maximumW && tilesToBoom[i].y < maximumH)
		{
			tileBoomTime[i] += dt;
			if (tileBoomTime[i] < 1.0f)
			{
				for (int ii = 1; ii < tiles[tilesToBoom[i].x][tilesToBoom[i].y].size(); ii++)
				{
					tiles[tilesToBoom[i].x][tilesToBoom[i].y][ii]->setVisible(false);
				}
				tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->setSpriteFrame(ch[0].splode[tileBoomTime[i] * 10.f]);
				tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->setColor(Color3B(255, 255, 255));
				tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->setGlobalZOrder(100 + 0.0001f * (tilesToBoom[i].x - tilesToBoom[i].y));
				tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->setScale(256.f / tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->getContentSize().height);
			}
			else
			{
				tiles[tilesToBoom[i].x][tilesToBoom[i].y][0]->setVisible(false);
				tilesToBoom.erase(tilesToBoom.begin() + i);
				tileBoomTime.erase(tileBoomTime.begin() + i);
				i--;
			}
		}
		else
		{
			tilesToBoom.erase(tilesToBoom.begin() + i);
			tileBoomTime.erase(tileBoomTime.begin() + i);
			i--;
		}
	}
}

void HelloWorld::blackout(float dt, float * t, bool direction)
{
	if (direction)
	{
		*t -= dt;
		if (*t < 0)
			*t = 0;
		blackScreen->setOpacity(*t * 255);
		loding->setOpacity(*t * 255);
	}
	else
	{
		*t += dt;
		if (*t > 1)
			*t = 1;
		blackScreen->setOpacity(*t * 255);
	}
}

void HelloWorld::moveBosses(float dt)
{
	for (int i = 0; i < bosses.size(); i++)
	{
		int locX3 = floor((bosses[i]->getBossSprite()->getPosition().x + 32.f) / 64 + maximumW / 2);
		int locY3 = floor((bosses[i]->getBossSprite()->getPosition().y + 16.f) / 64);
		int prelocX3 = floor((previousBossPositions[i].x + 32.f) / 64 + maximumW / 2);
		int prelocY3 = floor((previousBossPositions[i].y + 16.f) / 64);
		if (bosses[i]->isAlive())
		{
			previousBossPositions[i] = bosses[i]->getBossSprite()->getPosition();
			bosses[i]->setPlayerPosition(ch[0].name->getPosition());
			bosses[i]->move();
			bosses[i]->dialBackDamage(dt);
			bosses[i]->conductMovement(dt);
			if (bosses[i]->getBossType() == 1)
			{
				if (prelocX3 != locX3 || prelocY3 != locY3)
				{
					if (locX3 >= 0 && locX3 < maximumW && locY3 >= 0 && locY3 < maximumH)
					{
						mintyTrail.push_back(Vec2(locX3, locY3));
						int tbone = rand() % (int)(10.f - bosses[i]->getHP());
						mintyTiles[locX3][locY3] = tbone;
					}
				}
				if (bosses[i]->getBossSprite()->getPhysicsBody()->getVelocity().dot(bosses[i]->getBossSprite()->getPosition() - ch[0].name->getPosition()) < 0)
				{
					bosses[i]->setMaxSpeed(800);
				}
				else
				{
					bosses[i]->setMaxSpeed(200);
				}
				for (int j = 0; j < boxes.size(); j++)
				{
					if (abs(boxes[j]->getPosition().x - bosses[i]->getBossSprite()->getPosition().x) < 70.f && abs(boxes[j]->getPosition().y - bosses[i]->getBossSprite()->getPosition().y) < 36.f)
					{
						Effect.Sound("Soundtrack/Broken Box.mp3");
						bosses[i]->setVelocity(0, 0);
						bosses[i]->getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
						boxDestroyed[j] = true;
						bosses[i]->damage(200);
						amountOfShake += 15.f;
						if (j == ch[0].movingBox)
						{
							boxes[ch[0].movingBox]->setColor(Color3B(255, 255, 255));
							boxes[ch[0].movingBox]->getPhysicsBody()->setCollisionBitmask(2);
							boxes[ch[0].movingBox]->getPhysicsBody()->setCategoryBitmask(1);
							boxes[ch[0].movingBox]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("element_box.png"));
							ch[0].movingBox = -1;
							ch[0].toBox = Vec2(0, 0);
						}
						bosses[i]->setHP(bosses[i]->getHP() - 1);
						bosses[i]->onDamage(dt);
						if (bosses[i]->getHP() <= 0)
						{
							Effect.deletemusic();
							bosses[i]->die();
							for (int k = 0; k < bossDoorBOOL.size(); k++)
							{
								bossDoorBOOL[k] = true;
								bossDoorsOpened[bossDoorLocations[k].x][bossDoorLocations[k].y] = true;
							}
						}
					}
				}
			}
			else if (bosses[i]->getBossType() == 2)
			{
				bosses[i]->attack(1, dt);
				Vec3 tempo = bosses[i]->getSignal();
				if (tempo.z == 1)
				{
					amountOfShake += 25.f;
					if ((ch[0].name->getPosition() - Vec2(0, 16) - Vec2(tempo.x, tempo.y)).length() < 40.f)
					{
						ch[0].burned = true;
					}
				}
			}
			else if (bosses[i]->getBossType() == 3)
			{
				int q = 2;
				if (!stopBlackOut)
				{
					q = 1;
					bosses[i]->setCustomFrame();
				}
				bosses[i]->attack(q, dt);
				Vec3 tempo = bosses[i]->getSignal();
				if (tempo.z == 1)
				{
					ch[0].burned = true;
				}
				else if (tempo.z == 2)
				{
					Effect.Sound("Soundtrack/CEO hurt.mp3");
					bosses[i]->setHP(bosses[i]->getHP() - 1);
					if (bosses[i]->getHP() <= 0)
					{
						Effect.deletemusic();
						bosses[i]->die();
						for (int k = 0; k < bossDoorBOOL.size(); k++)
						{
							bossDoorBOOL[k] = true;
							bossDoorsOpened[bossDoorLocations[k].x][bossDoorLocations[k].y] = true;
						}
					}
					else
					{
						stopBlackOut = false;
						SUPEROpacityUpdate.push_back(bosses[i]->getSpawnPoint() / 64.f + Vec2(maximumW * 0.5f, 0));
					}
				}
				else if (tempo.z == 3)
				{
					stopBlackOut = false;
					SUPEROpacityUpdate.push_back(bosses[i]->getSpawnPoint() / 64.f + Vec2(maximumW * 0.5f, 0));
				}
			}
		}
		else
		{
			bosses[i]->deathSequence(dt);
		}
	}
}

void HelloWorld::gameOverScreen(float dt)
{
}

void HelloWorld::mintyGoop(float dt)
{
	for (int i = 0; i < mintyTrail.size(); i++)
	{
		mintyTiles[mintyTrail[i].x][mintyTrail[i].y] -= dt;
		if (mintyTiles[mintyTrail[i].x][mintyTrail[i].y] <= 0)
		{
			mintyTiles[mintyTrail[i].x][mintyTrail[i].y] = 0.f;
			for (int m = 0; m < tiles[mintyTrail[i].x][mintyTrail[i].y].size(); m++)
				if (tiles[mintyTrail[i].x][mintyTrail[i].y][m]->getTag() == 60)
					tiles[mintyTrail[i].x][mintyTrail[i].y][m]->setVisible(false);
			mintyTrail.erase(mintyTrail.begin() + i);
			i--;
		}
	}
}

void HelloWorld::turnOnAllLights()
{
	vector <Vec2> NEXTSUPAOP;
	if (SUPEROpacityUpdate.size() > 0)
		cout << SUPEROpacityUpdate.size() << endl;
	for (int i = 0; i < SUPEROpacityUpdate.size(); i++)
	{
		permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y] = true;
		if (SUPEROpacityUpdate[i].x > 0)
		{
			if (!permanentlyOn[SUPEROpacityUpdate[i].x - 1][SUPEROpacityUpdate[i].y])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x - 1, SUPEROpacityUpdate[i].y));
				permanentlyOn[SUPEROpacityUpdate[i].x - 1][SUPEROpacityUpdate[i].y] = true;
			}
		}
		if (SUPEROpacityUpdate[i].y > 0)
		{
			if (!permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y - 1])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x, SUPEROpacityUpdate[i].y - 1));
				permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y - 1] = true;
			}
		}
		if (SUPEROpacityUpdate[i].x < maximumW - 1)
		{
			if (!permanentlyOn[SUPEROpacityUpdate[i].x + 1][SUPEROpacityUpdate[i].y])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x + 1, SUPEROpacityUpdate[i].y));
				permanentlyOn[SUPEROpacityUpdate[i].x + 1][SUPEROpacityUpdate[i].y] = true;
			}
		}
		if (SUPEROpacityUpdate[i].y < maximumH - 1)
		{
			if (!permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y + 1])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x, SUPEROpacityUpdate[i].y + 1));
				permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y + 1] = true;
			}
		}
	}
	SUPEROpacityUpdate.clear();
	for (int i = 0; i < NEXTSUPAOP.size(); i++)
	{
		SUPEROpacityUpdate.push_back(NEXTSUPAOP[i]);
	}
	NEXTSUPAOP.clear();
}

void HelloWorld::turnOffAllLights()
{
	vector <Vec2> NEXTSUPAOP;
	for (int i = 0; i < SUPEROpacityUpdate.size(); i++)
	{
		permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y] = false;
		if (SUPEROpacityUpdate[i].x > 0)
		{
			if (permanentlyOn[SUPEROpacityUpdate[i].x - 1][SUPEROpacityUpdate[i].y])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x - 1, SUPEROpacityUpdate[i].y));
				permanentlyOn[SUPEROpacityUpdate[i].x - 1][SUPEROpacityUpdate[i].y] = false;
			}
		}
		if (SUPEROpacityUpdate[i].y > 0)
		{
			if (permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y - 1])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x, SUPEROpacityUpdate[i].y - 1));
				permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y - 1] = false;
			}
		}
		if (SUPEROpacityUpdate[i].x < maximumW - 1)
		{
			if (permanentlyOn[SUPEROpacityUpdate[i].x + 1][SUPEROpacityUpdate[i].y])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x + 1, SUPEROpacityUpdate[i].y));
				permanentlyOn[SUPEROpacityUpdate[i].x + 1][SUPEROpacityUpdate[i].y] = false;
			}
		}
		if (SUPEROpacityUpdate[i].y < maximumH - 1)
		{
			if (permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y + 1])
			{
				NEXTSUPAOP.push_back(Vec2(SUPEROpacityUpdate[i].x, SUPEROpacityUpdate[i].y + 1));
				permanentlyOn[SUPEROpacityUpdate[i].x][SUPEROpacityUpdate[i].y + 1] = false;
			}
		}
	}
	SUPEROpacityUpdate.clear();
	for (int i = 0; i < NEXTSUPAOP.size(); i++)
	{
		SUPEROpacityUpdate.push_back(NEXTSUPAOP[i]);
	}
	NEXTSUPAOP.clear();
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}

PhysicsWorld* HelloWorld::physicsWorld = nullptr;
//Scene* HelloWorld::sceneHandle = nullptr;
//Scene* HelloWorld::layeryH = nullptr;