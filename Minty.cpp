#include "Minty.h"
#include "Soundtrack.h"

Sounds MintyDeath;

Minty::Minty(Scene * s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection) : boss(s, pos, walkCycle, startDirection, 1)
{
	setStandardAcc(accLimit);
	setSpawnpoint(pos);
	setHP(6);
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	setAlive(true);
	setUseStandardAcc(true);
	setMaxSpeed(600.f);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
}

void Minty::spawn()
{
	setPosition(getSpawnPoint());
	getBossSprite()->setPosition(getSpawnPoint());
	getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	setVelocity(0, 0);
	setAcceleration(0, 0);
	setHP(6);
	setAlive(true);
	setUseStandardAcc(true);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
}

void Minty::die()
{
	MintyDeath.Sound("Soundtrack/Minty Death.mp3");
	setAlive(false);
}

void Minty::move()
{
	Vec2 tempAcc = getVectorTo(getPlayerPosition());
	if (abs(tempAcc.x) > abs(tempAcc.y))
	{
		if (tempAcc.x > 0)
		{
			setDirection(3);
			getBossSprite()->setScaleX(-1 * abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(1);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
	}
	else
	{
		if (tempAcc.y > 0)
		{
			setDirection(2);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(0);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
	}
	tempAcc.normalize();
	tempAcc *= getStandardAcc();
	setAcceleration(tempAcc);
}

Vec2 Minty::getVectorTo(Vec2 place)
{
	if ((place - getPosition()).length() > 0)
	{
		Vec2 temp = place - getPosition();
		temp.normalize();
		return temp;
	}
	else
		return Vec2(0, 0);
}

void Minty::setTarget(Vec2 tar)
{
	target = tar;
}

Vec2 Minty::getTarget()
{
	return target;
}
