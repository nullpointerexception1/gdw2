#pragma once
#include <SimpleAudioEngine.h>
#include <string>
#include <cstring>

using namespace CocosDenshion;

class Sounds
{
public:
	Sounds();
	~Sounds();
	void Music(std::string);
	void Sound(std::string);
	void deletemusic();
	void SoundE();
};