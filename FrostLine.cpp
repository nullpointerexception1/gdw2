#include "FrostLine.h"
#include "SimpleAudioEngine.h"
#include "DisplayHandler.h"
#include "InputHandler.h"

cocos2d::Scene * FrostLine::createScene()
{
	return FrostLine::create();
}

void FrostLine::update(float dt)
{
	updateInputs(dt);
	INPUTS->clearForNextFrame();
}

void FrostLine::updateInputs(float dt)
{
	totalTime += dt;
	if (totalTime < 0.5f)
	{
		FROST->setOpacity(510.f * totalTime);
	}
	else if (totalTime < 1.f)
	{
		FROST->setOpacity(255);
		LINE->setOpacity(510.f * (totalTime - 0.5f));
	}
	else if (totalTime < 1.5f)
	{
		LINE->setOpacity(255);
		EXPERIENCE->setOpacity(510.f * (totalTime - 1.f));
	}
	else if (totalTime < 2.f)
	{
		EXPERIENCE->setOpacity(255);
	}
	else if (totalTime < 2.5f)
	{
		FROST->setOpacity(510.f * (2.5f - totalTime));
		LINE->setOpacity(510.f * (2.5f - totalTime));
		EXPERIENCE->setOpacity(510.f * (2.5f - totalTime));
	}
	else
	{
		Scene* s = Title::createScene();
		Director::getInstance()->replaceScene(s);
	}
}

void FrostLine::onExit()
{
	Scene::onExit();
}

void FrostLine::onEnter()
{
	Scene::onEnter();
}

bool FrostLine::init()
{
	if (!Scene::init())
	{
		return false;
	}

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("TempSprSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("door.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player_up.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player_down.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player_side.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("explosion.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player_blink.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ceo_up.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ceo_side.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ceo_down.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("FLE.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("signs.plist");

	auto visibleSize = Director::getInstance()->getVisibleSize();
	VS = visibleSize;
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	OS = origin;

	FROST = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("frost.png"));
	FROST->setPosition(VS * 0.5f);
	FROST->setScale(0.5f);
	FROST->setOpacity(0);
	LINE = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Line.png"));
	LINE->setPosition(VS * 0.5f);
	LINE->setScale(0.5f);
	LINE->setOpacity(0);
	EXPERIENCE = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Experience.png"));
	EXPERIENCE->setPosition(VS * 0.5f);
	EXPERIENCE->setScale(0.5f);
	EXPERIENCE->setOpacity(0);

	this->addChild(FROST, 2);
	this->addChild(LINE, 1);
	this->addChild(EXPERIENCE, 0);

	this->scheduleUpdate();
	return true;
}

void FrostLine::menuCloseCallback(cocos2d::Ref * pSender)
{
}
