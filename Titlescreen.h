#pragma once

#include "mBox.h"
#include "cocos2d.h"
#include <vector>
#include <string>

using namespace::cocos2d;
class Title : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	void update(float dt);
	void updateInputs(float dt);
	void updateMouseInputs(float dt);
	void updateKeyboardInputs(float dt);
	void trigger(float dt, std::string i);
	void blackout(float dt, float *t, bool direction);

	virtual void onExit();
	virtual void onEnter();
	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	Sprite* backdrop;
	DrawNode* blackScreen;
	std::vector<Label*> ops;
	std::vector<Sprite*> icons;
	Director* director;
	std::vector<mbox> clicks;
	bool transferScene = false;
	float inTime = 1.f;
	std::string choice;

	// implement the "static create()" method manually
	CREATE_FUNC(Title);
private:
	static PhysicsWorld* physicsWorld;
	static Scene* sceneHandle;
};
