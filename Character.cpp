#include <string>
#include "Character.h"
#include "Soundtrack.h"

Sounds explo;

character::character()
{
	name = Sprite::create("YOU.png");
	Scale(4.f);
	setBB(name->getContentSize().width, name->getContentSize().height);
	speed = Vec2(0.f, 0.f);
	acc = 800000.f;
	checkpoint = Vec2(60 / 2 * 64.f, 0);
	name->setGlobalZOrder(100);
}

character::character(Scene * scene, std::vector<std::vector<SpriteFrame*>> daSpritz)
{
	walk = daSpritz;
	name = Sprite::createWithSpriteFrame(walk[0][0]);
	name->setScale(2.0f);
	PhysicsBody *pb = PhysicsBody::createCircle(name->getContentSize().width / 4);
	pb->setDynamic(true);
	pb->setPositionOffset(Vec2(0, -name->getContentSize().height / 2));
	pb->setRotationEnable(false);
	name->setPhysicsBody(pb);
	name->getPhysicsBody()->setCollisionBitmask(1);
	name->getPhysicsBody()->setCategoryBitmask(2);
	name->setPosition(0, 0);
	setBB(name->getContentSize().width, name->getContentSize().height);
	speed = Vec2(0.f, 0.f);
	acc = 800000.f;
	checkpoint = Vec2(8 * 64.f, 2 * 64.f);
	name->setGlobalZOrder(100);
	direction = 0;
	frameTot = walk[0].size();
	walkNum = 0;
	walkTime = 0.f;
	invert = false;
	stepLength = 0.05f;
	explosionFrame = 0;
	boomTime = 0;
	boomLength = 0.1f;
	maxPlode = 10;
	blowingUp = false;
	hasMoved = false;
	movingBox = -1;
	toBox = Vec2(0, 0);
}

character::~character()
{
	
}

void character::setBB(float x, float y)
{
	bx = x;
	by = y;
}

void character::Scale(float s)
{
	scale = s;
	name->setScale(s);
}

float character::getBX()
{
	return bx;
}

float character::getBY()
{
	return by;
}

Sprite * character::getSprite()
{
	return name;
}

void character::explode()
{
	//cocos2d::CallFunc* _reset = cocos2d::CallFunc::create([=]() {
	//	this->setAtCheckpoint();
	//});
	//name->runAction(Sequence::create(TintTo::create(0.1f, Color3B(255, 0, 0)), TintTo::create(0.1f, Color3B(255, 255, 255)), TintTo::create(0.1f, Color3B(255, 0, 0)), TintTo::create(0.1f, Color3B(255, 255, 255)), TintTo::create(0.1f, Color3B(255, 0, 0)), TintTo::create(0.1f, Color3B(255, 255, 255)), _reset, NULL));
	explo.Sound("Soundtrack/Explode.mp3");
	blowingUp = true;
	name->getPhysicsBody()->setEnabled(false);
}

void character::setAtCheckpoint()
{
	name->setPosition(checkpoint);
}

void character::incrementWalk(float dt)
{
	walkTime += dt;
	while (walkTime > stepLength)
	{
		walkTime -= stepLength;
		walkNum++;
		if (walkNum >= frameTot)
			walkNum = 0;
	}
}

void character::resetWalk()
{
	walkNum = 0;
	walkTime = 0.f;
}

void character::explosionSequence(float dt)
{
	boomTime += dt;
	while (boomTime > boomLength)
	{
		boomTime -= boomLength;
		name->setColor(Color3B(255, 255, 255));
		if (explosionFrame < maxPlode)
			explosionFrame++;
		if (explosionFrame >= maxPlode)
		{
			blowingUp = false;
			explosionFrame = maxPlode - 1;
		}
	}
}
