#include "CEO.h"
#include "Soundtrack.h"

Sounds CEOdeath;

CEO::CEO(Scene * s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection, vector<SpriteFrame*> monocle) : boss(s, pos, walkCycle, startDirection, 3)
{
	setStandardAcc(accLimit);
	setSpawnpoint(pos);
	setHP(3);
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	setAlive(true);
	setUseStandardAcc(true);
	setMaxSpeed(200.f);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
	Monocle = monocle;
	setCustomFrame();
	timeUntilTeleport = 0.f;
	teleportTime = 5.f;
	SHITHECANSEEME = false;
	portin = false;
	phaseIn = true;
	exposed = 0.f;
	maximumExposure = 6.f;
	swinging = false;
	SOUPAswinging = false;
	swing = 0.f;
}

void CEO::spawn()
{
	setHP(3);
	getBossSprite()->setOpacity(255);
	getBossSprite()->setVisible(true);
	getBossSprite()->setPosition(getSpawnPoint());
	setAlive(true);
	setUseStandardAcc(true);
	setMaxSpeed(200.f);
	setCycleFrame(0);
	setDeathCycleFrame(0);
	setWalkFrame(0, 0);
	setCustomFrame();
	damage(0);
	getBossSprite()->setColor(Color3B(255, 255, 255));
	timeUntilTeleport = 0.f;
	teleportTime = 5.f;
	SHITHECANSEEME = false;
	portin = false;
	phaseIn = true;
	toSpawn = false;
	CEOpacity = 255.f;
	exposed = 0.f;
	maximumExposure = 6.f;
	swinging = false;
	SOUPAswinging = false;
	swing = 0.f;
}

void CEO::die()
{
	CEOdeath.Sound("Soundtrack/CEO Death.mp3");
	setAlive(false);
}

void CEO::move()
{
	Vec2 tempAcc = getVectorTo(getPlayerPosition());
	if (SHITHECANSEEME)
	{
		tempAcc *= -1;
		setMaxSpeed(200.f + (3 - getHP()) * 100.f);
	}
	else
	{
		setMaxSpeed(600.f + (3 - getHP()) * 200.f);
	}
	if (abs(tempAcc.x) > abs(tempAcc.y))
	{
		if (tempAcc.x > 0)
		{
			setDirection(3);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(1);
			getBossSprite()->setScaleX(-1 * abs(getBossSprite()->getScaleX()));
		}
	}
	else
	{
		if (tempAcc.y > 0)
		{
			setDirection(2);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
		else
		{
			setDirection(0);
			getBossSprite()->setScaleX(abs(getBossSprite()->getScaleX()));
		}
	}
	tempAcc.normalize();
	tempAcc *= getStandardAcc();
	setAcceleration(tempAcc);
}

void CEO::setCustomFrame()
{
	getBossSprite()->setSpriteFrame(Monocle[(int)swing]);
}

void CEO::attack(int attackType, float dt)
{
	if (toSpawn)
	{
		getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
		if (phaseIn)
		{
			CEOpacity -= dt * 510.f * (4.f - getHP());
			if (CEOpacity < 0)
			{
				CEOpacity = 0;
				getBossSprite()->setPosition(getSpawnPoint());
				phaseIn = false;
			}
			getBossSprite()->setOpacity(CEOpacity);
		}
		else
		{
			getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
			CEOpacity += dt * 510.f * (4.f - getHP());
			if (CEOpacity > 255.f)
			{
				CEOpacity = 255.f;
				phaseIn = true;
				portin = false;
				toSpawn = false;
			}
			getBossSprite()->setSpriteFrame(Monocle[(int)swing]);
			getBossSprite()->setOpacity(CEOpacity);
		}
	}
	else
	{
		if (attackType == 2)
			SHITHECANSEEME = true;
		else
			SHITHECANSEEME = false;
		if (!SHITHECANSEEME)
		{
			if (!portin)
			{
				timeUntilTeleport += dt;
				if (timeUntilTeleport > teleportTime + getHP() - 3.f)
				{
					timeUntilTeleport = 0;
					portin = true;
				}
				if ((getPlayerPosition() - getBossSprite()->getPosition()).length() < 48.f)
				{
					setSignal(Vec3(0, 0, 1));
					swinging = true;
				}
			}
			else
			{
				getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
				if (phaseIn)
				{
					if ((getPlayerPosition() - getBossSprite()->getPosition()).length() < 48.f)
					{
						setSignal(Vec3(0, 0, 1));
						swinging = true;
					}
					CEOpacity -= dt * 510.f * (4.f - getHP());
					if (CEOpacity < 0)
					{
						CEOpacity = 0;
						getBossSprite()->setPosition(getPlayerPosition());
						phaseIn = false;
					}
					getBossSprite()->setOpacity(CEOpacity);
				}
				else
				{
					getBossSprite()->getPhysicsBody()->setVelocity(Vec2(0, 0));
					CEOpacity += dt * 510.f * (4.f - getHP());
					if (CEOpacity > 255.f)
					{
						CEOpacity = 255.f;
						phaseIn = true;
						portin = false;
					}
					getBossSprite()->setOpacity(CEOpacity);
				}
			}
			getBossSprite()->setSpriteFrame(Monocle[(int)swing]);
		}
		else
		{
			swinging = false;
			swing = 0.f;
			exposed += dt;
			phaseIn = true;
			portin = false;
			CEOpacity = 255;
			getBossSprite()->setOpacity(CEOpacity);
			if ((getPlayerPosition() - getBossSprite()->getPosition()).length() < 48.f)
			{
				setSignal(Vec3(0, 0, 2));
				exposed = 0.f;
				toSpawn = true;
				timeUntilTeleport = 0.f;
			}
			else if (exposed > maximumExposure)
			{
				exposed = 0.f;
				setSignal(Vec3(0, 0, 3));
				toSpawn = true;
				timeUntilTeleport = 0.f;
			}
		}
	}
	if (swinging && swing < Monocle.size() && !SOUPAswinging)
	{
		swing += dt * 20.f;
		if (swing >= Monocle.size())
		{
			swing = 0.f;
			SOUPAswinging = true;
			swinging = false;
		}
	}
}

Vec2 CEO::getVectorTo(Vec2 place)
{
	if ((place - getPosition()).length() > 0)
	{
		Vec2 temp = place - getPosition();
		temp.normalize();
		return temp;
	}
	else
		return Vec2(0, 0);
}

void CEO::setTarget(Vec2 tar)
{
	target = tar;
}

Vec2 CEO::getTarget()
{
	return target;
}

void CEO::onDamage(float dt)
{
	SHITHECANSEEME = false;
}
