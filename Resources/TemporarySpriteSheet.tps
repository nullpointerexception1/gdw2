<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>C:/Users/100653937/Documents/GDW/Second2Die/SecondToDeath/Resources/TemporarySpriteSheet.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>TempSprSheet.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Arrow1.png</key>
            <key type="filename">Arrow2.png</key>
            <key type="filename">EmptyTile.png</key>
            <key type="filename">NothingTile.png</key>
            <key type="filename">SewerTile.png</key>
            <key type="filename">WallFront.png</key>
            <key type="filename">YOU.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">CloseNormal.png</key>
            <key type="filename">CloseSelected.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">HelloWorld.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,68,97,135</rect>
                <key>scale9Paddings</key>
                <rect>49,68,97,135</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">LoadIcon.png</key>
            <key type="filename">SaveIcon.png</key>
            <key type="filename">tile_carpet.png</key>
            <key type="filename">tile_path_T.png</key>
            <key type="filename">tile_path_corner.png</key>
            <key type="filename">tile_path_end.png</key>
            <key type="filename">tile_path_four.png</key>
            <key type="filename">tile_path_straight.png</key>
            <key type="filename">tile_tile.png</key>
            <key type="filename">../../Sprites/tile_concrete.png</key>
            <key type="filename">../../Sprites/tile_dirt.png</key>
            <key type="filename">../../Sprites/tile_path_outer_corner.png</key>
            <key type="filename">../../Sprites/tile_path_outer_corner2.png</key>
            <key type="filename">../../Sprites/tile_path_outer_double.png</key>
            <key type="filename">../../Sprites/tile_path_outer_opposing.png</key>
            <key type="filename">../../Sprites/tile_path_outer_straight.png</key>
            <key type="filename">../../Sprites/tile_path_outer_straight_corner.png</key>
            <key type="filename">../../Sprites/tile_path_outer_straight_corner2.png</key>
            <key type="filename">../../Sprites/tile_path_outer_triple.png</key>
            <key type="filename">../../Sprites/tile_wood.png</key>
            <key type="filename">../../Sprites/wall_basement_brick.png</key>
            <key type="filename">../../Sprites/wall_fancy_paper.png</key>
            <key type="filename">../../Sprites/wall_textured_lab.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MenuIcon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,15,128,30</rect>
                <key>scale9Paddings</key>
                <rect>64,15,128,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PlaceholderTitlescreen.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4622,3467,9245,6933</rect>
                <key>scale9Paddings</key>
                <rect>4622,3467,9245,6933</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Scrollbox.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,5,50,10</rect>
                <key>scale9Paddings</key>
                <rect>25,5,50,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>Arrow1.png</filename>
            <filename>Arrow2.png</filename>
            <filename>CloseNormal.png</filename>
            <filename>CloseSelected.png</filename>
            <filename>EmptyTile.png</filename>
            <filename>HelloWorld.png</filename>
            <filename>MenuIcon.png</filename>
            <filename>NothingTile.png</filename>
            <filename>Scrollbox.png</filename>
            <filename>SewerTile.png</filename>
            <filename>tile_carpet.png</filename>
            <filename>tile_path_corner.png</filename>
            <filename>tile_path_end.png</filename>
            <filename>tile_path_four.png</filename>
            <filename>tile_path_straight.png</filename>
            <filename>tile_path_T.png</filename>
            <filename>tile_tile.png</filename>
            <filename>WallFront.png</filename>
            <filename>YOU.png</filename>
            <filename>SaveIcon.png</filename>
            <filename>LoadIcon.png</filename>
            <filename>PlaceholderTitlescreen.png</filename>
            <filename>../../Sprites/tile_concrete.png</filename>
            <filename>../../Sprites/tile_dirt.png</filename>
            <filename>../../Sprites/tile_path_outer_corner.png</filename>
            <filename>../../Sprites/tile_path_outer_corner2.png</filename>
            <filename>../../Sprites/tile_path_outer_straight.png</filename>
            <filename>../../Sprites/wall_basement_brick.png</filename>
            <filename>../../Sprites/wall_textured_lab.png</filename>
            <filename>../../Sprites/tile_path_outer_double.png</filename>
            <filename>../../Sprites/tile_path_outer_straight_corner.png</filename>
            <filename>../../Sprites/tile_path_outer_straight_corner2.png</filename>
            <filename>../../Sprites/tile_path_outer_triple.png</filename>
            <filename>../../Sprites/tile_path_outer_opposing.png</filename>
            <filename>../../Sprites/tile_wood.png</filename>
            <filename>../../Sprites/wall_fancy_paper.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
