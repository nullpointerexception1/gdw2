#pragma once
#include "BossVirt.h"

using namespace::std;

class Minty : public boss
{
public:
	Minty(Scene* s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection);
	void spawn();
	void die();
	void move();
	Vec2 getVectorTo(Vec2 place);
	void setTarget(Vec2 tar);
	Vec2 getTarget();
private:
	Vec2 target;
};