#pragma once

#include "cocos2d.h"

using namespace::cocos2d;

class mbox
{
public:
	mbox();
	~mbox();
	int x, y, h, w;
	void setPos(int X, int Y);
	void setDim(int W, int H);
	void setPos(Vec2 v);
	void setDim(Vec2 v);
	Vec2 getPos();
	bool contains(Vec2 v);
};