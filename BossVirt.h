#pragma once
#include "cocos2d.h"
#include <vector>

using namespace::std;

USING_NS_CC;

class boss
{
public:
	boss(Scene* s, Vec2 pos, vector<vector<SpriteFrame*>> wc, int startDirection, int bossType);
	~boss();
	virtual void move() = 0;
	virtual void attack(int attackType, float dt);
	virtual void die() = 0;
	void deathSequence(float dt);
	virtual void spawn() = 0;
	virtual void onDamage(float dt) {};
	virtual void setCustomFrame() {};
	void damage(float damageAMT);
	void dialBackDamage(float dt);
	void incrementCycle(float dt);
	void setPosition(Vec2 pos);
	void setPosition(float x, float y);
	void setMaxSpeed(float ms);
	void setVelocity(Vec2 vel);
	void setVelocity(float x, float y);
	void setAcceleration(Vec2 acc);
	void setAcceleration(float x, float y);
	Vec2 getPosition();
	Vec2 getVelocity();
	Vec2 getAcceleration();
	float getMaxSpeed();
	void setHP(int h);
	int getHP();
	void setWalkFrame(int frameNumber, int direction);
	void setDeathFrame(int frameNumber);
	void setDirection(int dir);
	int getDirection();
	void setCycleFrame(int frame);
	void setDeathCycleFrame(int frame);
	int getDeathCycleFrame();
	int getCycleFrame();
	void loadWalkCycle(vector<vector<SpriteFrame*>> wc);
	void loadDeathCycle(vector<vector<SpriteFrame*>> dc);
	void addBossToScene(Scene* s);
	void removeBossFromScene();
	void setStandardAcc(float acc);
	float getStandardAcc();
	void conductMovement(float dt);
	Sprite* getBossSprite();
	void setBossPosition();
	void setAlive(bool state);
	bool isAlive();
	void setUseStandardAcc(bool state);
	bool isUsingStandardAcc();
	void setPlayerPosition(Vec2 pp);
	Vec2 getPlayerPosition();
	int getBossType();
	void setSpawnpoint(Vec2 pos);
	Vec2 getSpawnPoint();
	void setSignal(Vec3 v);
	Vec3 getSignal();
	int getSignalSize();
	void setBossCoolDown(float ff);
	float getBossCoolDown();
private:
	Vec2 position, velocity, acceleration, playerPosition, spawnPoint;
	vector <Vec3> signals;
	vector<vector<SpriteFrame*>> walkCycle;
	vector<vector<SpriteFrame*>> deathFrames;
	Sprite* bossSprite;
	int hp, cycleFrame = 0, direction = 0, deathFrame = 0, bossT, bossCoolDown = 0.f;
	float maxSpeed, standardAcc, walkTime = 0.04f, currentTime = 0.f, deathTime = 0.f, deathFrameTime = 0.1f, damaged = 0.f;
	bool alive, useStandardAcc;
	Scene* scene;
};