#include <string>
#include "LevelTile.h"

levelTile::levelTile()
{
	tile = Sprite::create("NothingTile.png");
	tile->setScale(4);
	tile->setPosition(0.f, 0.f);
}

levelTile::~levelTile()
{
}

levelTile::levelTile(std::string str)
{
	tile = Sprite::create(str);
	tile->setScale(4);
	tile->setPosition(0.f, 0.f);
}

levelTile::levelTile(float f1, float f2)
{
	tile = Sprite::create("NothingTile.png");
	tile->setScale(4);
	tile->setPosition(f1, f2);
}

levelTile::levelTile(Vec2 v)
{
	tile = Sprite::create("NothingTile.png");
	tile->setScale(4);
	tile->setPosition(v);
}

levelTile::levelTile(std::string str, float f1, float f2)
{
	tile = Sprite::create(str);
	tile->setScale(4);
	tile->setPosition(f1, f2);
}

levelTile::levelTile(std::string str, Vec2 v)
{
	tile = Sprite::create(str);
	tile->setScale(4);
	tile->setPosition(v);
}

void levelTile::setPos(float f1, float f2)
{
	tile->setPosition(f1, f2);
}

void levelTile::setPos(Vec2 v)
{
	tile->setPosition(v);
}

void levelTile::setSprite(std::string str)
{
	tile->setSpriteFrame(str);
}

void levelTile::setSolid(bool q)
{
	solid = q;
}
