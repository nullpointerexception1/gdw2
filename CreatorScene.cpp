#include <vector>
#include <fstream>
#include "mBox.h"
#include "CreatorScene.h"
#include "Titlescreen.h"
#include "SimpleAudioEngine.h"
#include "DisplayHandler.h"
#include "InputHandler.h"
#include <sstream>
#include <iterator>

USING_NS_CC;

Size visibleSizeG;
Vec2 originG, visibleSizeV;

int num = 1, mouz = 0, loadSel = 0;
bool deselect = true;
bool selmen = false;
bool aTest = true;
bool freeTurn = true;
bool toTest = true;

using namespace::std;

cocos2d::Scene * Creator::createScene()
{
	Scene* scene = Creator::create();
	//layer2 = Creator::create();
	//layer2->setTag(2);
	

	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	//scene->addChild(layer2, 1);
	//sceneHandle = scene;

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();

	return scene;
}

void Creator::onExit()
{
	Scene::onExit();
}

void Creator::onEnter()
{
	Scene::onEnter();
}

bool Creator::init()
{
	if (!Scene::init())
	{
		return false;
	}
	//DISPLAY->createDebugConsole(true);
	//std::cout << "THE CONSOLE IS NOW OPEN" << std::endl;
	//SpriteFrameCache::getInstance()->addSpriteFramesWithFile("TempSprSheet.plist");
	//SpriteFrameCache::getInstance()->addSpriteFramesWithFile("door.plist");

	Size visibleSize = Director::getInstance()->getVisibleSize();
	visibleSizeG = visibleSize;
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	originG = origin;
	visibleSizeV.x = visibleSizeG.width;
	visibleSizeV.y = visibleSizeG.height;

	Texture2D::TexParams texParams = { GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };

	ifstream fIn;
	fIn.open("AllSprite.txt");
	string mini;
	while (getline(fIn, mini))
	{
		Sprite* temp = Sprite::createWithSpriteFrameName(mini);
		temp->getTexture()->setTexParameters(texParams);
		temp->setScale(64.f / temp->getContentSize().width);
		temp->setPosition(-1 * temp->getContentSize() * temp->getScale());
		getline(fIn, mini);
		temp->setGlobalZOrder(stoi(mini) + 10);
		getline(fIn, mini);
		temp->setTag(stoi(mini));
		
		all.push_back(temp);
		string te = "";
		int sm = all.size() - 1;
		te += to_string(sm);
		all[sm]->setName(te);
		this->addChild(all[sm]);
	}
	fIn.close();

	bar = Sprite::createWithSpriteFrameName("Scrollbox.png");
	bar->setScale(80.f / bar->getContentSize().height);
	bar->setPosition(visibleSizeG.width / 2, visibleSizeG.height / 8);
	bar->setGlobalZOrder(9);

	al = Sprite::createWithSpriteFrameName("Arrow1.png");
	al->getTexture()->setTexParameters(texParams);
	ar = Sprite::createWithSpriteFrameName("Arrow2.png");
	ar->getTexture()->setTexParameters(texParams);
	al->setScale(64.f / al->getContentSize().width);
	ar->setScale(al->getScale());
	al->setPosition(bar->getPosition() - Vec2(1.2f * bar->getScale() * bar->getContentSize().width / 2, 0));
	ar->setPosition(bar->getPosition() + Vec2(1.2f * bar->getScale() * bar->getContentSize().width / 2, 0));
	al->setGlobalZOrder(10);
	ar->setGlobalZOrder(10);

	bl.setPos(al->getPosition() - al->getContentSize() / 2 * al->getScale());
	bl.setDim(al->getContentSize() * al->getScale());
	br.setPos(ar->getPosition() - ar->getContentSize() / 2 * ar->getScale());
	br.setDim(ar->getContentSize() * ar->getScale());
	bm.setPos(bar->getPosition() - bar->getContentSize() / 2 * bar->getScale());
	bm.setDim(bar->getContentSize() * bar->getScale());

	saveB = Sprite::createWithSpriteFrameName("SaveIcon.png");
	saveB->getTexture()->setTexParameters(texParams);
	saveB->setScale(64.f / saveB->getContentSize().width);
	saveB->setPosition(visibleSizeG.width * 0.9f, visibleSizeG.height * 0.9f);
	saveB->setGlobalZOrder(20);
	loadB = Sprite::createWithSpriteFrameName("LoadIcon.png");
	loadB->getTexture()->setTexParameters(texParams);
	loadB->setScale(64.f / saveB->getContentSize().width);
	loadB->setPosition(loadB->getPosition() - Vec2(80.f, 0.f));
	loadB->setGlobalZOrder(20);

	bs.setPos(saveB->getPosition() - saveB->getContentSize() / 2 * saveB->getScale());
	bs.setDim(saveB->getContentSize() * saveB->getScale());
	bo.setPos(bs.getPos() - Vec2(80.f, 0.f));
	bo.setDim(loadB->getContentSize() * loadB->getScale());

	for (int i = 0; i < 5; i++)
	{
		choices[i].setPos(Vec2(visibleSizeG.width / 2 - (2 - i) * bar->getContentSize().height * bar->getScale(), visibleSizeG.height / 8) - all[0]->getContentSize() / 2 * all[0]->getScale());
		choices[i].setDim(all[0]->getContentSize() * all[0]->getScale());
	}

	clik = Sprite::createWithSpriteFrame(all[0]->getSpriteFrame());
	clik->getTexture()->setTexParameters(texParams);
	clik->setName(all[0]->getName());
	clik->setScale(all[0]->getScale());
	clik->setPosition(0, 0);
	clik->setOpacity(81);
	clik->setGlobalZOrder(11);

	for (int i = 0; i < all.size(); i++)
		all[i]->setPosition(-1 * all[i]->getContentSize() * all[i]->getScale() + getDefaultCamera()->getPosition() - visibleSizeG / 2);

	for (int i = 0; i < 5; i++)
	{
		if (all.size() >(num - 1) * 5 + i)
		{
			all[(num - 1) * 5 + i]->setPosition(getDefaultCamera()->getPosition() - visibleSizeG / 2 + Vec2(visibleSizeG.width / 2 - (2 - i) * bar->getContentSize().height * bar->getScale(), visibleSizeG.height / 8));
		}
	}

	for (int i = 0; i < maxW; i++)
	{
		std::vector<std::vector<Sprite*>> ppHold;
		for (int j = 0; j < maxH; j++)
		{
			std::vector <Sprite*> pHold2;
			Sprite* pHold = Sprite::createWithSpriteFrame(all[1]->getSpriteFrame());
			pHold->getTexture()->setTexParameters(texParams);
			pHold->setName(all[1]->getName());
			pHold->setPosition((i - maxW / 2) * 64.f, (j) * 64.f);
			pHold->setScale(64.f / pHold->getContentSize().width);
			pHold->setTag(all[1]->getTag());
			pHold->setGlobalZOrder(1);
			pHold2.push_back(pHold);
			ppHold.push_back(pHold2);
		}
		MapT.push_back(ppHold);
	}
	for (int i = 0; i < maxW * maxH; i++)
	{
		this->addChild(MapT[i % maxW][i / maxW][0]);
		MapT[i % maxW][i / maxW][0]->setVisible(false);
		//vertz[i % maxW][i / maxW] = Vec2(MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[0].vertices.x, MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[0].vertices.y);
		//if (i / maxW == maxH - 1)
		//	vertz[i % maxW][i / maxW + 1] = Vec2(MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[2].vertices.x, MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[2].vertices.y);
		//if (i % maxW == maxW - 1)
		//	vertz[i % maxW + 1][i / maxW] = Vec2(MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[1].vertices.x, MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[1].vertices.y);
		//if (i % maxW == maxW - 1 && i / maxW == maxH - 1)
		//	vertz[i % maxW + 1][i / maxW + 1] = Vec2(MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[3].vertices.x, MapT[i % maxW][i / maxW][0]->getPolygonInfo().triangles.verts[3].vertices.y);
	}
	for (int i = 0; i < maxW * maxH; i++)
	{
		
	}
	//for (int i = 0; i < maxW * maxH; i++)
	//{
	//}
	nameOfSave = Label::createWithSystemFont("", "Times New Roman", 30);
	nameOfSave->setVisible(false);
	nameOfSave->setGlobalZOrder(25);
	nameOfSave->setColor(Color3B::BLACK);

	saveScreen = Sprite::create("SaveScreen.png");
	saveScreen->setVisible(false);
	saveScreen->setGlobalZOrder(24);

	saveMap = Sprite::createWithSpriteFrameName("MenuIcon.png");
	saveMap->setVisible(false);
	saveMap->setGlobalZOrder(26);
	saveMap->setScale(0.8f);

	cancelSave = Sprite::createWithSpriteFrameName("MenuIcon.png");
	cancelSave->setVisible(false);
	cancelSave->setGlobalZOrder(26);
	cancelSave->setScale(0.8f);

	saveMapL = Label::createWithSystemFont("Save", "Times New Roman", 20);
	saveMapL->setVisible(false);
	saveMapL->setGlobalZOrder(27);
	saveMapL->setColor(Color3B::BLACK);

	saveMapB.setDim(saveMap->getContentSize() * saveMap->getScale());
	saveMapB.setPos(saveMap->getPosition() - saveMap->getContentSize() / 2 * saveMap->getScale());

	cancelSaveL = Label::createWithSystemFont("Cancel", "Times New Roman", 28);
	cancelSaveL->setVisible(false);
	cancelSaveL->setGlobalZOrder(27);
	cancelSaveL->setColor(Color3B::BLACK);
	
	cancelSaveB.setDim(cancelSave->getContentSize() * cancelSave->getScale());
	cancelSaveB.setPos(cancelSave->getPosition() - cancelSave->getContentSize() / 2 * cancelSave->getScale());

	for (int i = 0; i < 5; i++)
	{
		saveSlots[i] = Label::createWithSystemFont("-", "Times New roman", 24);
		saveSlots[i]->setVisible(false);
		saveSlots[i]->setGlobalZOrder(27);
		saveSlots[i]->setColor(Color3B::BLACK);

		saveSlotsB[i].setPos(saveScreen->getPosition().x - saveScreen->getContentSize().width / 2 * saveScreen->getScale(), saveSlots[i]->getPosition().y - saveSlots[i]->getContentSize().height / 2 * saveSlots[i]->getScale());
		saveSlotsB[i].setDim(saveScreen->getContentSize().width * saveScreen->getScale(), saveSlots[i]->getContentSize().height * saveSlots[i]->getScale());
	}

	loadScreen = Sprite::create("loadScreen.png");
	loadScreen->setVisible(false);
	loadScreen->setGlobalZOrder(24);

	al2 = Sprite::createWithSpriteFrameName("Arrow1.png");
	al2->getTexture()->setTexParameters(texParams);
	ar2 = Sprite::createWithSpriteFrameName("Arrow2.png");
	ar2->getTexture()->setTexParameters(texParams);
	al2->setScale(64.f / al->getContentSize().width);
	ar2->setScale(al->getScale());
	al2->setPosition(saveScreen->getPosition() - Vec2(200, 80));
	ar2->setPosition(saveScreen->getPosition() + Vec2(200, -80));
	al2->setGlobalZOrder(27);
	ar2->setGlobalZOrder(27);
	al2->setVisible(false);
	ar2->setVisible(false);

	bl2.setPos(al2->getPosition() - al2->getContentSize() / 2 * al2->getScale());
	bl2.setDim(al2->getContentSize() * al2->getScale());
	br2.setPos(ar2->getPosition() - ar2->getContentSize() / 2 * ar2->getScale());
	br2.setDim(ar2->getContentSize() * ar2->getScale());

	leave = Sprite::create("ExitDoor..png");
	leave->getTexture()->setTexParameters(texParams);
	leave->setScale(64.f / leave->getContentSize().height);
	leave->setPosition(visibleSize.width * 0.1f, visibleSize.height * 0.9f);
	leave->setGlobalZOrder(20);

	leaveB.setPos(leave->getPosition() - leave->getContentSize() * 0.5f * leave->getScale());
	leaveB.setDim(leave->getContentSize() * leave->getScale());

	this->addChild(leave);
	this->addChild(saveMap);
	this->addChild(cancelSave);
	this->addChild(nameOfSave);
	this->addChild(saveScreen);
	this->addChild(saveMapL);
	this->addChild(cancelSaveL);
	this->addChild(bar);
	this->addChild(al);
	this->addChild(ar);
	this->addChild(clik);
	this->addChild(saveB);
	this->addChild(loadB);
	for (int i = 0; i < 5; i++)
		this->addChild(saveSlots[i]);
	this->addChild(loadScreen);
	this->addChild(al2);
	this->addChild(ar2);

	prep = Vec2(0, 0);
	forTyping = "";	
	
	Creator::scheduleUpdate();
	return true;
}

void Creator::update(float dt)
{
	Vec2 rt = INPUTS->getMousePosition() + getDefaultCamera()->getPosition() + Vec2(32.f, 32.f);
	if (floor((rt.x - visibleSizeV.x / 2) / 64) >= -maxW / 2 && floor((rt.x - visibleSizeV.x / 2) / 64) < maxW / 2 && floor((rt.y - visibleSizeV.y / 2) / 64) >= 0 && floor((rt.y - visibleSizeV.y / 2) / 64) < maxH)
		clik->setPosition(64.f * Vec2(floor((rt.x) / 64), floor((rt.y) / 64)) - visibleSizeG / 2);
	if (num == 1)
	{
		al->setOpacity(81);
	}
	else
	{
		al->setOpacity(255);
	}
	if (num * 5 >= all.size())
	{
		ar->setOpacity(81);
	}
	else
	{
		ar->setOpacity(255);
	}
	//for (int i = 0; i < 5; i++)
	//{
	//	if (all.size() > (num - 1) * 5 + i)
	//	{
	//		all[(num - 1) * 5 + i]->setPosition(Vec2(visibleSizeG.width / 2 - (2 - i) * bar->getContentSize().height * bar->getScale(), visibleSizeG.height / 8));
	//	}
	//}
	Vec2 scru = prep - visibleSizeV / 2;
	Vec2 scrd = prep + visibleSizeV / 2;
	for (int i = scru.x / 64 - 1; i < scrd.x / 64 + 1; i++)
	{
		for (int j = scru.y / 64 - 1; j < scrd.y / 64 + 1; j++)
		{
			if (i > -maxW / 2 && i < maxW / 2 && j > 0 && j < maxH)
			{
				for (int k = 0; k < MapT[maxW / 2 + i][j].size(); k++)
				{
					MapT[maxW / 2 + i][j][k]->setVisible(false);
				}
			}
		}
	}
	scru = getDefaultCamera()->getPosition() - visibleSizeV / 2;
	scrd = getDefaultCamera()->getPosition() + visibleSizeV / 2;
	for (int i = scru.x / 64 - 1; i < scrd.x / 64 + 1; i++)
	{
		for (int j = scru.y / 64 - 1; j < scrd.y / 64 + 1; j++)
		{
			if (i >= -maxW / 2 && i < maxW / 2 && j >= 0 && j < maxH)
			{
				for (int k = 0; k < MapT[maxW/2 + i][j].size(); k++)
					//if (MapT[maxW / 2 + i][j][k]->getName() != "1")
						MapT[maxW / 2 + i][j][k]->setVisible(true);
			}
		}
	}
	prep = getDefaultCamera()->getPosition();
	updateInputs(dt);
}

void Creator::updateInputs(float dt)
{
	if (aTest)
	{
		updateKeyboardInputs(dt);
		updateMouseInputs(dt);
	}
	else
	{
		saving();
	}
}

void Creator::updateMouseInputs(float dt)
{
	Texture2D::TexParams texParams = { GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };
	if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
	{
		deselect = true;
	}
	if (selmen)
	{
		int urChoice = 0;
		for (int i = 0; i < 5; i++)
		{
			if (loadSel * 5 + i < namesOfLevels.size())
			{
				saveSlots[i]->setString(namesOfLevels[loadSel * 5 + i]);
			}
			else
			{
				saveSlots[i]->setString("-");
			}
		}

		if (bl2.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && loadSel > 0 && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				deselect = false;
				loadSel--;
			}
		}
		else if (br2.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && loadSel < (namesOfLevels.size() - 1) / 5 && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				loadSel++;
				deselect = false;
			}
		}
		if (cancelSaveB.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			cancelSave->setScale(1.2f);
			cancelSave->setGlobalZOrder(28);
			cancelSaveL->setScale(1.5f);
			cancelSaveL->setGlobalZOrder(29);
			if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT))
			{
				deselect = false;
				loadSel = 0;
				namesOfLevels.clear();
				selmen = false;
				loadScreen->setVisible(false);
				cancelSave->setVisible(false);
				cancelSaveL->setVisible(false);
				for (int i = 0; i < 5; i++)
				{
					saveSlots[i]->setVisible(false);
				}
				al2->setVisible(false);
				ar2->setVisible(false);
			}
		}
		else
		{
			cancelSave->setScale(0.8f);
			cancelSave->setGlobalZOrder(26);
			cancelSaveL->setScale(1.0f);
			cancelSaveL->setGlobalZOrder(27);
		}

		for (int i = 0; i < 5; i++)
			if (saveSlotsB[i].contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
				if (deselect && saveSlots[i]->getString() != "-" && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
				{
					transmitterz.clear();
					receiverz.clear();
					deselect = false;
					loadSel = 0;
					namesOfLevels.clear();
					forTyping = (saveSlots[i]->getString());
					int starter = 0;
					ifstream saveFile;
					string *toOpen;
					saveFile.open(forTyping + "S2DF.txt");
					string s;
					vector <string> ss;
					while (getline(saveFile, s))
					{
						stringstream into(s);
						while (getline(into, s, '/'))
						{
							ss.push_back(s);
						}
						for (int i = MapT[starter % maxW][starter / maxW].size() - 1; i >= 0; i--)
						{
							MapT[starter % maxW][starter / maxW][i]->setVisible(false);
							this->removeChild(MapT[starter % maxW][starter / maxW][i]);
						}
						MapT[starter % maxW][starter / maxW].clear();
						for (int i = 0; i < ss.size(); i++)
						{
							string a = ss[i];
							stringstream into2(a);
							vector <string> sss;
							while (getline(into2, a, '='))
							{
								sss.push_back(a);
							}
							Sprite* temp = Sprite::createWithSpriteFrame(all[stoi(sss[0])]->getSpriteFrame());
							temp->getTexture()->setTexParameters(texParams);
							temp->setName(sss[0]);
							temp->setScale(64.f / temp->getContentSize().width);
							temp->setPosition((starter % maxW - maxW / 2) * 64.f, (starter / maxW) * 64.f);
							temp->setGlobalZOrder(stoi(sss[1]));
							temp->setRotation(stoi(sss[2]) * 90);
							temp->setTag(stoi(sss[3]));
							MapT[starter % maxW][starter / maxW].push_back(temp);
							this->addChild(MapT[starter % maxW][starter / maxW][i]);
						}
						ss.clear();
						starter++;
					}
					for (int i = 0; i < maxW * maxH; i++)
					{
						for (int k = 0; k < MapT[i % maxW][i / maxW].size(); k++)
							MapT[i % maxW][i / maxW][k]->setVisible(false);
					}
					saveFile.close();
					saveFile.open(forTyping + "S2DW.txt");
					while (getline(saveFile, s))
					{
						stringstream into(s);
						while (getline(into, s, ','))
						{
							ss.push_back(s);
						}
						transmitterz.push_back(Vec2(stoi(ss[0]), stoi(ss[1])));
						receiverz.push_back(Vec2(stoi(ss[2]), stoi(ss[3])));
						ss.clear();
					}
					saveFile.close();
					saveFile.open(forTyping + "S2DT.txt");
					getline(saveFile, s);

					stringstream into(s);
					while (getline(into, s, ','))
					{
						ss.push_back(s);
					}
					start = Vec2(stoi(ss[0]), stoi(ss[1]));
					end = Vec2(stoi(ss[2]), stoi(ss[3]));
					ss.clear();
					saveFile.close();
					saveFile.open(forTyping + "S2DS.txt");
					signLoc.clear();
					while (getline(saveFile, s))
					{
						stringstream into(s);
						while (getline(into, s, ','))
						{
							ss.push_back(s);
						}
						signLoc.push_back(Vec2(stoi(ss[0]), stoi(ss[1])));
						signStr.push_back(ss[2]);
						ss.clear();
					}
					saveFile.close();
					selmen = false;
					loadScreen->setVisible(false);
					cancelSave->setVisible(false);
					cancelSaveL->setVisible(false);
					for (int i = 0; i < 5; i++)
					{
						saveSlots[i]->setVisible(false);
					}
					al2->setVisible(false);
					ar2->setVisible(false);
				}

		if (loadSel == 0)
			al2->setOpacity(81);
		else
			al2->setOpacity(255);
		if (loadSel == (namesOfLevels.size() - 1) / 5 || namesOfLevels.size() == 0)
			ar2->setOpacity(81);
		else
			ar2->setOpacity(255);

		//int starter = 0;
		//ifstream saveFile;
		//string *toOpen;
		//saveFile.open("RenameThisSavedFileSoItIsNotOverriden.txt");
		//string s;
		//vector <string> ss;
		//while (getline(saveFile, s))
		//{
		//	stringstream into(s);
		//	while (getline(into, s, '/'))
		//	{
		//		ss.push_back(s);
		//	}
		//	for (int i = MapT[starter % maxW][starter / maxW].size() - 1; i >= 0; i--)
		//	{
		//		MapT[starter % maxW][starter / maxW][i]->setVisible(false);
		//		this->removeChild(MapT[starter % maxW][starter / maxW][i]);
		//	}
		//	MapT[starter % maxW][starter / maxW].clear();
		//	for (int i = 0; i < ss.size(); i++)
		//	{
		//		string a = ss[i];
		//		stringstream into2(a);
		//		vector <string> sss;
		//		while (getline(into2, a, '='))
		//		{
		//			sss.push_back(a);
		//		}
		//		Sprite* temp = Sprite::createWithSpriteFrame(all[stoi(sss[0])]->getSpriteFrame());
		//		temp->setName(sss[0]);
		//		temp->setScale(64.f / temp->getContentSize().width);
		//		temp->setPosition((starter % maxW - maxW / 2) * 64.f, (starter / maxW) * 64.f);
		//		temp->setGlobalZOrder(stoi(sss[1]));
		//		temp->setRotation(stoi(sss[2]) * 90);
		//		MapT[starter % maxW][starter / maxW].push_back(temp);
		//		this->addChild(MapT[starter % maxW][starter / maxW][i]);
		//	}
		//	ss.clear();
		//	starter++;
		//}
		//for (int i = 0; i < maxW * maxH; i++)
		//{
		//	for (int k = 0; k < MapT[i % maxW][i / maxW].size(); k++)
		//		MapT[i % maxW][i / maxW][k]->setVisible(false);
		//}
		//saveFile.close();
		//selmen = false;
	}
	else
	{
		if (bl.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && num > 1 && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				for (int i = 0; i < 5; i++)
				{
					if (all.size() > (num - 1) * 5 + i)
					{
						all[(num - 1) * 5 + i]->setPosition(-1 * all[i]->getContentSize() * all[i]->getScale() + getDefaultCamera()->getPosition() - visibleSizeG / 2);
					}
				}
				num--;
				for (int i = 0; i < 5; i++)
				{
					if (all.size() > (num - 1) * 5 + i)
					{
						all[(num - 1) * 5 + i]->setPosition(getDefaultCamera()->getPosition() - visibleSizeG / 2 + Vec2(visibleSizeG.width / 2 - (2 - i) * bar->getContentSize().height * bar->getScale(), visibleSizeG.height / 8));
					}
				}
				deselect = false;
			}
		}
		else if (br.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && num * 5 <= all.size() && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				for (int i = 0; i < 5; i++)
				{
					if (all.size() > (num - 1) * 5 + i)
					{
						all[(num - 1) * 5 + i]->setPosition(-1 * all[i]->getContentSize() * all[i]->getScale() + getDefaultCamera()->getPosition() - visibleSizeG / 2);
					}
				}
				num++;
				for (int i = 0; i < 5; i++)
				{
					if (all.size() > (num - 1) * 5 + i)
					{
						all[(num - 1) * 5 + i]->setPosition(getDefaultCamera()->getPosition() - visibleSizeG / 2 + Vec2(visibleSizeG.width / 2 - (2 - i) * bar->getContentSize().height * bar->getScale(), visibleSizeG.height / 8));
					}
				}
				deselect = false;
			}
		}
		else if (bm.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect)
				for (int i = 0; i < 5; i++)
				{
					if (/*deselect && */choices[i].contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
					{
						if ((num - 1) * 5 + i < all.size() && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
						{
							clik->setSpriteFrame(all[(num - 1) * 5 + i]->getSpriteFrame());
							clik->setScale(all[(num - 1) * 5 + i]->getScale());
							clik->setName(all[(num - 1) * 5 + i]->getName());
							clik->setGlobalZOrder(all[(num - 1) * 5 + i]->getGlobalZOrder());
							//deselec	t = false;
							clik->setRotation(0);
							clik->setTag(all[(num - 1) * 5 + i]->getTag());
						}
					}
				}
		}
		else if (bs.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT))
			{
				deselect = false;
				//insert code to save here
				nameOfSave->setVisible(true);
				saveScreen->setVisible(true);
				saveMap->setVisible(true);
				cancelSave->setVisible(true);
				saveMapL->setVisible(true);
				cancelSaveL->setVisible(true);
				aTest = false;
				//ofstream saveFile;
				//saveFile.open("RenameThisSavedFileSoItIsNotOverriden.txt");
				//for (int i = 0; i < maxW * maxH; i++)
				//{
				//	for (int j = 0; j < MapT[i % maxW][i / maxW].size(); j++)
				//	{
				//		saveFile << MapT[i % maxW][i / maxW][j]->getName() << "=" << MapT[i % maxW][i / maxW][j]->getGlobalZOrder() << "=" << (int)(MapT[i % maxW][i / maxW][j]->getRotation() / 90);
				//		if (j < MapT[i % maxW][i / maxW].size() - 1)
				//			saveFile << "/";
				//	}
				//	saveFile << endl;
				//}
				//saveFile.close();
			}
		}
		else if (bo.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT))
			{
				deselect = false;
				selmen = true;
				ofstream nOpe;
				nOpe.open("CollectionOfS2DSaves.txt", fstream::app);
				nOpe.close();
				ifstream allUrLevels;
				allUrLevels.open("CollectionOfS2DSaves.txt");
				string toHold;
				while (getline(allUrLevels, toHold))
				{
					namesOfLevels.push_back(toHold);
				}
				loadScreen->setVisible(true);
				cancelSave->setVisible(true);
				cancelSaveL->setVisible(true);
				for (int i = 0; i < 5; i++)
				{
					saveSlots[i]->setVisible(true);
					if (i < namesOfLevels.size())
						saveSlots[i]->setString(namesOfLevels[i]);
				}
				al2->setVisible(true);
				ar2->setVisible(true);
				al2->setOpacity(81);
				if (namesOfLevels.size() <= 5)
					ar2->setOpacity(81);

			}
		}
		else if (leaveB.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
		{
			if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT))
			{
				deselect = false;
				Scene* s = Title::create();
				Director::getInstance()->replaceScene(s);
			}
		}
		else
		{
			if (INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				int replacer = -1;
				for (int i = 0; i < MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].size(); i++)
				{
					if (clik->getGlobalZOrder() - 10 == MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][i]->getGlobalZOrder())
						replacer = i;
				}
				if (replacer >= 0)
				{
					int tempTag = MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->getTag();
					if ((tempTag == 3 || tempTag == 7 || tempTag == 8 || tempTag == 9) && clik->getTag() != 3 && clik->getTag() != 7 && clik->getTag() != 8 && clik->getTag() != 9)
					{
						for (int j = 0; j < transmitterz.size(); j++)
						{
							if ((int)clik->getPosition().x / 64 + maxW / 2 == transmitterz[j].x && (int)clik->getPosition().y / 64 == transmitterz[j].y)
							{
								transmitterz.erase(transmitterz.begin() + j);
								if (j < receiverz.size())
								{
									receiverz.push_back(receiverz[j]);
									receiverz.erase(receiverz.begin() + j);
								}
							}
						}
					}
					else if ((tempTag == 4 || tempTag == 13) && clik->getTag() != 4 && clik->getTag() != 13)
					{
						for (int j = 0; j < receiverz.size(); j++)
						{
							if ((int)clik->getPosition().x / 64 + maxW / 2 == receiverz[j].x && (int)clik->getPosition().y / 64 == receiverz[j].y)
							{
								if (j < transmitterz.size())
								{
									transmitterz.push_back(transmitterz[j]);
									transmitterz.erase(transmitterz.begin() + j);
								}
								receiverz.erase(receiverz.begin() + j);
							}
						}
					}
					else if (tempTag == 11 && clik->getTag() != 11)
					{
						start = Vec2(0, -1);
					}
					else if (tempTag == 12 && clik->getTag() != 12)
					{
						end = Vec2(0, -1);
					}
					else if (tempTag == 18 && clik->getTag() != 18)
					{
						for (int j = 0; j < signLoc.size(); j++)
						{
							if ((int)clik->getPosition().x / 64 + maxW / 2 == signLoc[j].x && (int)clik->getPosition().y / 64 == signLoc[j].y)
							{
								signLoc.erase(signLoc.begin() + j);
								signStr.erase(signStr.begin() + j);
							}
						}
					}
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setSpriteFrame(clik->getSpriteFrame());
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setScale(clik->getScale());
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setName(clik->getName());
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setGlobalZOrder(clik->getGlobalZOrder() - 10);
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setRotation(clik->getRotation());
					MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->setTag(clik->getTag());
					int tempTag1 = MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][replacer]->getTag();
					if ((clik->getTag() == 3 || clik->getTag() == 7 || clik->getTag() == 8 || clik->getTag() == 9) && tempTag1 != 3 && tempTag1 != 7 && tempTag1 != 8 && tempTag1 != 9)
					{
						transmitterz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
					}
					//else if (clik->getTag() == 7 && tempTag1 != 3 && tempTag1 != 7 && tempTag1 != 8 && tempTag1 != 9)
					//{
					//	transmitterz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
					//}
					//else if (clik->getTag() == 8 && tempTag1 != 3 && tempTag1 != 7 && tempTag1 != 8 && tempTag1 != 9)
					//{
					//	transmitterz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
					//}
					//else if (clik->getTag() == 9 && tempTag1 != 3 && tempTag1 != 7 && tempTag1 != 8 && tempTag1 != 9)
					//{
					//	transmitterz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
					//}
					else if ((clik->getTag() == 4 || clik->getTag() == 13) && tempTag1 != 4 && tempTag != 13)
					{
						receiverz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
					}
					else if (clik->getTag() == 11 && tempTag1 != 11)
					{
						if (start.y >= 0)
							for (int k = 0; k < MapT[(int)start.x][(int)start.y].size(); k++)
							{
								if (MapT[(int)start.x][(int)start.y][k]->getTag() == 11)
								{
									this->removeChild(MapT[(int)start.x][(int)start.y][k]);
									MapT[(int)start.x][(int)start.y].erase(MapT[(int)start.x][(int)start.y].begin() + k);
									k--;
								}
							}
						start = Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64);
					}
					else if (clik->getTag() == 12 && tempTag1 != 12)
					{
						if (end.y >= 0)
							for (int k = 0; k < MapT[(int)end.x][(int)end.y].size(); k++)
							{
								if (MapT[(int)end.x][(int)end.y][k]->getTag() == 12)
								{
									this->removeChild(MapT[(int)end.x][(int)end.y][k]);
									MapT[(int)end.x][(int)end.y].erase(MapT[(int)end.x][(int)end.y].begin() + k);
									k--;
								}
							}
						end = Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64);
					}
					else if (clik->getTag() == 18 && tempTag1 != 18)
					{
						signLoc.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
						signStr.push_back("ui_sign.png");
					}
					if (clik->getName() == "1")
					{
						Vec2 pp = MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][0]->getPosition();
						for (int i = 0; i < MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].size(); i++)
						{
							int tempTag2 = MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][i]->getTag();
							if (tempTag2 == 3 || tempTag2 == 7 || tempTag2 == 8 || tempTag2 == 9)
							{
								for (int j = 0; j < transmitterz.size(); j++)
								{
									if ((int)clik->getPosition().x / 64 + maxW / 2 == transmitterz[j].x && (int)clik->getPosition().y / 64 == transmitterz[j].y)
									{
										transmitterz.erase(transmitterz.begin() + j);
										if (j < receiverz.size())
										{
											receiverz.push_back(receiverz[j]);
											receiverz.erase(receiverz.begin() + j);
										}
									}
								}
							}
							else if (tempTag2 == 4 || tempTag2 == 13)
							{
								for (int j = 0; j < receiverz.size(); j++)
								{
									if ((int)clik->getPosition().x / 64 + maxW / 2 == receiverz[j].x && (int)clik->getPosition().y / 64 == receiverz[j].y)
									{
										if (j < transmitterz.size())
										{
											transmitterz.push_back(transmitterz[j]);
											transmitterz.erase(transmitterz.begin() + j);
										}
										receiverz.erase(receiverz.begin() + j);
									}
								}
							}
							else if (tempTag2 == 11)
							{
								start = Vec2(0, -1);
							}
							else if (tempTag2 == 12)
							{
								end = Vec2(0, -1);
							}
							else if (tempTag2 == 18);
							{
								for (int j = 0; j < signLoc.size(); j++)
								{
									if ((int)clik->getPosition().x / 64 + maxW / 2 == signLoc[j].x && (int)clik->getPosition().y / 64 == signLoc[j].y)
									{
										signLoc.erase(signLoc.begin() + j);
										signStr.erase(signStr.begin() + j);
									}
								}
							}
							this->removeChild(MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][i]);
						}
						MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].clear();
						Sprite* oops = Sprite::createWithSpriteFrame(clik->getSpriteFrame());
						oops->getTexture()->setTexParameters(texParams);
						oops->setScale(clik->getScale());
						oops->setName(clik->getName());
						oops->setGlobalZOrder(clik->getGlobalZOrder() - 10);
						oops->setTag(clik->getTag());
						oops->setPosition(pp);
						MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].push_back(oops);
						this->addChild(MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][0]);
					}
				}
				else
				{
					if (MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][0]->getName() != "1")
					{
						if (clik->getTag() == 3 || clik->getTag() == 7 || clik->getTag() == 8 || clik->getTag() == 9)
						{
							transmitterz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
						}
						else if (clik->getTag() == 4 || clik->getTag() == 13)
						{
							receiverz.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
						}
						else if (clik->getTag() == 11)
						{
							if (start.y >= 0)
								for (int k = 0; k < MapT[(int)start.x][(int)start.y].size(); k++)
								{
									if (MapT[(int)start.x][(int)start.y][k]->getTag() == 11)
									{
										this->removeChild(MapT[(int)start.x][(int)start.y][k]);
										MapT[(int)start.x][(int)start.y].erase(MapT[(int)start.x][(int)start.y].begin() + k);
										k--;
									}
								}
							start = Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64);
						}
						else if (clik->getTag() == 12)
						{
							if (end.y >= 0)
								for (int k = 0; k < MapT[(int)end.x][(int)end.y].size(); k++)
								{
									if (MapT[(int)end.x][(int)end.y][k]->getTag() == 12)
									{
										this->removeChild(MapT[(int)end.x][(int)end.y][k]);
										MapT[(int)end.x][(int)end.y].erase(MapT[(int)end.x][(int)end.y].begin() + k);
										k--;
									}
								}
							end = Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64);
						}
						else if (clik->getTag() == 18)
						{
							signLoc.push_back(Vec2((int)clik->getPosition().x / 64 + maxW / 2, (int)clik->getPosition().y / 64));
							signStr.push_back("ui_sign.png");
						}
						Sprite* oops = Sprite::createWithSpriteFrame(clik->getSpriteFrame());
						oops->getTexture()->setTexParameters(texParams);
						oops->setScale(clik->getScale());
						oops->setName(clik->getName());
						oops->setGlobalZOrder(clik->getGlobalZOrder() - 10);
						oops->setTag(clik->getTag());
						oops->setRotation(clik->getRotation());
						oops->setPosition(MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][0]->getPosition());
						MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].push_back(oops);
						this->addChild(MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64][MapT[(int)clik->getPosition().x / 64 + maxW / 2][(int)clik->getPosition().y / 64].size() - 1]);
					}
				}
				deselect = false;
			}
		}
	}
}

void Creator::updateKeyboardInputs(float dt)
{
	Vec2 v = Vec2(0, 0);
	if (INPUTS->getKey(KeyCode::KEY_Q))
	{
		if (clik->getGlobalZOrder() > 11 && freeTurn)
		{
			float f = clik->getRotation() - 90.f;
			if (f < 0.f)
				f += 360.f;
			clik->setRotation(f);
			freeTurn = false;
		}
	}
	else if (INPUTS->getKey(KeyCode::KEY_E))
	{
		if (clik->getGlobalZOrder() > 11 && freeTurn)
		{
			float f = clik->getRotation() + 90.f;
			if (f >= 360.f)
				f -= 360.f;
			clik->setRotation(f);
			freeTurn = false;
		}
	}
	else
	{
		freeTurn = true;
	}
	if (INPUTS->getKey(KeyCode::KEY_W))
	{
		v += Vec2(0.f, 1.f);
	}
	if (INPUTS->getKey(KeyCode::KEY_A))
	{
		v += Vec2(-1.f, 0.f);
	}
	if (INPUTS->getKey(KeyCode::KEY_S))
	{
		v += Vec2(0.f, -1.f);
	}
	if (INPUTS->getKey(KeyCode::KEY_D))
	{
		v += Vec2(1.f, 0.f);
	}
	if (v.length() > 0.1)
		v.normalize();
	v *= 300.f;
	if (INPUTS->getKey(KeyCode::KEY_SHIFT))
	{
		v *= 3.f;
	}
	if ((getDefaultCamera()->getPosition() + v * dt).y < 0)
	{
		v.y = -(getDefaultCamera()->getPosition()).y / dt;
	}
	else if ((getDefaultCamera()->getPosition() + v * dt).y > (float)(maxH - 1) * 64.f)
	{
		v.y = (((float)(maxH - 1)) * 64.f - (getDefaultCamera()->getPosition()).y) / dt;
	}
	if ((getDefaultCamera()->getPosition() + v * dt).x > (float)(maxW - 1) * 32.f)
	{
		v.x = (((float)(maxW - 1)) * 32.f - (getDefaultCamera()->getPosition()).x) / dt;
	}
	else if ((getDefaultCamera()->getPosition() + v * dt).x < (float)(maxW - 1) * -32.f)
	{
		v.x = (((float)(maxW - 1)) * -32.f - (getDefaultCamera()->getPosition()).x) / dt;
	}
	getDefaultCamera()->setPosition(getDefaultCamera()->getPosition() + v * dt);
	bar->setPosition(getDefaultCamera()->getPosition() - visibleSizeV / 2 + Vec2(visibleSizeG.width / 2, visibleSizeG.height / 8));
	al->setPosition(bar->getPosition() - Vec2(1.2f * bar->getScale() * bar->getContentSize().width / 2, 0));
	ar->setPosition(bar->getPosition() + Vec2(1.2f * bar->getScale() * bar->getContentSize().width / 2, 0));
	saveB->setPosition(getDefaultCamera()->getPosition() + visibleSizeV * 0.4f);
	loadB->setPosition(saveB->getPosition() - Vec2(80.f, 0.f));
	bs.setPos(saveB->getPosition() - saveB->getContentSize() / 2 * saveB->getScale());
	bo.setPos(bs.getPos() - Vec2(80.f, 0.f));
	bl.setPos(al->getPosition() - al->getContentSize() / 2 * al->getScale());
	br.setPos(ar->getPosition() - ar->getContentSize() / 2 * ar->getScale());
	bm.setPos(bar->getPosition() - bar->getContentSize() / 2 * bar->getScale());
	nameOfSave->setPosition(getDefaultCamera()->getPosition());
	saveScreen->setPosition(getDefaultCamera()->getPosition() - Vec2(0.f, 25.f));
	saveMap->setPosition(saveScreen->getPosition() + Vec2(-120.f, -120.f));
	cancelSave->setPosition(saveScreen->getPosition() + Vec2(120.f, -120.f));
	saveMapL->setPosition(saveMap->getPosition());
	cancelSaveL->setPosition(cancelSave->getPosition());
	saveMapB.setPos(saveMap->getPosition() - saveMap->getContentSize() / 2* saveMap->getScale());
	cancelSaveB.setPos(cancelSave->getPosition() - cancelSave->getContentSize() / 2 * 0.8f);
	loadScreen->setPosition(saveScreen->getPosition());
	for (int i = 0; i < 5; i++)
	{
		saveSlots[i]->setPosition(saveScreen->getPosition().x, saveScreen->getPosition().y + 80 - 40 * (i - 2));
		saveSlotsB[i].setPos(saveScreen->getPosition().x - saveScreen->getContentSize().width / 2 * saveScreen->getScale(), saveSlots[i]->getPosition().y - saveSlots[i]->getContentSize().height / 2 * saveSlots[i]->getScale());
	}
	al2->setPosition(saveScreen->getPosition() - Vec2(200, 60));
	ar2->setPosition(saveScreen->getPosition() + Vec2(200, -60));
	bl2.setPos(al2->getPosition() - al2->getContentSize() / 2 * al2->getScale());
	br2.setPos(ar2->getPosition() - ar2->getContentSize() / 2 * ar2->getScale());
	leave->setPosition(Vec2(visibleSizeG.width * 0.1f, visibleSizeG.height * 0.9f) + getDefaultCamera()->getPosition() - visibleSizeV * 0.5f);
	leaveB.setPos(leave->getPosition() - leave->getContentSize() * 0.5f * leave->getScale());

	for (int i = 0; i < all.size(); i++)
	{
		all[i]->setPosition(all[i]->getPosition() + v * dt);
	}
	for (int i = 0; i < 5; i++)
	{
		choices[i].setPos(bar->getPosition() + (i - 2) * Vec2(80.f, 0.f) - all[0]->getContentSize() / 2 * all[0]->getScale());
		//choices[i].setPos(choices[i].getPos() + v * dt);
	}
}

void Creator::selectSave(string* str)
{
	
}

void Creator::saving()
{
	getUserType();
	nameOfSave->setString(forTyping);
	if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
	{
		deselect = true;
	}
	if (saveMapB.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
	{
		saveMap->setScale(1.2f);
		saveMap->setGlobalZOrder(28);
		saveMapL->setScale(1.5f);
		saveMapL->setGlobalZOrder(29);
		if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT) && forTyping.size() > 0)
		{
			ofstream saveFile;
			saveFile.open(forTyping + "S2DF.txt");
			for (int i = 0; i < maxW * maxH; i++)
			{
				for (int j = 0; j < MapT[i % maxW][i / maxW].size(); j++)
				{
					saveFile << MapT[i % maxW][i / maxW][j]->getName() << "=" << MapT[i % maxW][i / maxW][j]->getGlobalZOrder() << "=" << (int)(MapT[i % maxW][i / maxW][j]->getRotation() / 90) << "=" << MapT[i % maxW][i / maxW][j]->getTag();
					if (j < MapT[i % maxW][i / maxW].size() - 1)
						saveFile << "/";
				}
				saveFile << endl;
			}
			saveFile.close();
			ifstream checkName;
			checkName.open("CollectionOfS2DSaves.txt");
			string duplicationCheck;
			bool doubleCheck = true;
			while (getline(checkName, duplicationCheck))
			{
				if (duplicationCheck == forTyping)
					doubleCheck = false;
			}
			checkName.close();
			if (doubleCheck)
			{
				ofstream fileOfSaveFiles;
				fileOfSaveFiles.open("CollectionOfS2DSaves.txt", fstream::app);
				fileOfSaveFiles << forTyping << endl;
				fileOfSaveFiles.close();
			}
			saveFile.open(forTyping + "S2DW.txt");
			for (int i = 0; i < transmitterz.size(); i++)
			{
				if (i < receiverz.size())
				{
					saveFile << transmitterz[i].x << "," << transmitterz[i].y << "," << receiverz[i].x << "," << receiverz[i].y << endl;
				}
			}
			saveFile.close();
			saveFile.open(forTyping + "S2DT.txt");
			saveFile << start.x << ", " << start.y << ", " << end.x << ", " << end.y << endl;
			saveFile.close();
			saveFile.open(forTyping + "S2DS.txt");
			for (int i = 0; i < signLoc.size(); i++)
			{
				saveFile << signLoc[i].x << "," << signLoc[i].y << "," << signStr[i] << endl;
			}
			saveFile.close();
			nameOfSave->setVisible(false);
			saveScreen->setVisible(false);
			saveMap->setVisible(false);
			cancelSave->setVisible(false);
			saveMapL->setVisible(false);
			cancelSaveL->setVisible(false);
			aTest = true;
		}
	}
	else
	{
		saveMap->setScale(0.8f);
		saveMap->setGlobalZOrder(26);
		saveMapL->setScale(1.0f);
		saveMapL->setGlobalZOrder(27);
	}

	if (cancelSaveB.contains(INPUTS->getMousePosition() + getDefaultCamera()->getPosition() - visibleSizeG / 2))
	{
		cancelSave->setScale(1.2f);
		cancelSave->setGlobalZOrder(28);
		cancelSaveL->setScale(1.5f);
		cancelSaveL->setGlobalZOrder(29);
		if (deselect && INPUTS->getMouseButton(MouseButton::BUTTON_LEFT))
		{
			nameOfSave->setVisible(false);
			saveScreen->setVisible(false);
			saveMap->setVisible(false);
			cancelSave->setVisible(false);
			saveMapL->setVisible(false);
			cancelSaveL->setVisible(false);
			aTest = true;
		}
	}
	else
	{
		cancelSave->setScale(0.8f);
		cancelSave->setGlobalZOrder(26);
		cancelSaveL->setScale(1.0f);
		cancelSaveL->setGlobalZOrder(27);
	}

	//ofstream saveFile;
	//saveFile.open("RenameThisSavedFileSoItIsNotOverriden.txt");
	//for (int i = 0; i < maxW * maxH; i++)
	//{
	//	for (int j = 0; j < MapT[i % maxW][i / maxW].size(); j++)
	//	{
	//		saveFile << MapT[i % maxW][i / maxW][j]->getName() << "=" << MapT[i % maxW][i / maxW][j]->getGlobalZOrder() << "=" << (int)(MapT[i % maxW][i / maxW][j]->getRotation() / 90);
	//		if (j < MapT[i % maxW][i / maxW].size() - 1)
	//			saveFile << "/";
	//	}
	//	saveFile << endl;
	//}
	//saveFile.close();
}

void Creator::getUserType()
{
	if (INPUTS->getKeyPress(KeyCode::KEY_A))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "A";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_B))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "B";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_C))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "C";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_D))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "D";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_E))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "E";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_F))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "F";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_G))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "G";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_H))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "H";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_I))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "I";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_J))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "J";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_K))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "K";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_L))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "L";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_M))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "M";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_N))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "N";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_O))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "O";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_P))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "P";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_Q))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "Q";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_R))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "R";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_S))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "S";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_T))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "T";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_U))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "U";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_V))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "V";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_W))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "W";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_X))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "X";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_Y))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "Y";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_Z))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "Z";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_0))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "0";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_1))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "1";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_2))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "2";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_3))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "3";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_4))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "4";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_5))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "5";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_6))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "6";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_7))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "7";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_8))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "8";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_9))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += "9";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_SPACE))
	{
		if (toTest == true)
		{
			toTest = false;
			forTyping += " ";
		}
	}
	else if (INPUTS->getKeyPress(KeyCode::KEY_BACKSPACE))
	{
		if (toTest == true && forTyping.size() > 0)
		{
			toTest = false;
			forTyping.erase(forTyping.end() - 1);
		}
	}
	else
		toTest = true;
}

void Creator::menuCloseCallback(cocos2d::Ref * pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}
Scene* Creator::layer2 = nullptr;
