#pragma once
#include "cocos2d.h"
#include "Titlescreen.h"

using namespace::cocos2d;
class FrostLine : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	void update(float dt);
	void updateInputs(float dt);

	virtual void onExit();
	virtual void onEnter();
	virtual bool init();
	Vec2 VS;
	Vec2 OS;
	Sprite* FROST;
	Sprite* LINE;
	Sprite* EXPERIENCE;
	float totalTime = 0.f;

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(FrostLine);
private:
	static PhysicsWorld* physicsWorld;
	static Scene* sceneHandle;
};