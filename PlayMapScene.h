#pragma once

#include "cocos2d.h"
#include "mBox.h"

using namespace::cocos2d;

class PlayMap : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	void update(float dt);
	void updateInputs(float dt);
	void updateMouseInputs(float dt);
	void updateKeyboardInputs(float dt);
	void blackout(float dt, float *t, bool direction);
	void trigger(float dt, int i);

	virtual void onExit();
	virtual void onEnter();
	virtual bool init();

	int levelSlide;
	bool canClick;
	Sprite* door;
	mbox doorB;
	Sprite* levels[5];
	mbox levelsB[5];
	Label* levelNames[5];
	Sprite* nxt;
	mbox nxtB;
	Sprite* pre;
	mbox preB;
	std::vector<std::string> nameList;

	bool transferScene = false;
	float inTime = 1.f;

	DrawNode* blackScreen;
	Label* loding;

	void menuCloseCallback(cocos2d::Ref* pSender);

	Director* director;

	// implement the "static create()" method manually
	CREATE_FUNC(PlayMap);
};