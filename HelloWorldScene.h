#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "Character.h"
#include "cocos2d.h"
#include "Minty.h"
#include "PRIISM.h"
#include "CEO.h"
#include "BossVirt.h"

const int maximumH = 60;
const int maximumW = 60;

using namespace::cocos2d;
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
	void update(float dt);
	void tseq();
	void updateInputs(float dt);
	void updateMouseInputs(float dt);
	void updateKeyboardInputs(float dt);
	void updateUpdatables(float dt);
	void tileCollisionCheck(float dt, Vec2 *tot);
	void regTile(int tilex, int tiley, int tilez, std::string tileSprite);
	void physTile(int tilex, int tiley, int tilez, std::string tileSprite);
	void NPC(int x, int y, std::string NPCSprite);
	void LoadingMap();
	void tileUpdateList();
	void receiverOutput(bool mode, int k);
	void nextTileUpdateList();
	void buttonUpdateList(float dt);
	void doorOpener(float dt);
	void triggerTimers(float dt);
	void triggerLight(float dt);
	void nextTriggerLight(float dt);
	void opacityUp(float dt);
	void nextOpacityUp(float dt);
	void shakey();
	void splatter(float dt);
	void singe(float dt);
	void winSeq(float dt);
	void tileBoom(float dt);
	void blackout(float dt, float *t, bool direction);
	void moveBosses(float dt);
	void gameOverScreen(float dt);
	void mintyGoop(float dt);
	void turnOnAllLights();
	void turnOffAllLights();

	std::vector <std::vector < std::vector <Sprite*>>> tiles;
	std::vector <std::vector <std::vector <bool>>> updateGrid;

	std::vector <character> ch;
	std::vector <Sprite*> all;

	std::vector <Vec2> tileUpdate;
	std::vector <bool> tileBOOL;
	std::vector <Vec2> nextTileUpdate;
	std::vector <bool> nextTileBOOL;

	std::vector <Vec2> transmitters;
	std::vector <Vec2> receivers;
	std::vector <Vec3> buttonUpdate;

	std::vector <Vec3> openDoors;
	std::vector <bool> openBOOL;
	std::vector <SpriteFrame*> doorz;

	std::vector <Sprite*> boxes;
	std::vector <Vec2> originalBoxPositions;
	std::vector <bool> boxDestroyed;

	std::vector <Vec2> tilesToBoom;
	std::vector <float> tileBoomTime;

	std::vector <Vec2> signLocations;
	std::vector <std::string> signSprites;

	std::vector <std::vector <Sprite*>> lazerz;
	std::vector <Vec2> lazerLocs;
	std::vector <float> lazerTime;
	std::vector <Vec2> beamLocs;
	std::vector <bool> isLazer;
	std::vector<Vec2> lightLevel;
	std::vector<Vec2> nextLightLevel;

	std::vector<std::vector<int>> opacityOfSquare;
	std::vector<Vec2> opacityUpdate;
	std::vector<Vec2> nextOpacityUpdate;
	std::vector<std::vector<bool>> lampsOnOrOff;
	//std::vector<std::vector<Vec3>> laserDirections;
	//std::vector<Sprite*> laserPieces;
	//std::vector<Vec2> laserPositions;
	std::vector<Vec4> timerz;
	std::vector<bool> timerMode;
	Sprite* currentSignSprite;
	Vec2 prep, daStart, daEnd, bPrep;
	std::string mapName;
	DrawNode* blackScreen;
	Label* loding;

	std::vector<boss*> bosses;
	std::vector<Vec2> previousBossPositions;

	std::vector<std::vector<float>> mintyTiles;
	std::vector<Vec2> mintyTrail;

	std::vector<Vec2> bossDoorLocations;
	std::vector<bool> bossDoorBOOL;
	std::vector<float> bossDoorTimes;
	std::vector <SpriteFrame*> bossDoorz;
	std::vector <std::vector <bool>> bossDoorsOpened;

	Sprite* keepOnGoing, *giveUp, *GG;
	Label* keepOnGoingL, *giveUpL;
	mbox keepOnGoingB, giveUpB;

	float dtWalk, maxTime = 1.0f, tickTock = 0.f, doorSpeed = 0.05f, amountOfShake = 0.f, pauseBeforeExit = 0.f, splatterTime = 0.f, burnTime = 0.f, bombSeqTime = 0.f, nextBomb = 0.05f, inTime = 1,f, lazerDelay = 0.03f, GGOpacity = 0.f;
	bool press = false, won = false, transferScene = false, blackedOut = false, resetMap = false, iClicked = false, death = false, mintyTime = false, boxGoopMinor = false, boxGoopMajor = false, beMovin = false;
	int walkNum, shakeDirection = 1, lowerAnimX, lowerAnimY, upperAnimX, upperAnimY, moveAnimX, moveAnimY, slowSeq = 0, lazerToPlay = 0;

	Sprite* displayStuff;
	Label* clickToContinue;

	vector <Vec2> antiBlackOutSections;
	bool stopBlackOut = false;
	vector <Vec2> SUPEROpacityUpdate;
	vector<vector<bool>> permanentlyOn;

	virtual void onExit();
	virtual void onEnter();
	virtual bool init();

	//virtual bool initWithPhysics();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

	Director* director;

	Texture2D::TexParams texParams = { GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };

	bool noFrameChange = false;
	SpriteFrame* frontBlink;
	SpriteFrame* sideBlink;
	SpriteFrame* backBlink;

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
	static PhysicsWorld* physicsWorld;
	//static Scene* sceneHandle;
	//static Scene* layeryH;
};

#endif // __HELLOWORLD_SCENE_H__
