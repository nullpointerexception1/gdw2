#pragma once
#include "BossVirt.h"
#include <vector>

using namespace::std;

class CEO : public boss
{
public:
	CEO(Scene* s, Vec2 pos, vector<vector<SpriteFrame*>> walkCycle, float accLimit, int startDirection, vector<SpriteFrame*> monocle);
	void spawn();
	void die();
	void move();
	void setCustomFrame();
	void attack(int attackType, float dt);
	Vec2 getVectorTo(Vec2 place);
	void setTarget(Vec2 tar);
	Vec2 getTarget();
	void onDamage(float dt);
private:
	Vec2 target;
	vector<SpriteFrame*> Monocle;
	float timeUntilTeleport = 0.f;
	float teleportTime = 5.f;
	bool SHITHECANSEEME = false;
	bool toSpawn = false;
	bool portin = false;
	bool phaseIn = true;
	float CEOpacity = 255.f;
	float exposed = 0.f;
	float maximumExposure = 6.f;
	float swing = 0.f;
	bool SOUPAswinging = false;
	bool swinging = false;
};