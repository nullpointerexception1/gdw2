#ifndef __CREATOR_SCENE_H__
#define __CREATOR_SCENE_H__

#include "cocos2d.h"

const int maxH = 60;
const int maxW = 60;
using namespace::cocos2d;
class Creator : public cocos2d::Scene, public cocos2d::CCTextFieldDelegate
{
public:
	static cocos2d::Scene* createScene();
	void update(float dt);
	void updateInputs(float dt);
	void updateMouseInputs(float dt);
	void updateKeyboardInputs(float dt);

	virtual void onExit();
	virtual void onEnter();
	virtual bool init();

	std::vector <Sprite*> all;
	Sprite* bar;
	Sprite* al;
	Sprite* ar;
	Sprite* clik;
	Sprite* saveB;
	Sprite* loadB;
	Sprite* saveScreen;
	Label* nameOfSave;
	Sprite* saveMap;
	Label* saveMapL;
	mbox saveMapB;
	Sprite* cancelSave;
	Label* cancelSaveL;
	mbox cancelSaveB;
	mbox bl;
	mbox br;
	mbox bm;
	mbox bs;
	mbox bo;
	mbox choices[5];
	Label* saveSlots[5];
	mbox saveSlotsB[5];
	std::vector <std::string> namesOfLevels;
	Sprite* loadScreen;
	Sprite* al2;
	Sprite* ar2;
	mbox bl2;
	mbox br2;
	Sprite* leave;
	mbox leaveB;
	std::vector<Vec2> transmitterz;
	std::vector<Vec2> receiverz;
	std::vector<Vec2> signLoc;
	std::vector<std::string> signStr;
	Vec2 start, end;

	std::string forTyping;
	void selectSave(std::string *str);
	void saving();
	void getUserType();
	//Sprite* why;

	std::vector <std::vector <std::vector <Sprite*>>> MapT;


	
	Vec2 vertz[maxW + 1][maxH + 1];
	//Sprite* Map[maxW][maxH];

	Vec2 prep;
	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	Director* director;

	// implement the "static create()" method manually
	CREATE_FUNC(Creator);
private:
	//static PhysicsWorld* physicsWorld;
	//static Scene* sceneHandle;
	static Scene* layer2;
};

#endif // __CREATOR_SCENE_H__