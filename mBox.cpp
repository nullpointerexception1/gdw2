#include "mBox.h"

mbox::mbox()
{
}

mbox::~mbox()
{
}

void mbox::setPos(int X, int Y)
{
	x = X;
	y = Y;
}

void mbox::setDim(int W, int H)
{
	h = H;
	w = W;
}

void mbox::setPos(Vec2 v)
{
	x = v.x;
	y = v.y;
}

void mbox::setDim(Vec2 v)
{
	h = v.y;
	w = v.x;
}

Vec2 mbox::getPos()
{
	return Vec2(x, y);
}

bool mbox::contains(Vec2 v)
{
	if (x < v.x && v.x < x + w && y < v.y && v.y < y + h)
		return true;
	return false;
}
