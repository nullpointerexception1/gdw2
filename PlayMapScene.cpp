#include <vector>
#include <fstream>
#include "PlayMapScene.h"
#include "Titlescreen.h"
#include "DisplayHandler.h"
#include "InputHandler.h"
#include "HelloWorldScene.h"
#include "Soundtrack.h"

using namespace::std;

Sounds play;

cocos2d::Scene * PlayMap::createScene()
{
	Scene* scene = PlayMap::create();

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();

	return scene;
}

void PlayMap::update(float dt)
{
	for (int i = 0; i < 5; i++)
	{
		if (i + levelSlide * 5 < nameList.size())
		{
			levelNames[i]->setString(nameList[i + levelSlide * 5]);
		}
		else
		{
			levelNames[i]->setString("-");
		}
	}
	if (levelSlide > 0)
		pre->setOpacity(255);
	else
		pre->setOpacity(81);
	if ((levelSlide + 1) * 5 < nameList.size())
		nxt->setOpacity(255);
	else
		nxt->setOpacity(81);
	updateInputs(dt);
}

void PlayMap::updateInputs(float dt)
{
	if ((!transferScene) && inTime > 0)
	{
		//std::cout << "DECREASING" << std::endl;
		blackout(dt * 2.f, &inTime, true);
	}
	else if (transferScene)
	{
		trigger(dt, 0);
	}
	else
	{
		updateMouseInputs(dt);
		updateKeyboardInputs(dt);
	}
}

void PlayMap::updateMouseInputs(float dt)
{
	if (INPUTS->getMouseButtonRelease(MouseButton::BUTTON_LEFT))
	{
		canClick = true;
	}
	else
	{
		if (doorB.contains(INPUTS->getMousePosition()))
		{
			if (canClick && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				Scene* s = Title::createScene();
				Director::getInstance()->replaceScene(s);
				canClick = false;
			}
		}
		else if (nxtB.contains(INPUTS->getMousePosition()))
		{
			if (canClick && (levelSlide + 1) * 5 < nameList.size() && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				levelSlide++;
				canClick = false;
			}
		}
		else if (preB.contains(INPUTS->getMousePosition()))
		{
			if (canClick && levelSlide > 0 && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT))
			{
				levelSlide--;
				canClick = false;
			}
		}
		for (int i = 0; i < 5; i++)
		{
			if (levelsB[i].contains(INPUTS->getMousePosition()))
			{
				if (canClick && INPUTS->getMouseButtonPress(MouseButton::BUTTON_LEFT) && levelNames[i]->getString() != "-")
				{
					play.Sound("Soundtrack/selection button.mp3");
					canClick = false;
					ofstream FIN;
					FIN.open("S2DPlayingMap.txt");
					FIN << levelNames[i]->getString() << endl;
					FIN.close();
					transferScene = true;
				}
			}
		}
	}
}

void PlayMap::updateKeyboardInputs(float dt)
{
}

void PlayMap::blackout(float dt, float * t, bool direction)
{
	if (direction)
	{
		*t -= dt;
		if (*t < 0)
			*t = 0;
		blackScreen->setOpacity(*t * 255);
	}
	else
	{
		*t += dt;
		if (*t > 1)
			*t = 1;
		blackScreen->setOpacity(*t * 255);
		loding->setOpacity(*t * 255);
	}
}

void PlayMap::trigger(float dt, int i)
{
	if (inTime < 1)
	{
		blackout(dt * 2.f, &inTime, false);
	}
	else
	{
		Scene* s = HelloWorld::createScene();
		Director::getInstance()->replaceScene(s);
	}
}

void PlayMap::onExit()
{
	play.deletemusic();
	Scene::onExit();
}

void PlayMap::onEnter()
{
	play.Music("Soundtrack/Second to Death title.mp3");
	Scene::onEnter();
}

bool PlayMap::init()
{
	//DISPLAY->createDebugConsole(true);
	//std::cout << "THE CONSOLE IS NOW OPEN" << std::endl;
	if (!Scene::init())
	{
		return false;
	}
	canClick = true;
	levelSlide = 0;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("TempSprSheet.plist");

	string toReadIn;
	ifstream daLevels;
	daLevels.open("CollectionOfS2DSaves.txt");
	while (getline(daLevels, toReadIn))
	{
		nameList.push_back(toReadIn);
	}
	
	door = Sprite::create("ExitDoor..png");
	nxt = Sprite::createWithSpriteFrameName("Arrow2.png");
	pre = Sprite::createWithSpriteFrameName("Arrow1.png");
	for (int i = 0; i < 5; i++)
	{
		levels[i] = Sprite::createWithSpriteFrameName("MenuIcon.png");
		levelNames[i] = Label::createWithSystemFont("-", "Times New Roman", 30);
	}

	door->setPosition(visibleSize.width * 0.2f, visibleSize.height * 0.8f);
	door->setScale(64.f / door->getContentSize().height);
	door->setGlobalZOrder(2.f);

	doorB.setPos(door->getPosition() - door->getContentSize() * 0.5f * door->getScale());
	doorB.setDim(door->getContentSize() * door->getScale());

	nxt->setPosition(visibleSize.width * 0.7f, visibleSize.height * 0.2f);
	pre->setPosition(visibleSize.width * 0.3f, visibleSize.height * 0.2f);
	nxt->setScale(64.f / nxt->getContentSize().height);
	pre->setScale(64.f / pre->getContentSize().height);
	nxt->setGlobalZOrder(2.f);
	pre->setGlobalZOrder(2.f);

	nxtB.setPos(nxt->getPosition() - nxt->getContentSize() * 0.5f * nxt->getScale());
	nxtB.setDim(nxt->getContentSize() * nxt->getScale());
	preB.setPos(pre->getPosition() - pre->getContentSize() * 0.5f * pre->getScale());
	preB.setDim(pre->getContentSize() * pre->getScale());

	for (int i = 0; i < 5; i++)
	{
		levels[i]->setPosition(visibleSize.width * 0.5f, -i * 100 + 200 + visibleSize.height * 0.5f);
		levels[i]->setScale(64.f / levels[i]->getContentSize().height);
		levels[i]->setGlobalZOrder(1.f);

		levelsB[i].setPos(levels[i]->getPosition() - levels[i]->getContentSize() * 0.5f * levels[i]->getScale());
		levelsB[i].setDim(levels[i]->getContentSize() * levels[i]->getScale());
		
		levelNames[i]->setPosition(levels[i]->getPosition());
		levelNames[i]->setGlobalZOrder(2.f);
		levelNames[i]->setColor(Color3B::BLACK);
	}

	this->addChild(door);
	this->addChild(nxt);
	this->addChild(pre);
	for (int i = 0; i < 5; i++)
	{
		this->addChild(levels[i]);
		this->addChild(levelNames[i]);
	}

	blackScreen = DrawNode::create();
	blackScreen->drawSolidRect(Vec2(0, 0), visibleSize, Color4F::BLACK);
	blackScreen->setGlobalZOrder(10);
	this->addChild(blackScreen, 10);
	
	loding = Label::createWithSystemFont("Loading...", "Helvetica", 80);
	loding->setGlobalZOrder(10);
	loding->setPosition(getDefaultCamera()->getPosition());
	loding->setColor(Color3B::WHITE);
	loding->setOpacity(0);

	this->addChild(loding, 11);

	PlayMap::scheduleUpdate();
	return true;
}

void PlayMap::menuCloseCallback(cocos2d::Ref * pSender)
{
}
