#pragma once

#include "cocos2d.h"

using namespace::cocos2d;

class levelTile
{
public:
	levelTile();
	~levelTile();
	levelTile(std::string str);
	levelTile(float f1, float f2);
	levelTile(Vec2 v);
	levelTile(std::string str, float f1, float f2);
	levelTile(std::string str, Vec2 v);
	void setPos(float f1, float f2);
	void setPos(Vec2 v);
	void setSprite(std::string str);
	void setSolid(bool q);
	Sprite* tile;
	bool solid;
};