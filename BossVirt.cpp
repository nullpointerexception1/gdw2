#include "BossVirt.h"

boss::boss(Scene * s, Vec2 pos, vector<vector<SpriteFrame*>> wc, int startDirection, int bossType)
{
	setPosition(pos);
	loadWalkCycle(wc);
	bossSprite = Sprite::createWithSpriteFrame(wc[startDirection][0]);
	bossSprite->setScale(80.f / bossSprite->getContentSize().height);
	bossSprite->setGlobalZOrder(3.f);
	setVelocity(0, 0);
	setAcceleration(0, 0);
	bossT = bossType;
	scene = s;
	bossCoolDown = 0.f;
}

boss::~boss()
{
	removeBossFromScene();
}

void boss::attack(int attackType, float dt)
{
}

void boss::deathSequence(float dt)
{
	deathTime += dt;
	while (deathTime > deathFrameTime)
	{
		deathTime -= deathFrameTime;
		deathFrame++;
		if (deathFrame >= 51 + deathFrames[direction].size())
		{
			bossSprite->setOpacity(255);
			bossSprite->setVisible(false);
		}
		else if (deathFrame >= deathFrames[direction].size())
			bossSprite->setOpacity(255 - (deathFrame - deathFrames[direction].size()) * 5);
		else if (deathFrame < deathFrames[direction].size())
		{
			setDeathFrame(deathFrame);
		}
	}
}

void boss::damage(float damageAMT)
{
	damaged = damageAMT;
}

void boss::dialBackDamage(float dt)
{
	if (damaged > 0)
	{
		damaged -= 500.f * dt;
		if (damaged < 0)
			damaged = 0;
		bossSprite->setColor(Color3B(255, 255 - damaged, 255 - damaged));
	}
}

void boss::incrementCycle(float dt)
{
	currentTime += dt;
	while (currentTime > walkTime)
	{
		currentTime -= walkTime;
		cycleFrame++;
		if (cycleFrame >= walkCycle[direction].size())
		{
			cycleFrame -= walkCycle[direction].size();
		}
		setWalkFrame(cycleFrame, direction);
	}
}

void boss::setPosition(Vec2 pos)
{
	position = pos;
}

void boss::setPosition(float x, float y)
{
	position.x = x;
	position.y = y;
}

void boss::setMaxSpeed(float ms)
{
	maxSpeed = ms;
}

void boss::setVelocity(Vec2 vel)
{
	velocity = vel;
}

void boss::setVelocity(float x, float y)
{
	velocity.x = x;
	velocity.y = y;
}

void boss::setAcceleration(Vec2 acc)
{
	acceleration = acc;
}

void boss::setAcceleration(float x, float y)
{
	acceleration.x = x;
	acceleration.y = y;
}

Vec2 boss::getPosition()
{
	return position;
}

Vec2 boss::getVelocity()
{
	return velocity;
}

Vec2 boss::getAcceleration()
{
	return acceleration;
}

float boss::getMaxSpeed()
{
	return maxSpeed;
}

void boss::setHP(int h)
{
	hp = h;
}

int boss::getHP()
{
	return hp;
}

void boss::setWalkFrame(int frameNumber, int dir)
{
	bossSprite->setSpriteFrame(walkCycle[dir][frameNumber]);
	direction = dir;
}

void boss::setDeathFrame(int frameNumber)
{
	bossSprite->setSpriteFrame(deathFrames[direction][frameNumber]);
}

void boss::setDirection(int dir)
{
	direction = dir;
}

int boss::getDirection()
{
	return direction;
}

void boss::setCycleFrame(int frame)
{
	cycleFrame = frame;
}

void boss::setDeathCycleFrame(int frame)
{
	deathFrame = frame;
}

int boss::getDeathCycleFrame()
{
	return deathFrame;
}

int boss::getCycleFrame()
{
	return cycleFrame;
}

void boss::loadWalkCycle(vector<vector<SpriteFrame*>> wc)
{
	for (int i = 0; i < walkCycle.size(); i++)
	{
		walkCycle[i].clear();
	}
	walkCycle.clear();
	for (int i = 0; i < wc.size(); i++)
	{
		vector<SpriteFrame*> tempCycle;
		for (int j = 0; j < wc[i].size(); j++)
		{
			tempCycle.push_back(wc[i][j]);
		}
		walkCycle.push_back(tempCycle);
	}
}

void boss::loadDeathCycle(vector<vector<SpriteFrame*>> dc)
{
	for (int i = 0; i < deathFrames.size(); i++)
	{
		deathFrames[i].clear();
	}
	deathFrames.clear();
	for (int i = 0; i < dc.size(); i++)
	{
		vector<SpriteFrame*> tempCycle;
		for (int j = 0; j < dc[i].size(); j++)
		{
			tempCycle.push_back(dc[i][j]);
		}
		deathFrames.push_back(tempCycle);
	}
}

void boss::addBossToScene(Scene * s)
{
	s->addChild(bossSprite);
}

void boss::removeBossFromScene()
{
	bossSprite->removeFromParent();
}

void boss::setStandardAcc(float acc)
{
	standardAcc = acc;
}

float boss::getStandardAcc()
{
	return standardAcc;
}

void boss::conductMovement(float dt)
{
	position = bossSprite->getPosition();
	velocity = bossSprite->getPhysicsBody()->getVelocity();
	velocity += acceleration * dt;
	if (velocity.length() > maxSpeed)
	{
		velocity.normalize();
		velocity *= maxSpeed;
	}
	incrementCycle(dt);
	bossSprite->getPhysicsBody()->setVelocity(velocity);
	if (bossCoolDown > 0)
		bossCoolDown -= dt;
}

Sprite * boss::getBossSprite()
{
	return bossSprite;
}

void boss::setBossPosition()
{
	bossSprite->setPosition(position);
}

void boss::setAlive(bool state)
{
	alive = state;
}

bool boss::isAlive()
{
	return alive;
}

void boss::setUseStandardAcc(bool state)
{
	useStandardAcc = state;
}

bool boss::isUsingStandardAcc()
{
	return useStandardAcc;
}

void boss::setPlayerPosition(Vec2 pp)
{
	playerPosition = pp;
}

Vec2 boss::getPlayerPosition()
{
	return playerPosition;
}

int boss::getBossType()
{
	return bossT;
}

void boss::setSpawnpoint(Vec2 pos)
{
	spawnPoint = pos;
}

Vec2 boss::getSpawnPoint()
{
	return spawnPoint;
}

void boss::setSignal(Vec3 v)
{
	signals.push_back(v);
}

Vec3 boss::getSignal()
{
	if (signals.size() > 0)
	{
		Vec3 temp = signals[0];
		signals.erase(signals.begin());
		return temp;
	}
	else
	{
		return(Vec3(0, 0, 0));
	}
}

int boss::getSignalSize()
{
	return signals.size();
}

void boss::setBossCoolDown(float ff)
{
	bossCoolDown = ff;
}

float boss::getBossCoolDown()
{
	return bossCoolDown;
}
